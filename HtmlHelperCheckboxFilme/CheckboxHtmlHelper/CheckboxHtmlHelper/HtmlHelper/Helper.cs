﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckboxHtmlHelper.HtmlHelper
{
    public static class Helper
    {
        
      

        public static IHtmlContent CheckBoxList(this IHtmlHelper helper, string name, SelectList selectlist, object[] selected)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in selectlist)
            {
                sb.Append(string.Format($"<input type='checkbox' value='{item.Value}' id='{item.Value}' name='{name}' {(selected.Contains(item.Value) ? "checked" : "")}/> {item.Text}<br/>"));

            }


            return new HtmlString(sb.ToString());
        }


    }
}
