﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CheckboxHtmlHelper.Models;

namespace CheckboxHtmlHelper.Controllers
{
    public class HomeController : Controller
    {


        public IActionResult Index()
        {
            if (Request.Method == "POST")
            {

            }


            FilmRepository filmrepo = new FilmRepository();

            //Muss erst initialisiert werden, damit der Helper was zu futtern hat
            ViewBag.FilmIDs = new string[0];

            return View(filmrepo.alleFilme);
        }


        [HttpPost]
        public IActionResult Index(string[] movies)
        {
            if (movies != null)
            {
                ViewBag.FilmIDs = movies;
            }

            FilmRepository filmrepo = new FilmRepository();

            return View(filmrepo.alleFilme);
        }




        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
