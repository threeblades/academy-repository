﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckboxHtmlHelper.Models
{
    public class FilmRepository
    {
        public List<Film> alleFilme = new List<Film>();

        public FilmRepository()
        {
            alleFilme.Add(new Film(1, "Blade Runner"));
            alleFilme.Add(new Film(2, "Star Wars"));
            alleFilme.Add(new Film(3, "The Godfather"));
            alleFilme.Add(new Film(4, "Shawshank Redemption"));
            alleFilme.Add(new Film(5, "A Tale of Two Sisters"));
            alleFilme.Add(new Film(6, "The Ring"));
            alleFilme.Add(new Film(7, "JU-ON"));
            alleFilme.Add(new Film(8, "Airplane!"));
            alleFilme.Add(new Film(9, "Top Secret"));
            alleFilme.Add(new Film(10, "Enemy"));

        }
    }
}
