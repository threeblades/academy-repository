﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CheckboxHtmlHelper.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Titel { get; set; }

        public Film(int id, string titel)
        {
            Id = id;
            Titel = titel;
        }

    }
}
