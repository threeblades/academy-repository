﻿using FilterActionFilter.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FilterActionFilter.Filter
{
    public class SpeedFilterAttribute : Attribute, IActionFilter
    {
        Stopwatch watch = new Stopwatch();


        public void OnActionExecuting(ActionExecutingContext executingContext)
        {
            watch.Start();

        }

        public void OnActionExecuted(ActionExecutedContext executedContext)
        {
            watch.Stop();

            ((HomeController)executedContext.Controller).ViewBag.TimeElapsed = watch.ElapsedMilliseconds;

            watch.Reset();
        }

    }
}
