﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FilterActionFilter.Models;
using System.Threading;
using FilterActionFilter.Filter;

namespace FilterActionFilter.Controllers
{
    public class HomeController : Controller
    {
        [SpeedFilter]
        public IActionResult Index()
        {

            Thread.Sleep(1000);

            return View();
        }


        //Steht in der Startup
        //[DivideZeroFilter]
        public IActionResult DivideZero()
        {
            int a = 1;
            int b = 0;
            int c = a / b;
            
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
