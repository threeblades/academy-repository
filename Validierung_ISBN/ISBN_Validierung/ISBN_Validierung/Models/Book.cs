﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ISBN_Validierung.Models
{
    public class Book
    {
        [Required]
        [StringLength(200, MinimumLength = 2)]
        public string BookTitle { get; set; }

        //[Remote(action: "CheckISBN", controller: "Home", ErrorMessage = "ISBN ist nicht gültig!")]
        [ISBNCheck]
        public string ISBN { get; set; }
    }
}
