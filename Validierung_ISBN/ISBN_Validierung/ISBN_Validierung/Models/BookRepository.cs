﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ISBN_Validierung.Models
{
    public class BookRepository
    {
        public bool CheckISBN(string isbn)
        { 

            if (isbn.Length == 10)
            {
                int checkDigit = Int32.Parse(isbn[9].ToString()); 
                int summe = 0;
                int t = 0;

                for (int i = 0; i < 9; i++)
                {
                    t += Int32.Parse(isbn[i].ToString()) * (i + 1);
                    summe = t;
                }

                return checkDigit == (summe % 11);

            }
            else if (isbn.Length == 13)
            {
                int checkDigit = Int32.Parse(isbn[12].ToString());
                int summe = 0;

                for (int i = 0; i < 12; i++)
                {
                    if ((i + 1 ) % 2 == 0)
                    {
                        summe += 3 * Int32.Parse(isbn[i].ToString());
                    }
                    else
                    {
                        summe += Int32.Parse(isbn[i].ToString());
                    }

                }

                return checkDigit == ( 10 - (summe % 10) % 10);

            }

            return false;

        }


    }
}
