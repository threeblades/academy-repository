﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ISBN_Validierung.Models
{
    public class ISBNCheckAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            BookRepository bookrepo = new BookRepository();

            bool state = false;

            string isbn = value as String;

            if (isbn != null)
            {

                if (isbn.Length == 10 || isbn.Length == 13)
                {
                    state = bookrepo.CheckISBN(isbn);
                }
            }

            return state;
        }
    }
}
