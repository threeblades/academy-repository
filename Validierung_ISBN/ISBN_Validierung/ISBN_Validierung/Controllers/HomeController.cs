﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ISBN_Validierung.Models;

namespace ISBN_Validierung.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Index(Book book)
        {
            if (ModelState.IsValid == false)
            {
                return View();
            }
            else
            {
                return View(book);
            }
            
        }


        
        public IActionResult CheckISBN(string ISBN)
        {
            BookRepository bookrepo = new BookRepository();

            bool state = false;

            if (ISBN != null)
            {

                if (ISBN.Length == 10 || ISBN.Length == 13)
                {
                    state = bookrepo.CheckISBN(ISBN);
                }
            }

            return Json(state);
        }



        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
