﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CachingCountries.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;

namespace CachingCountries.Controllers
{
    public class HomeController : Controller
    {
        IMemoryCache myCache;

        CountryRepository repo = new CountryRepository();

        public HomeController (IMemoryCache memoryCache)
        {
            myCache = memoryCache;
        }


        public IActionResult Index()
        {


            var allContinents = repo.GetContinents();

            ViewData["continent"] = new SelectList(allContinents, "ContinentName", "ContinentName");

            return View();
        }



        public IActionResult SearchCountriesResult(string continent)
        {

            var cacheEntry = myCache.GetOrCreate(continent, entry => {

                var selectedCountries = repo.SearchCountries(continent);

                entry.SlidingExpiration = TimeSpan.FromMinutes(60);

                return selectedCountries;
            });

           
            ViewBag.Continent = continent;

            return PartialView("SearchCountriesResult", cacheEntry);
        }






        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
