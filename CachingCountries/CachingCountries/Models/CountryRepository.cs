﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CachingCountries.Models
{
    public class CountryRepository
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;" +
                           "Initial Catalog=Mondial;" +
                           "Integrated Security=true;";


        public List<Continent> GetContinents()
        {
            List<Continent> allContinents = new List<Continent>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT * FROM dbo.continent", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {

                        allContinents.Add(new Continent
                        {
                            ContinentName = reader.GetString(0),
                            Area = reader.GetInt32(1)

                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }


            }

                return allContinents;
        }



        public List<Country> SearchCountries(string continent)
        {
            List<Country> selectedCountries = new List<Country>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT Name, Capital, Population FROM dbo.country JOIN dbo.encompasses ON Code = Country WHERE Continent =@continent ORDER BY Name;", connection);

                    command.Parameters.AddWithValue("@continent", continent);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {

                        selectedCountries.Add(new Country
                        {
                            CountryName = reader.GetString(0),
                            Capital = reader.GetString(1),
                            Population = reader.GetInt32(2)

                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }


            }

            return selectedCountries;
        }




    }
}
