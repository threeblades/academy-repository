﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingCountries.Models
{
    public class Country
    {
        public string CountryName { get; set; }
        public string Capital { get; set; }
        public int Population { get; set; }

    }
}
