﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingCountries.Models
{
    public class Continent
    {
        public string ContinentName { get; set; }
        public int Area { get; set; }

    }
}
