﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ajax_Northwind.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ajax_Northwind.Controllers
{
    public class HomeController : Controller
    {
        
        NorthwindContext _context;

        public HomeController(NorthwindContext context)
        {
            _context = context;
        }



        public IActionResult Index()
        {
            ViewData["countryselect"] = new SelectList(_context.Customers.Select(c => c.Country).Distinct());

            return View();
        }





        public IActionResult GetCustomersPartial(string countryname)
        {
            var allCustomers = _context.Customers.Where(country => country.Country == countryname);

            return PartialView("_GetCustomersPartial", allCustomers.ToList());
        }






        public IActionResult GetOrdersPartial(string customerID)
        {
            var allOrders = _context.Orders.Where(order => order.CustomerId == customerID);

            return PartialView("_GetOrdersPartial", allOrders.ToList());
        }





        public IActionResult GetOrderDetailsPartial(int orderID)
        {
            ViewBag.FullPrice = _context.OrderDetails.Where(o => o.OrderId == orderID).Sum(p => p.UnitPrice * p.Quantity);

            var oneOrder = _context.Orders.Where(order => order.OrderId == orderID).FirstOrDefault();

            //ViewBag.Employee = _context.Employees.Where(e => e.EmployeeId == oneOrder.EmployeeId).Select(a => a.FirstName).FirstOrDefault();

            return PartialView("_GetOrderDetailsPartial", oneOrder);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
