﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Eurobanknoten.Models;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Eurobanknoten.Controllers
{
    public class HomeController : Controller
    {

        private IStringLocalizer<HomeController> localizer;


        Seriennummerprüfer seriennummerprüfer;

        public HomeController(IStringLocalizer<HomeController> localizer, Seriennummerprüfer prüfer)
        {
            this.localizer = localizer;
            seriennummerprüfer = prüfer;

        }

        [HttpGet]
        public IActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public IActionResult SingleCheck(string serial)
        {


            ViewData["Message"] = localizer[seriennummerprüfer.CheckSerial(serial)];

            return View("Index");
        }

        [HttpPost]
        public IActionResult MultiCheck(string multipleserials)
        {
            List<string> templist = new List<string>();

            string[] singleserialnumbers = multipleserials.Split("\r\n");

            for (int i = 0; i < singleserialnumbers.Length; i++)
            {
                templist.Add(localizer[seriennummerprüfer.CheckSerial(singleserialnumbers[i].Trim())]);

            }

            ViewData["MessageMulti"] = templist;

            return View("Index");
        }

        [HttpPost]
        public IActionResult FileCheck(IFormFile inputFile)
        {
            List<string> templist = new List<string>();

            using (StreamReader sr = new StreamReader(inputFile.OpenReadStream()))
            {
                while (sr.EndOfStream == false)
                {
                    string singleserial = sr.ReadLine();

                    templist.Add(localizer[seriennummerprüfer.CheckSerial(singleserial.Trim())]);
                }
            }

            ViewData["MessageFile"] = templist;

            return View("Index");
        }



        public IActionResult Privacy()
        {
            return View();
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
