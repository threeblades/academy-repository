using Microsoft.Extensions.Localization;
using System;
using System.Text.RegularExpressions;

namespace Eurobanknoten.Models
{
    public class Seriennummerpr�fer
    {
        private IStringLocalizer<Seriennummerpr�fer> localizer;

        public Seriennummerpr�fer(IStringLocalizer<Seriennummerpr�fer> localizer)
        {
            this.localizer = localizer;

        }

        public string CheckSerial(string serialNumber)
        {
            try
            {
                // Auf Formatfehler pr�fen
                CheckFormat(serialNumber.ToUpper());
            }
            catch (FormatException fe)
            {
                // Seriennummer mit Fehlertext zur�ckliefern
                return string.Format("{0} {1}", serialNumber, fe.Message);
            }

            string digitOnlyNumber = CreateDigitOnlySerial(serialNumber);

            // Seriennummer mit Generation und G�ltigkeit zur�ckliefern

            return string.Format("{0} {1} {2}", serialNumber,
                                                IsSecondGeneration(serialNumber) ? localizer["2. Gen"] : localizer["1. Gen"],
                                                DigitalRoot(digitOnlyNumber) ? localizer["Valide"] : localizer["Nicht Valide"]);
        }

        private void CheckFormat(string serialNumber)
        {
            // �berpr�fung der L�nge
            if (serialNumber.Length < 12)
            {
                throw new FormatException(localizer["Zu Kurz"]);
            }
            else if (serialNumber.Length > 12)
            {
                throw new FormatException(localizer["Zu Lang"]);
            }

            // Welche Generation
            if (IsSecondGeneration(serialNumber))
            {
                // 2. Generation

                // Zwei Buchstaben zu Beginn?
                if (char.IsLetter(serialNumber[0]) == false &&
                    char.IsLetter(serialNumber[1]) == false)
                {
                    throw new FormatException(localizer["Kein Buchstabe"]);
                }

                // G�ltiger Buchstabe zu Beginn?
                Regex validLetters = new Regex(@"^[DEHJMNPR-Z]");

                if (validLetters.IsMatch(serialNumber) == false)
                {
                    throw new FormatException(localizer["Falscher Buchstabe"]);
                }

                // Restliche Stellen nur Zahlen?
                Regex ziffernRegex = new Regex(@"^..\d{10}$");

                if (!ziffernRegex.IsMatch(serialNumber))
                {
                    throw new FormatException(localizer["Falsche Zeichen"]);
                }
            }
            else
            {
                // 1. Generation

                // Buchstabe am Anfang
                if (!char.IsLetter(serialNumber[0]))
                {
                    throw new FormatException(localizer["Kein Buchstabe"]);
                }

                // G�ltiger Buchstabe zu Beginn?
                Regex validLetters = new Regex(@"^[^A-CI-KRW]");

                if (validLetters.IsMatch(serialNumber) == false)
                {
                    throw new FormatException(localizer["Falscher Buchstabe"]);
                }

                // Restliche Stellen nur Zahlen?
                Regex ziffernRegex = new Regex(@"^.\d{11}$");

                if (!ziffernRegex.IsMatch(serialNumber))
                {
                    throw new FormatException(localizer["Falsche Zeichen"]);
                }
            }
        }

        private string CreateDigitOnlySerial(string original)
        {
            // In Gro�buchstaben umwandeln
            original = original.ToUpper();
            string digitOnlySerial;
            if (IsSecondGeneration(original))
            {
                // 1. und 2. Stelle in ASCII Code umwandeln und des Rest des Strings an neuen String anf�gen
                digitOnlySerial = ((int)original[0]).ToString() + ((int)original[1]).ToString() + original.Substring(2, original.Length - 2);
            }
            else
            {
                // 1. Stelle in ASCII Code umwandeln und des Rest des Strings an neuen String anf�gen
                digitOnlySerial = ((int)original[0]).ToString() + original.Substring(1, original.Length - 1);
            }
            return digitOnlySerial;
        }

        private bool DigitalRoot(string digitOnlySerialnumber)
        {
            int startQuersumme = 0;
            // Alle Ziffern aufaddieren
            for (int i = 0; i < digitOnlySerialnumber.Length; i++)
            {
                startQuersumme += int.Parse(digitOnlySerialnumber[i].ToString());
            }

            int zahl = startQuersumme;
            int neunerrest = 0;
            do
            {
                neunerrest = 0;
                // Quersumme bilden
                while (zahl > 0)
                {
                    neunerrest += zahl % 10;
                    zahl /= 10;
                }
                zahl = neunerrest;
            }
            while (neunerrest > 9); // Wiederholen falls Quersumme immernoch gr��er 9 ist

            return neunerrest == 9;
        }

        private bool IsSecondGeneration(string serialnumber)
        {
            // Sind die ersten zwei Zeichen in der Seriennummer Buchstaben?
            bool twoLetters = char.IsLetter(serialnumber[0]) && char.IsLetter(serialnumber[1]);
            bool restDigits = true;

            // �berpr�fen ob der Rest des Strings Ziffern sind
            for (int i = 2; i < serialnumber.Length; i++)
            {
                if (char.IsDigit(serialnumber[i]) == false)
                {
                    restDigits = false;
                }
            }
            return twoLetters && restDigits;
        }
    }
}