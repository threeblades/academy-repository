﻿using System;
using System.Collections.Generic;

namespace Zustand_Staedtereisen.Models
{
    public partial class Attraktion
    {
        public int AttraktionId { get; set; }
        public string Name { get; set; }
        public string Anschrift { get; set; }
        public int Bewertung { get; set; }
        public string Url { get; set; }
        public int StadtId { get; set; }

        public virtual Stadt Stadt { get; set; }
    }
}
