﻿using System;
using System.Collections.Generic;

namespace Zustand_Staedtereisen.Models
{
    public partial class Stadt
    {
        public Stadt()
        {
            Attraktion = new HashSet<Attraktion>();
        }

        public int StadtId { get; set; }
        public string Name { get; set; }
        public int BundeslandId { get; set; }

        public virtual Bundesland Bundesland { get; set; }
        public virtual ICollection<Attraktion> Attraktion { get; set; }
    }
}
