﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Zustand_Staedtereisen.Models
{
    public partial class StaedtereisenContext : DbContext
    {
        public StaedtereisenContext()
        {
        }

        public StaedtereisenContext(DbContextOptions<StaedtereisenContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Attraktion> Attraktion { get; set; }
        public virtual DbSet<Bundesland> Bundesland { get; set; }
        public virtual DbSet<Stadt> Stadt { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=Staedtereisen;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Attraktion>(entity =>
            {
                entity.Property(e => e.Anschrift)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Stadt)
                    .WithMany(p => p.Attraktion)
                    .HasForeignKey(d => d.StadtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Attraktio__Stadt__15502E78");
            });

            modelBuilder.Entity<Bundesland>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stadt>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Bundesland)
                    .WithMany(p => p.Stadt)
                    .HasForeignKey(d => d.BundeslandId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Stadt__Bundeslan__1273C1CD");
            });
        }
    }
}
