﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zustand_Staedtereisen.Models
{
    public class CityRatingModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int AttractionCount { get; set; }
        public double ScoreAverage { get; set; }
    }
}
