﻿using System;
using System.Collections.Generic;

namespace Zustand_Staedtereisen.Models
{
    public partial class Bundesland
    {
        public Bundesland()
        {
            Stadt = new HashSet<Stadt>();
        }

        public int BundeslandId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Stadt> Stadt { get; set; }
    }
}
