﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Zustand_Staedtereisen.Models;
using Microsoft.AspNetCore.Http;

namespace Zustand_Staedtereisen.Controllers
{
    public class HomeController : Controller
    {

        IMemoryCache myCache;
        StaedtereisenContext _context;

        public HomeController(StaedtereisenContext context, IMemoryCache cache)
        {
            myCache = cache;
            _context = context;
        }


        public IActionResult Index()
        {

            return View();
        }

        public IActionResult SearchForCity()
        {
            List<string> allStrings = new List<string>();

            if (HttpContext.Session.Keys.Count() > 0)
            {
                allStrings.AddRange(HttpContext.Session.Keys); 
            }

            if (allStrings.Count() > 5)
            {
                allStrings = allStrings.Skip(allStrings.Count() - 5).ToList();
            }


            return View(allStrings);
        }


        public IActionResult ShowCitiesForSearch(string inputText)
        {

            var cities = _context.Stadt;

            var cityForSearch = cities.Where(s => s.Name == inputText).ToList();

            HttpContext.Session.SetString(inputText, inputText);

            return PartialView("_PartialShowCitiesForSearch", cityForSearch);
        }


        public IActionResult GetAttractions(int stadtid)
        {
            var cityAttraction = _context.Attraktion.Where(a => a.StadtId == stadtid);

            return View(cityAttraction.ToList());
        }





        public IActionResult NachBundesland()
        {

            ViewData["selectlist"] = new SelectList(_context.Bundesland, "BundeslandId", "Name");

            return View();
        }

        public IActionResult ShowCitiesForState(int stateid)
        {
            var cities = _context.Stadt;

            var cityForState = cities.Where(s => s.BundeslandId == stateid);

            return PartialView("_PartialShowCitiesForState", cityForState.ToList());
        }


        public IActionResult GetTopTenCities()
        {
            var top10 = _context.Stadt.Select(s => new CityRatingModel
            {
                ID = s.StadtId,
                Name = s.Name,
                AttractionCount = s.Attraktion.Count(),
                ScoreAverage = (s.Attraktion.Count > 0 ? s.Attraktion.Average(b => b.Bewertung) : 0)

            }).OrderByDescending(c => c.ScoreAverage).Take(10);


            return PartialView("_PartialShowTopTenCities", top10);
        }







        public IActionResult Privacy()
        {
            return View();
        }





        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
