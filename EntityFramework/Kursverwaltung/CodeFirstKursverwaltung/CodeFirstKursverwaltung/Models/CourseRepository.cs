﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CodeFirstKursverwaltung.Models
{
    public class CourseRepository
    {
        List<Course> allCourses = new List<Course>();


        public List<Course> GetCoursesFromFile(IFormFile file)
        {
            string zeile = "";

            using (StreamReader sr = new StreamReader(file.OpenReadStream()))
            {
                sr.ReadLine();

                while (sr.EndOfStream == false)
                {
                    zeile = sr.ReadLine();
                    var fields = zeile.Split(";");

                    allCourses.Add(new Course { Name = fields[1], Start = Convert.ToDateTime(fields[2]), Duration = Convert.ToInt32(fields[3])});
                }

            }

                return allCourses;
        }

    }
}
