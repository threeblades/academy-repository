﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationsBooks.Models
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Publisher> Publishers { get; set; }

        public BookContext(DbContextOptions options) :base(options)
        {
            //Database.EnsureCreated();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            

            modelBuilder.Entity<Book>()
                .HasOne(pt => pt.Publisher).WithMany(t => t.Books)
                .HasForeignKey(pt => pt.PublisherID);

          
        }
    }
}
