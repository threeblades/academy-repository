﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VererbungKurse.Models;

namespace VererbungKurse.Controllers
{
    public class OnlineCoursesController : Controller
    {
        private readonly CourseContext _context;

        public OnlineCoursesController(CourseContext context)
        {
            _context = context;
        }

        // GET: OnlineCourses
        public async Task<IActionResult> Index()
        {
            return View(await _context.OnlineCourse.ToListAsync());
        }

        // GET: OnlineCourses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineCourse = await _context.OnlineCourse
                .FirstOrDefaultAsync(m => m.CourseID == id);
            if (onlineCourse == null)
            {
                return NotFound();
            }

            return View(onlineCourse);
        }

        // GET: OnlineCourses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OnlineCourses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CourseURL,CourseID,Name,Certificate")] OnlineCourse onlineCourse)
        {
            if (ModelState.IsValid)
            {
                _context.Add(onlineCourse);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(onlineCourse);
        }

        // GET: OnlineCourses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineCourse = await _context.OnlineCourse.FindAsync(id);
            if (onlineCourse == null)
            {
                return NotFound();
            }
            return View(onlineCourse);
        }

        // POST: OnlineCourses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CourseURL,CourseID,Name,Certificate")] OnlineCourse onlineCourse)
        {
            if (id != onlineCourse.CourseID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(onlineCourse);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OnlineCourseExists(onlineCourse.CourseID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(onlineCourse);
        }

        // GET: OnlineCourses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineCourse = await _context.OnlineCourse
                .FirstOrDefaultAsync(m => m.CourseID == id);
            if (onlineCourse == null)
            {
                return NotFound();
            }

            return View(onlineCourse);
        }

        // POST: OnlineCourses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var onlineCourse = await _context.OnlineCourse.FindAsync(id);
            _context.OnlineCourse.Remove(onlineCourse);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OnlineCourseExists(int id)
        {
            return _context.OnlineCourse.Any(e => e.CourseID == id);
        }
    }
}
