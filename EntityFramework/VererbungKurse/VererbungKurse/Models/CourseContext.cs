﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VererbungKurse.Models;

namespace VererbungKurse.Models
{
    public class CourseContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InhouseCourse>().HasBaseType<Course>();

            modelBuilder.Entity<OnlineCourse>().HasBaseType<Course>();

            modelBuilder.Entity<Course>().HasDiscriminator<string>("CourseType");

        }

        public CourseContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureCreated();
        }

        public DbSet<VererbungKurse.Models.InhouseCourse> InhouseCourse { get; set; }

        public DbSet<VererbungKurse.Models.OnlineCourse> OnlineCourse { get; set; }

    }
}
