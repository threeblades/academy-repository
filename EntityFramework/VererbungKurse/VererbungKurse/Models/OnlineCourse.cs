﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VererbungKurse.Models
{
    public class OnlineCourse : Course
    {
        public string CourseURL { get; set; }
    }
}
