    //In NuGet-Konsole
//Scaffold-DbContext "Server=.\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Tables Projects, Employees

    //in appsettings.json
//,"ConnectionStrings": {"TeachSQLDatabase": "Server=.\\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;"}
    
    //In Startup.cs
//services.AddDbContext<TeachSQLContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TeachSQLDatabase")));