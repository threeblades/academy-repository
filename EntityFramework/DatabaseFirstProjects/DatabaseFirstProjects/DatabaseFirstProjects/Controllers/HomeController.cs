﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DatabaseFirstProjects.Models;

    //In NuGet-Konsole
//Scaffold-DbContext "Server=.\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Tables Projects, Employees

    //in appsettings.json
//,"ConnectionStrings": {"TeachSQLDatabase": "Server=.\\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;"}
    
    //In Startup.cs
//services.AddDbContext<TeachSQLContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TeachSQLDatabase")));



namespace DatabaseFirstProjects.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
