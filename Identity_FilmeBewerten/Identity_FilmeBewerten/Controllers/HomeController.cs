﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Identity_FilmeBewerten.Models;

namespace Identity_FilmeBewerten.Controllers
{
    public class HomeController : Controller
    {
        FilmContext context = new FilmContext();

        public HomeController(FilmContext context)
        {
            this.context = context;
        }

        //GET
        public IActionResult Index()
        {
            

            return View(context.Films.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
