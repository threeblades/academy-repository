﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_FilmeBewerten.Models
{
    public class FilmContext : DbContext
    {
        public DbSet<Film> Films { get; set; }
        public DbSet<Vote> Votes { get; set; }

        public FilmContext(DbContextOptions options):base(options)
        {

        }

        public FilmContext()
        {

        }

    }


}

