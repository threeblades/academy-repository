﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity_FilmeBewerten.Models
{
    public partial class Vote
    {
        public int VoteID { get; set; }

        [Required, Range(1, 10)]
        public int Value { get; set; }

        public virtual Film Film { get; set; }


    }
}