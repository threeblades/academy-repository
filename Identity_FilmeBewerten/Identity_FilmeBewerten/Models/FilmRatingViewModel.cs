﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_FilmeBewerten.Models
{
    public class FilmRatingViewModel
    {
        public string Title { get; set; }

        [DisplayFormat(DataFormatString = "{0:F}")]
        public double Rating { get; set; }




    }
}
