﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_FilmeBewerten.Models
{
    public partial class Film
    {
        public int FilmID { get; set; }

        public string Title { get; set; }

        public DateTime Erscheinungsdatum { get; set; }

        public string Regisseur { get; set; }

        public virtual ICollection<Vote> Bewertungen { get; set; }

    }
}
