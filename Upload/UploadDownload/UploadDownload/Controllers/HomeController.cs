﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UploadDownload.Models;

namespace UploadDownload.Controllers
{
    public class HomeController : Controller
    {
        FileRepository filerepo = new FileRepository();

        [HttpGet]
        public IActionResult Index()
        {
            var allFiles = filerepo.GetAllFiles();

            return View(allFiles);
        }

        [HttpPost]
        public IActionResult Index(IFormFile inputFile)
        {
            filerepo.UploadFile(inputFile);

            var allFiles = filerepo.GetAllFiles();

            return View(allFiles);
        }


        public IActionResult FileDownload(int id, string filename, string filetype)
        {
            Stream fileContent = filerepo.DownloadFile(id);

            return File(fileContent, filetype, filename);
        }



        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
