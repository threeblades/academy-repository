﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadDownload.Models
{
    public class FileClass
    {
        public int ID { get; set; }
        public string DocName { get; set; }
        public string ContentType { get; set; }
        public long ContentLength { get; set; }
        public DateTime InsertionDate { get; set; }

        

    }
}
