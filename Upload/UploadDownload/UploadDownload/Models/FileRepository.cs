﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace UploadDownload.Models
{
    public class FileRepository
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                            "Initial Catalog=NETDB;" +
                                            "Integrated Security=true;";


        public void UploadFile(IFormFile file)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("INSERT INTO dbo.DocStore (DocName, DocData, ContentType, ContentLength, InsertionDate) VALUES (@Name, @Data, @Type, @Length, @Date)", connection);

                    command.Parameters.AddWithValue("@Name", file.FileName);
                    command.Parameters.AddWithValue("@Data", file.OpenReadStream());
                    command.Parameters.AddWithValue("@Type", file.ContentType);
                    command.Parameters.AddWithValue("@Length", file.Length);
                    command.Parameters.AddWithValue("@Date", DateTime.Now);

                    command.ExecuteNonQuery();

                }
                catch (Exception)
                { 


                    throw;
                }
            }

        }

        public List<FileClass> GetAllFiles()
        {
            List<FileClass> allFiles = new List<FileClass>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {

                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT * FROM dbo.DocStore", connection);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        allFiles.Add(new FileClass { ID = reader.GetInt32(0), DocName = reader.GetString(1), ContentType = reader.GetString(3), ContentLength = reader.GetInt64(4), InsertionDate = reader.GetDateTime(5)});

                    }


                }
                catch (Exception)
                {

                    throw;
                }
            }

            return allFiles;
        }


        public Stream DownloadFile(int id)
        {

            Stream fileStream = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {

                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT DocData FROM dbo.DocStore WHERE DocID=@ID", connection);

                    command.Parameters.AddWithValue("@ID", id);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        fileStream = reader.GetStream(0);

                    }


                }
                catch (Exception)
                {

                    throw;
                }
            }

            return fileStream;
        }


    }
}
