﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebAPI_Bundesliga.Models
{
    public class BLRepository
    {
        private const string url = "https://www.openligadb.de/";

        private readonly HttpClient client;




        public BLRepository()
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }




        public List<Day> GetAllDays()
        {
            List<Day> allDays = new List<Day>();

            for (int i = 1; i < 6; i++)
            {
                allDays.Add(new Day { allMatches = GetMatches("api/getmatchdata/bl1/2019/" + i) });
            }

            return allDays;
        }



        public List<Match> GetMatches(string path)
        {
            List<Match> matches = null;

            HttpResponseMessage response = client.GetAsync(path).Result;

            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;

                matches = JsonConvert.DeserializeObject<List<Match>>(result);
            }

            return matches;

        }


    }
}
