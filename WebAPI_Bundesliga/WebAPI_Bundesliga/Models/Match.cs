﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Bundesliga.Models
{
    public class Match
    {
        public int MatchID { get; set; }
        public DateTime MatchDateTime { get; set; }
        public MatchResult[] MatchResults { get; set; }
        public Team Team1 { get; set; }
        public Team Team2 { get; set; }
    }
}
