﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI_Bundesliga.Models
{
    public class MatchResult
    {
        public int PointsTeam1 { get; set; }
        public int PointsTeam2 { get; set; }

    }
}
