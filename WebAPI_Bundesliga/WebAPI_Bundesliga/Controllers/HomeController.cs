﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using WebAPI_Bundesliga.Models;

namespace WebAPI_Bundesliga.Controllers
{
    public class HomeController : Controller
    {
        IMemoryCache memoryCache;

        BLRepository blrepo = new BLRepository();



        public HomeController(IMemoryCache cache)
        {
            memoryCache = cache;

        }



        public IActionResult Index()
        {
            var cacheEntry = memoryCache.GetOrCreate("saison", entry =>
            {
                entry.SetSlidingExpiration(TimeSpan.FromHours(24));

                var allDays = blrepo.GetAllDays();
                
                return allDays;
            });

            return View(cacheEntry);
        }





        public IActionResult Privacy()
        {
            return View();
        }




        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
