﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Bookdata.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Bookdata.Controllers
{
    public class HomeController : Controller
    {
        public BookRepository bookrepo = new BookRepository();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ShowAllBooks()
        {
            return View(bookrepo.GetAllBooks());
        }

        public IActionResult SearchBooks()
        {
            return View();
        }

        public IActionResult ShowSearchBooksResults(string bookSearchString)
        {

            return PartialView("_ShowBooksPartial", bookrepo.SearchBooks(bookSearchString));
        }

        public IActionResult SearchBooksByCategory()
        {

            var categories = bookrepo.GetAllCategories();

            ViewBag.Categories = new SelectList(categories, "CategoryID", "CategoryName");
            

            return View();
        }

        public IActionResult ShowSearchBooksByCategoryResults(int Categories)
        {
            return PartialView("_ShowBooksPartial", bookrepo.SearchBooksByCategory(Categories));
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
