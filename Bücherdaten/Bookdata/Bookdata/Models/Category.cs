﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookdata.Models
{
    public class Category
    {
        public string Categories { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
