﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Bookdata.Models
{
    public class BookRepository
    {
        string connectionString = @"Data Source=.\SQLEXPRESS;" +
                           "Initial Catalog=BooksDB;" +
                           "Integrated Security=true;";

        public List<Book> GetAllBooks()
        {
            List<Book> allBooks = new List<Book>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    var command = new SqlCommand("usp_show_all_books", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)

                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }

            }

            return allBooks;
        }


        public List<Book> SearchBooks(string pattern)
        {
            List<Book> allBooks = new List<Book>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    var command = new SqlCommand("usp_filter_books", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("filter", pattern);
                    

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)

                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }

            }


            return allBooks;
        }

        public List<Book> SearchBooksByCategory(int id)
        {
            List<Book> allBooks = new List<Book>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    var command = new SqlCommand("usp_books_by_categoryid", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("CategoryID", id);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)

                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }

            }


            return allBooks;
        }

        public List<Category> GetAllCategories()
        {
            List<Category> allCategories = new List<Category>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    var command = new SqlCommand("SELECT CategoryID, CategoryName FROM Categories", connection);
                    

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        allCategories.Add(new Category
                        {
                            CategoryID = reader.GetInt32(0),
                            CategoryName = reader.GetString(1)
                            
                        });

                    }


                }
                catch (Exception)
                {

                    throw;
                }

            }


            return allCategories;
        }

    }
}
