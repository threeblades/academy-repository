﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bookdata.Models
{
    public class Book
    {
        public string CategoryName { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
    }
}
