﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ConsoleClient
{
    class Program
    {
        private const string url = "http://localhost:62656";


        static void Main(string[] args)
        {
            HttpClient client = InitializeClient();

            HttpResponseMessage responseMessage = client.GetAsync("api/BLZ").Result;
            
            if (responseMessage.IsSuccessStatusCode)
            {
                string result = responseMessage.Content.ReadAsStringAsync().Result;

                Console.WriteLine(result);

                List<BankInfo> bi = JsonConvert.DeserializeObject<List<BankInfo>>(result);

                Console.WriteLine(string.Join("\n", bi));
            }

            Console.ReadLine();
        }



        private static HttpClient InitializeClient()
        {
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();
            
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

    }
}
