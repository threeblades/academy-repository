﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleClient
{
    class BankInfo
    {
        
        public string BLZ { get; set; }
        public string BIC { get; set; }
        public string Bezeichnung { get; set; }
        public int? PLZ { get; set; }
        public string Ort { get; set; }

        public override string ToString()
        {
            return string.Format("{0} : {1} : {2} : {3} : {4}", BLZ, BIC, Bezeichnung, PLZ, Ort);
        }

    }
}
