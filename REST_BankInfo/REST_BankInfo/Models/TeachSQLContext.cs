﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using REST_BankInfo.Models;

namespace REST_BankInfo.Models
{
    public partial class TeachSQLContext : DbContext
    {
        public TeachSQLContext()
        {
        }

        public TeachSQLContext(DbContextOptions<TeachSQLContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blztabelle> Blztabelle { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Blztabelle>(entity =>
            {
                entity.ToTable("BLZTabelle");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Aenderungskennzeichen)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Bezeichnung)
                    .HasMaxLength(58)
                    .IsUnicode(false);

                entity.Property(e => e.Bic)
                    .HasColumnName("BIC")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Blz)
                    .HasColumnName("BLZ")
                    .HasMaxLength(50);

                entity.Property(e => e.Kurzbezeichnung)
                    .HasMaxLength(27)
                    .IsUnicode(false);

                entity.Property(e => e.NachfolgeBlz)
                    .HasColumnName("NachfolgeBLZ")
                    .HasMaxLength(50);

                entity.Property(e => e.Ort)
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.Pan).HasColumnName("PAN");

                entity.Property(e => e.Plz).HasColumnName("PLZ");

                entity.Property(e => e.Pruefzifferberechnungsmethode)
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });
        }

        
    }
}
