﻿using System;
using System.Collections.Generic;

namespace REST_BankInfo.Models
{
    public partial class Blztabelle
    {
        public string Blz { get; set; }
        public short? Merkmal { get; set; }
        public string Bezeichnung { get; set; }
        public int? Plz { get; set; }
        public string Ort { get; set; }
        public string Kurzbezeichnung { get; set; }
        public int? Pan { get; set; }
        public string Bic { get; set; }
        public string Pruefzifferberechnungsmethode { get; set; }
        public int? Datensatznummer { get; set; }
        public string Aenderungskennzeichen { get; set; }
        public short? Bankleitzahlloeschung { get; set; }
        public string NachfolgeBlz { get; set; }
        public int Id { get; set; }
    }
}
