﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace REST_BankInfo.Models
{
    public class BankInfo
    {
        
        public string BLZ { get; set; }
        public string BIC { get; set; }
        public string Bezeichnung { get; set; }
        public int? PLZ { get; set; }
        public string Ort { get; set; }
    }
}
