﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using REST_BankInfo.Models;

namespace REST_BankInfo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BLZ2Controller : ControllerBase
    {
        private readonly TeachSQLContext _context;

        public BLZ2Controller(TeachSQLContext context)
        {
            _context = context;

        }


        // GET: api/BLZ
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BankInfo>>> GetBankInfo()
        {
            var blztabelle = _context.Blztabelle;

            List<BankInfo> allBankInfo = new List<BankInfo>();

            foreach (var item in blztabelle)
            {
                allBankInfo.Add(new BankInfo { Bezeichnung = item.Bezeichnung, BIC = item.Bic, BLZ = item.Blz, Ort = item.Ort, PLZ = item.Plz });
            }


            return allBankInfo;
        }

        // GET: api/BLZ/5
        [HttpGet("{blz}")]
        public async Task<ActionResult<BankInfo>> GetBankInfo(string blz)
        {
            var blztabelle = _context.Blztabelle;

            List<BankInfo> allBankInfo = new List<BankInfo>();

            foreach (var item in blztabelle)
            {
                allBankInfo.Add(new BankInfo {Bezeichnung = item.Bezeichnung, BIC = item.Bic, BLZ = item.Blz, Ort = item.Ort, PLZ = item.Plz });
            }

            BankInfo bi = allBankInfo.FirstOrDefault(x => x.BLZ == blz);

            if (bi == null)
            {
                return NotFound();
            }


            return Ok(bi);
        }




    }
}