﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT_Umfrage.Models
{
    public class SurveyUserModel
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public virtual Survey Survey { get; set; }
    }
}
