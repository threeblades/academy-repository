﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IT_Umfrage.Models
{
    public class Answer
    {
        [Key]
        public int AnswerID { get; set; }
        public string AnswerText { get; set; }
        public int SurveyID { get; set; }
        public int Votes { get; set; }
    }
}
