﻿// <auto-generated />
using System;
using IT_Umfrage.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace IT_Umfrage.Migrations
{
    [DbContext(typeof(SurveyDbContext))]
    [Migration("20190805115330_MSSQLLinux2")]
    partial class MSSQLLinux2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("IT_Umfrage.Models.Answer", b =>
                {
                    b.Property<int>("AnswerID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AnswerText");

                    b.Property<int>("SurveyID");

                    b.Property<int>("Votes");

                    b.HasKey("AnswerID");

                    b.HasIndex("SurveyID");

                    b.ToTable("Answers");
                });

            modelBuilder.Entity("IT_Umfrage.Models.Survey", b =>
                {
                    b.Property<int>("SurveyID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("SurveyTitle");

                    b.HasKey("SurveyID");

                    b.ToTable("Surveys");
                });

            modelBuilder.Entity("IT_Umfrage.Models.SurveyUserModel", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("SurveyID");

                    b.Property<string>("UserID");

                    b.HasKey("ID");

                    b.HasIndex("SurveyID");

                    b.ToTable("SurveyUserModels");
                });

            modelBuilder.Entity("IT_Umfrage.Models.Answer", b =>
                {
                    b.HasOne("IT_Umfrage.Models.Survey")
                        .WithMany("Answers")
                        .HasForeignKey("SurveyID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("IT_Umfrage.Models.SurveyUserModel", b =>
                {
                    b.HasOne("IT_Umfrage.Models.Survey", "Survey")
                        .WithMany()
                        .HasForeignKey("SurveyID");
                });
#pragma warning restore 612, 618
        }
    }
}
