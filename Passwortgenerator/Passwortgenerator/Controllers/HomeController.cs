﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Passwortgenerator.Models;

namespace Passwortgenerator.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(int pwl, int bl, int nl, int sl)
        {

            PasswordGenerator pwg = new PasswordGenerator();

            pwg.pwLength = pwl;
            pwg.bigLettersLength = bl;
            pwg.numbersLength = nl;
            pwg.specialCharactersLength = sl;

            pwg.GeneratePassword();

            ViewBag.FinalPasswordFinal = pwg.FinalPassword;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
