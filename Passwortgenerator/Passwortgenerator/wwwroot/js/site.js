﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

window.addEventListener('load', function () {
    console.log('All assets are loaded');

    var button = document.getElementById("submitbutton");

    button.addEventListener('click', function () {

        var pwlength = document.getElementById("pwlengthid").value;

        var biglength = document.getElementById("biglengthid").value;

        var numberlength = document.getElementById("numberlengthid").value;

        var speciallength = document.getElementById("speciallengthid").value;

        $.post("/Home/Index", { pwl: pwlength, bl: biglength, nl: numberlength, sl: speciallength });

    });

});
