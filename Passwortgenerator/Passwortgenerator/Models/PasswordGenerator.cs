﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Diagnostics;

namespace Passwortgenerator.Models
{
    public class PasswordGenerator
    {
        public string FinalPassword { get; set; }

        public Random randomChars = new Random();
        public Random randomIndex = new Random();

        public int randomIntIndex;
        public int pwLength = 8;
        public int bigLettersLength = 1;
        public int numbersLength = 1;
        public int specialCharactersLength = 1;

        public void GeneratePassword()
        {
            char[] newPassword = new char[pwLength];
            char[] tempPassword = new char[pwLength];


            for (int i = 0; i < pwLength; i++)
            {
                newPassword[i] = (char)randomChars.Next(97, 122);
            }


            for (int i = 0; i < bigLettersLength; i++)
            {
                newPassword[randomIndex.Next(i, pwLength)] = (char)randomChars.Next(65, 90);
            }


            for (int i = 0; i < numbersLength; i++)
            {
                newPassword[randomIndex.Next(i, pwLength)] = (char)randomChars.Next(48, 57);
            }


            for (int i = 0; i < specialCharactersLength; i++)
            {
                newPassword[randomIndex.Next(i, pwLength)] = (char)randomChars.Next(33, 47);
            }


            for (int i = 0; i < pwLength; i++)
            {
                randomIntIndex = randomIndex.Next(i, pwLength);
                tempPassword[i] = newPassword[i];
                newPassword[i] = newPassword[randomIntIndex];
                newPassword[randomIntIndex] = tempPassword[i];

            }

            FinalPassword = string.Join("", newPassword);

        }

    }
}
