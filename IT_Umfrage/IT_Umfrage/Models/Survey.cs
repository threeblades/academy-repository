﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IT_Umfrage.Models
{
    public class Survey
    {
        [Key]
        public int SurveyID { get; set; }
        public string SurveyTitle { get; set; }

        public virtual ICollection<Answer> Answers {get; set; }


    }
}
