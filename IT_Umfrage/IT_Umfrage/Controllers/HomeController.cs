﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IT_Umfrage.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace IT_Umfrage.Controllers
{
    public class HomeController : Controller
    {
        private readonly SurveyDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;


        public HomeController(SurveyDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        public IActionResult Index()
        {
            return View(_context.Surveys.ToList());
        }

        [Authorize]
        public IActionResult AttendSurvey(int id)
        {
            var survey = _context.Surveys.Where(s => s.SurveyID == id).First();

            return View(survey);
        }


        [HttpPost]
        public IActionResult ShowSurveyAnswers(int surveyid, int answerid)
        {
            var survey = _context.Surveys.Where(s => s.SurveyID == surveyid).First();

            Answer answer = _context.Answers.Where(s => s.AnswerID == answerid).First();

            IdentityUser identityUser = _userManager.GetUserAsync(HttpContext.User).Result;

            SurveyUserModel sum = _context.SurveyUserModels.Where(s => s.Survey.SurveyID == survey.SurveyID && s.UserID == identityUser.Id).FirstOrDefault();

            if (sum == null)
            {

                SurveyUserModel surveyUserModelNew = new SurveyUserModel { UserID = identityUser.Id, Survey = survey};
                answer.Votes++;

                _context.SurveyUserModels.Add(surveyUserModelNew);
                _context.SaveChanges();

            }


            return View(survey);
        }


        [HttpGet]
        public IActionResult ShowSurveyAnswers(int id)
        {
            var survey = _context.Surveys.Where(s => s.SurveyID == id).First();

            return View(survey);
        }


            public IActionResult Privacy()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
