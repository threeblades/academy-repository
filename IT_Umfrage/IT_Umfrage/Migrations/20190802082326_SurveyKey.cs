﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IT_Umfrage.Migrations
{
    public partial class SurveyKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Surveys_SurveyID",
                table: "Answers");

            migrationBuilder.AlterColumn<int>(
                name: "SurveyID",
                table: "Answers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Surveys_SurveyID",
                table: "Answers",
                column: "SurveyID",
                principalTable: "Surveys",
                principalColumn: "SurveyID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_Surveys_SurveyID",
                table: "Answers");

            migrationBuilder.AlterColumn<int>(
                name: "SurveyID",
                table: "Answers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_Surveys_SurveyID",
                table: "Answers",
                column: "SurveyID",
                principalTable: "Surveys",
                principalColumn: "SurveyID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
