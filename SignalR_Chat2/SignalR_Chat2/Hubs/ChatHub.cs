﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_Chat2.Hubs
{
    public class ChatHub : Hub
    {

        public static Dictionary<string, string> allChatUser = new Dictionary<string, string>();

        public async void SignOn(string chatUser)
        {
            allChatUser.Add(Context.ConnectionId, chatUser);

            await Clients.All.SendAsync("handleSignOn", allChatUser);

        }



        public async void BroadcastMsg(string msg)
        {


            await Clients.All.SendAsync("recieveMsg", msg);

        }




        public async void SendMsg(string user, string msg)
        {
            string userID = user;

            await Clients.Caller.SendAsync("recieveMsg", msg);
            await Clients.Client(userID).SendAsync("recieveMsg", msg);


        }


        public async override Task OnDisconnectedAsync(Exception exception)
        {
            string userID = Context.ConnectionId;

            allChatUser.Remove(userID);

            await Clients.Others.SendAsync("handleSignOff", userID);
        }



    }
}
