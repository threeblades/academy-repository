﻿
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/chathub")
    .build();

var chatUser;




window.onload = function () {

    connection.start().then(initUser);

};




function initUser() {

    chatUser = prompt("Name bitte:");

    if (chatUser == "" || chatUser == null) {

        chatUser = "DefaultUser" + Math.ceil(Math.random() * 100);
    }

    connection.invoke("SignOn", chatUser);

    startChat();
};





function startChat() {



    connection.on("recieveMsg", message => {

        $("#allmessages").append(message + "\n");


    });



    connection.on("handleSignOn", allUser => {

        $("#chatuserActual").html("Chatuser of this Instance: " + chatUser);

        $("#allChatUsers").empty();


        for (var i in allUser) {

            $("#allChatUsers").append(`<option value="${i}">${allUser[i]}</option>`);
            
        }
        
    });


    connection.on("handleSignOff", userID => {

        $("#allChatUsers option[value='" + userID + "']").remove();


    });




    $("#messagebutton").click(function () {

        var textmessage = chatUser + " sagt: " + $("#messagetext").val();

        var userSelected = $("#allChatUsers option:selected").val();



        if (userSelected != null) {


            connection.invoke("SendMsg", userSelected, textmessage);
        }
        else {

            connection.invoke("BroadcastMsg", textmessage);
            
        }

        

    });


};

function clearSelected() {

    var elements = document.getElementById("allChatUsers").selectedOptions;

    for (var i = 0; i < elements.length; i++) {
        elements[i].selected = false;
    }
}