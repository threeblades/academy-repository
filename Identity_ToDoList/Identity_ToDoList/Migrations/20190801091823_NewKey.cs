﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity_ToDoList.Migrations
{
    public partial class NewKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_ToDoLists_ToDoListID",
                table: "Assignments");

            migrationBuilder.AlterColumn<int>(
                name: "ToDoListID",
                table: "Assignments",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_ToDoLists_ToDoListID",
                table: "Assignments",
                column: "ToDoListID",
                principalTable: "ToDoLists",
                principalColumn: "ToDoListID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assignments_ToDoLists_ToDoListID",
                table: "Assignments");

            migrationBuilder.AlterColumn<int>(
                name: "ToDoListID",
                table: "Assignments",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Assignments_ToDoLists_ToDoListID",
                table: "Assignments",
                column: "ToDoListID",
                principalTable: "ToDoLists",
                principalColumn: "ToDoListID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
