﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_ToDoList.Models
{
    public class ToDoList
    {
        public int ToDoListID { get; set; }
        public string UserName { get; set; }
        public IdentityUser UserID { get; set; }

        public virtual ICollection<Assignment> Assignments { get; set; }

    }
}
