﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_ToDoList.Models
{
    public class Assignment
    {

        public int AssignmentID { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime CompletionDate { get; set; }
        public string Finished { get; set; } = "offen";


        public int ToDoListID { get; set; }

        [ForeignKey("ToDoListID")]
        public ToDoList ToDoList { get; set; }

    }
}
