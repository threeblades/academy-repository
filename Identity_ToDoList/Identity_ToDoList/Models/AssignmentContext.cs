﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_ToDoList.Models
{
    public class AssignmentContext : DbContext
    {
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<ToDoList> ToDoLists { get; set; }

        public AssignmentContext(DbContextOptions options) : base(options)
        {

        }

        public AssignmentContext()
        {

        }

    }
}
