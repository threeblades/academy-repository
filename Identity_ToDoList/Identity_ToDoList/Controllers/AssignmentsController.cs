﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity_ToDoList.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Identity_ToDoList.Controllers
{
    
    public class AssignmentsController : Controller
    {
        private readonly AssignmentContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public AssignmentsController(AssignmentContext context, UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        // GET: Assignments
        [Authorize]
        public async Task<IActionResult> Index()
        {


            var listPerUser = _context.ToDoLists.Include(y => y.Assignments).Where(x => x.UserID == _userManager.GetUserAsync(HttpContext.User).Result).FirstOrDefault();

            if (listPerUser == null)
            {
                _context.ToDoLists.Add(new ToDoList { UserID = _userManager.GetUserAsync(HttpContext.User).Result });

                _context.SaveChanges();

                return View(new List<Assignment>());
            }

            var assignmentsPerUser = listPerUser.Assignments;

            return View(assignmentsPerUser);
        }

        // GET: Assignments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assignments
                .FirstOrDefaultAsync(m => m.AssignmentID == id);
            if (assignment == null)
            {
                return NotFound();
            }

            return View(assignment);
        }

        // GET: Assignments/Create
        public IActionResult Create()
        {
            ViewBag.ToDoListID = _context.ToDoLists.Where(x => x.UserID == _userManager.GetUserAsync(HttpContext.User).Result).Select(z => z.ToDoListID).First();

            return View();
        }

        // POST: Assignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AssignmentID,ToDoListID,Name,CompletionDate")] Assignment assignment)
        {
            

            if (ModelState.IsValid)
            {
                _context.Add(assignment);
                
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(assignment);
        }

        // GET: Assignments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.ToDoListID = _context.ToDoLists.Where(x => x.UserID == _userManager.GetUserAsync(HttpContext.User).Result).Select(z => z.ToDoListID).First();

            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assignments.FindAsync(id);
            if (assignment == null)
            {
                return NotFound();
            }
            return View(assignment);
        }

        // POST: Assignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AssignmentID,ToDoListID,Name,CreateDate,CompletionDate,Finished")] Assignment assignment)
        {
            if (id != assignment.AssignmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assignment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssignmentExists(assignment.AssignmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(assignment);
        }

        // GET: Assignments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assignments
                .FirstOrDefaultAsync(m => m.AssignmentID == id);
            if (assignment == null)
            {
                return NotFound();
            }

            return View(assignment);
        }

        // POST: Assignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var assignment = await _context.Assignments.FindAsync(id);
            _context.Assignments.Remove(assignment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssignmentExists(int id)
        {
            return _context.Assignments.Any(e => e.AssignmentID == id);
        }


        public IActionResult ShowOpen()
        {
            var userlist = _context.ToDoLists.Where(x => x.UserID == _userManager.GetUserAsync(HttpContext.User).Result).Select(z => z.Assignments).First();

            var allOpenAssignments = userlist.Where(x => x.Finished == "open");

            return View(allOpenAssignments.ToList());
        }

        public IActionResult ShowClosed()
        {
            var userlist = _context.ToDoLists.Where(x => x.UserID == _userManager.GetUserAsync(HttpContext.User).Result).Select(z => z.Assignments).First();

            var allClosedAssignments = userlist.Where(x => x.Finished == "closed");

            return View(allClosedAssignments.ToList());
        }




    }
}
