﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HtmlHelperProgrammierer.Models
{
    public class Programmierer
    {

        [Display(Name = "Vorname")]
        public string Vorname { get; set; }

        [Display(Name = "Nachname")]
        public string Nachname { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Geschlecht")]
        public string Geschlecht { get; set; }

        public Aufgabe Tätigkeit { get; set; }

        public List<Projekt> alleProjekte { get; set; }

    }
}
