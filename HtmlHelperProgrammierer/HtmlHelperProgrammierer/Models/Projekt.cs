﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HtmlHelperProgrammierer.Models
{
    public class Projekt
    {
        [Display(Name = "Projekt")]
        public string ProjektName { get; set; }

    }
}
