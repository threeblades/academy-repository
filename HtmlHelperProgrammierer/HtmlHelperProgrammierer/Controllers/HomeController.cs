﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HtmlHelperProgrammierer.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HtmlHelperProgrammierer.Controllers
{
    public class HomeController : Controller
    {
        List<Aufgabe> alleAufgaben = new List<Aufgabe>();

        public IActionResult Index()
        {
            

            alleAufgaben.Add(new Aufgabe { AufgabeName = "Programmierer" });
            alleAufgaben.Add(new Aufgabe { AufgabeName = "Fisi" });
            alleAufgaben.Add(new Aufgabe { AufgabeName = "Ausputzer" });

            ViewData["AufgabenListe"] = new SelectList(alleAufgaben, "AufgabeName", "AufgabeName");

            return View();
        }

        [HttpPost]
        public IActionResult Index(Programmierer programmierer, string AufgabenListe)
        {

            ViewData["AufgabenListe"] = new SelectList(alleAufgaben, "AufgabeName", "AufgabeName", AufgabenListe);
           
            ViewBag.PrgVorname = programmierer.Vorname;
            ViewBag.PrgNachname = programmierer.Nachname;
            ViewBag.Email = programmierer.Email;
            ViewBag.Geschlecht = programmierer.Geschlecht;
            programmierer.Tätigkeit = alleAufgaben.Find(t => t.AufgabeName == AufgabenListe);
            ViewBag.Aufgabe = programmierer.Tätigkeit.AufgabeName;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
