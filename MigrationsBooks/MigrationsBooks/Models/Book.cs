﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MigrationsBooks.Models
{
    public class Book
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Pages { get; set; }
        public string Language { get; set; }
        public int? PublisherID { get; set; }

        public virtual Publisher Publisher { get; set; }

    }
}
