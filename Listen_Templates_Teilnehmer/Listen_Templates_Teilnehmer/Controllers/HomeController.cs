﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Listen_Templates_Teilnehmer.Models;
using Microsoft.Extensions.Caching.Memory;

namespace Listen_Templates_Teilnehmer.Controllers
{
    public class HomeController : Controller
    {
        IMemoryCache myCache;

        static List<Teilnehmer> teilnehmers = new List<Teilnehmer> {
            new Teilnehmer { Id= 1, Vorname = "Phil", Nachname = "Stöte", AnzahlFehltage = 33},
            new Teilnehmer { Id = 2, Vorname = "Chris", Nachname = "Schanz", AnzahlFehltage = 42 },
            new Teilnehmer { Id = 4, Vorname = "Magnus", Nachname = "Fieberg", AnzahlFehltage = 25 },
            new Teilnehmer { Id = 3, Vorname = "Alex", Nachname = "Bartel", AnzahlFehltage = 12 }};

        

        public HomeController(IMemoryCache cache)
        {
            myCache = cache;
            var cacheEntry = myCache.GetOrCreate("teiln", entry => teilnehmers);
        }


        public IActionResult Index()
        {
            var cacheEntry = myCache.GetOrCreate("teiln", entry => teilnehmers);

            return View(cacheEntry);
        }



        public IActionResult Anwesenheit()
        {
            var cacheEntry = myCache.GetOrCreate("teiln", entry => teilnehmers);

            return View(cacheEntry);
        }



        [HttpPost]
        public IActionResult Anwesenheit(ICollection<Teilnehmer> teilnehmer)
        {
            var cacheEntry = myCache.GetOrCreate("teiln", entry => teilnehmers);

            foreach (var item in teilnehmer)
            {
                if (item.Anwesend == false)
                {
                    cacheEntry.Where(e => e.Id == item.Id).SingleOrDefault().AnzahlFehltage++;
                }
            }

            return RedirectToAction("Index");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
