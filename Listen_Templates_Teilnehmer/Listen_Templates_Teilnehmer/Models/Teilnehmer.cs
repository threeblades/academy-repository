﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Listen_Templates_Teilnehmer.Models
{
    public class Teilnehmer
    {
        public int Id { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }


        public bool Anwesend { get; set; }

        private int anzahlFehltage;

        public int AnzahlFehltage
        {
            get { return anzahlFehltage; }
            set { anzahlFehltage = value; }
        }


    }
}
