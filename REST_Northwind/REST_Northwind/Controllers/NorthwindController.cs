﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NorthwindClassLibrary;
using REST_Northwind.Models;

namespace REST_Northwind.Controllers
{
    [Route("api/nw")]
    [ApiController]
    public class NorthwindController : ControllerBase
    {
        private readonly NorthwindContext _context;

        public NorthwindController(NorthwindContext context)
        {
            _context = context;
        }



        [HttpGet]
        public ActionResult<IEnumerable<Customers>> GetCustomers()
        {
            var customers = _context.Customers;

            return Ok(customers);
        }



        [HttpGet("{customerid}")]
        public ActionResult<IEnumerable<Customers>> GetCustomerById(string customerid)
        {
            var customers = _context.Customers.Where(c => c.CustomerId == customerid).FirstOrDefault();

            if (customers == null)
            {
                return NotFound("Nothing was found!");
            }

            return Ok(customers);
        }



        [HttpGet("country/{country}")]
        public ActionResult<IEnumerable<Customers>> GetCustomersByCountry(string country)
        {
            var customers = _context.Customers.Where(c => c.Country == country);

            if (customers == null)
            {
                return NotFound("Nothing was found!");
            }

            return Ok(customers.ToList());
        }



        [HttpGet("orders/{orderid}")]
        public ActionResult<IEnumerable<Orders>> GetOrdersById(string orderid)
        {
            var orders = _context.Orders.Where(c => c.CustomerId == orderid);

            if (orders == null)
            {
                return NotFound("Nothing was found!");
            }

            return Ok(orders.ToList());
        }



        [HttpGet("orderprice/{orderid}")]
        public ActionResult<IEnumerable<OrderDetails>> GetOrderPrice(int orderid)
        {
            var orderprice = _context.OrderDetails.Where(c => c.OrderId == orderid);

            if (orderprice == null)
            {
                return NotFound("Nothing was found!");
            }

            return Ok(orderprice.ToList());
        }


    }
}