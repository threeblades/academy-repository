﻿using Newtonsoft.Json;
using NorthwindClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace REST_Northwind_MVC.Models
{
    public class NorthwindRepository
    {
        private const string url = "http://localhost:50987";

        private readonly HttpClient client;

        public NorthwindRepository()
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }



        public Customers GetCustomerById(string customerid)
        {
            HttpResponseMessage responseMessage = client.GetAsync("api/nw/" + customerid).Result;

            return DeserializeRequest<Customers>(responseMessage);
        }



        public List<Customers> GetCustomersByCountry(string country)
        {
            HttpResponseMessage responseMessage = client.GetAsync("api/nw/country/" + country).Result;

            return DeserializeRequest<List<Customers>>(responseMessage);
        }



        public List<Orders> GetOrdersById(string customerid)
        {
            HttpResponseMessage responseMessage = client.GetAsync("api/nw/orders/" + customerid).Result;

            return DeserializeRequest<List<Orders>>(responseMessage);
        }




        public List<OrderDetails> GetPrice(int orderid)
        {
            HttpResponseMessage responseMessage = client.GetAsync("api/nw/orderprice/" + orderid).Result;

            return DeserializeRequest<List<OrderDetails>>(responseMessage);
        }






        private static T DeserializeRequest<T>(HttpResponseMessage responseMessage) where T : class
        {
            
            if (responseMessage.IsSuccessStatusCode)
            {
                string result = responseMessage.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<T>(result);
            }

            return null;
        }


    }
}
