﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using REST_Northwind_MVC.Models;
using NorthwindClassLibrary;


namespace REST_Northwind_MVC.Controllers
{
    public class HomeController : Controller
    {
        NorthwindRepository repo = new NorthwindRepository();

        public IActionResult Index()
        {
            return View();
        }


        public IActionResult GetCustomerById(string customerid)
        {
            var customer = repo.GetCustomerById(customerid);

            return View(customer);
        }

        public IActionResult GetCustomersByCountry(string country)
        {
            var customers = repo.GetCustomersByCountry(country);

            return View(customers);
        }

        public IActionResult GetOrdersById(string customerid)
        {
            var orders = repo.GetOrdersById(customerid);

            return View(orders);
        }

        public IActionResult GetPrice(int orderid)
        {
            var orderdetails = repo.GetPrice(orderid);

            var oneorderdetail = repo.GetPrice(orderid).FirstOrDefault();

            ViewBag.Price = orderdetails.Sum(o => o.Quantity * o.UnitPrice);

            return View(oneorderdetail);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
