﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CachingWeather.Models;
using Microsoft.Extensions.Caching.Memory;

namespace CachingWeather.Controllers
{
    public class HomeController : Controller
    {

        RegionDataRepo repo = new RegionDataRepo();

        private IMemoryCache myCache;


        //
        //MEMORY CACHE IN CONTROLLER ÜBERGEBEN
        public HomeController(IMemoryCache weatherCache)
        {
            myCache = weatherCache;
        }


        //
        //INDEX MIT CACHE ENTRY
        public IActionResult Index()
        {


            //MIT TRY GET ---- DIESER LÄUFT GERADE
            bool ifExist = myCache.TryGetValue("WeatherCache2", out repo.allRegionData);

            if (ifExist == false)
            {
                repo.GetFullPageString();
                repo.GetEverything();

                myCache.Set("WeatherCache2", repo.allRegionData, new MemoryCacheEntryOptions {

                    AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(30),
                    SlidingExpiration = TimeSpan.FromDays(1)


                });
                
            }

            //MIT GET OR CREATE
            var cacheEntry = myCache.GetOrCreate("WeatherCache", entry =>
            {
                repo.GetFullPageString();

                repo.GetEverything();

                entry.SlidingExpiration = TimeSpan.FromMinutes(30);

                //ViewData["AllRegionData"] = repo.allRegionData;

                return repo.allRegionData;
            });


            return View(repo.allRegionData);
        }


        //
        //UPDATE FÜR UPDATE BUTTON
        public IActionResult UpdateWeather()
        {
            myCache.Remove("WeatherCache");

            return RedirectToAction("Index");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
