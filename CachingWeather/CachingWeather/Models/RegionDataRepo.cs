﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CachingWeather.Models
{
    public class RegionDataRepo
    {
        public string path = "http://opendata.dwd.de/weather/text_forecasts/txt/";

        public Stream pageStream;

        public WebClient webClient = new WebClient();

        public List<RegionData> allRegionData = new List<RegionData>();



        public void GetFullPageString()
        {
            webClient.Encoding = Encoding.UTF7;

            pageStream = webClient.OpenRead(path);
        }



        public void GetEverything()
        {
            using (StreamReader reader = new StreamReader(pageStream))
            {
                while (reader.EndOfStream != true)
                {
                    string line = reader.ReadLine();

                    Regex regex = new Regex("VHDL");

                    if (regex.IsMatch(line))
                    {
                        string[] splitString = line.Split(">");


                        string link = splitString[1].Substring(0, splitString[1].IndexOf("<"));

                        string date = splitString[2].Substring(0, splitString[2].IndexOf(":") + 3).Trim();

                        string fullPath = Path.Combine(path, link);

                        string fullContent = webClient.DownloadString(fullPath);

                        try
                        {


                            string[] location = fullContent.Split("Warnlage für ");

                            location[1] = location[1].Substring(0, location[1].IndexOf("\r"));

                            string regionName = location[1];

                            allRegionData.Add(new RegionData { Link = fullPath, WeatherDate = Convert.ToDateTime(date), RegionName = regionName, FullContent = fullContent, DownloadDate = DateTime.Now });


                        }
                        catch (Exception e)
                        {


                            Console.WriteLine(e.Message);
                        }

                    }

                }

            }

        }

    }
}
