﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingWeather.Models
{
    public class RegionData
    {
        public string Link { get; set; }
        public DateTime WeatherDate { get; set; }
        public string RegionName { get; set; }
        public string FullContent { get; set; }
        public DateTime DownloadDate { get; set; }

    }
}
