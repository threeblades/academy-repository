﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_QuickSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] intArray = new int[] {12, 34, 4466, 933, 123, 1, 0, 5555, 555};

            DoQuickSort(intArray, 0, (intArray.Length - 1));

            Console.WriteLine(string.Join(", ", intArray));

        }


        public static void DoQuickSort(int[] intArray, int left, int right)
        {
            int i = left;
            int j = right;

            int pivot = intArray[(left+right) / 2];

            while (i <= j)
            {

                while (intArray[i] < pivot)
                {
                    i++;
                }

                while (intArray[j] > pivot)
                {
                    j--;
                }

                if(i <= j)
                {
                    int temp = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = temp;
                    i++;
                    j--;
                }

            }


            if(left < j)
            {
                DoQuickSort(intArray, left, j);
            }


            if(i < right)
            {
                DoQuickSort(intArray, i, right);
            }


        }

    }
}
