﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VererbungKurse.Migrations
{
    public partial class FullDiscriminator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Discriminator",
                table: "Courses",
                newName: "CourseType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CourseType",
                table: "Courses",
                newName: "Discriminator");
        }
    }
}
