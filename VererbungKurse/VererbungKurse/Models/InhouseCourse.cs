﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VererbungKurse.Models
{
    public class InhouseCourse : Course
    {
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public int Duration { get; set; }
    }
}
