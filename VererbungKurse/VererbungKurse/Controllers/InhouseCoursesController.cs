﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VererbungKurse.Models;

namespace VererbungKurse.Controllers
{
    public class InhouseCoursesController : Controller
    {
        private readonly CourseContext _context;

        public InhouseCoursesController(CourseContext context)
        {
            _context = context;
        }

        // GET: InhouseCourses
        public async Task<IActionResult> Index()
        {
            return View(await _context.InhouseCourse.ToListAsync());
        }

        // GET: InhouseCourses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inhouseCourse = await _context.InhouseCourse
                .FirstOrDefaultAsync(m => m.CourseID == id);
            if (inhouseCourse == null)
            {
                return NotFound();
            }

            return View(inhouseCourse);
        }

        // GET: InhouseCourses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InhouseCourses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Location,StartDate,Duration,CourseID,Name,Certificate")] InhouseCourse inhouseCourse)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inhouseCourse);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(inhouseCourse);
        }

        // GET: InhouseCourses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inhouseCourse = await _context.InhouseCourse.FindAsync(id);
            if (inhouseCourse == null)
            {
                return NotFound();
            }
            return View(inhouseCourse);
        }

        // POST: InhouseCourses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Location,StartDate,Duration,CourseID,Name,Certificate")] InhouseCourse inhouseCourse)
        {
            if (id != inhouseCourse.CourseID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inhouseCourse);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InhouseCourseExists(inhouseCourse.CourseID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(inhouseCourse);
        }

        // GET: InhouseCourses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inhouseCourse = await _context.InhouseCourse
                .FirstOrDefaultAsync(m => m.CourseID == id);
            if (inhouseCourse == null)
            {
                return NotFound();
            }

            return View(inhouseCourse);
        }

        // POST: InhouseCourses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inhouseCourse = await _context.InhouseCourse.FindAsync(id);
            _context.InhouseCourse.Remove(inhouseCourse);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InhouseCourseExists(int id)
        {
            return _context.InhouseCourse.Any(e => e.CourseID == id);
        }
    }
}
