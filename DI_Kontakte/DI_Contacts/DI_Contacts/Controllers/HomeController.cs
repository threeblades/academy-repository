﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DI_Contacts.Models;

namespace DI_Contacts.Controllers
{
    public class HomeController : Controller
    {
        private IContactRepo contactRepo;

        //Konstruktor für DI
        public HomeController(IContactRepo contactRepo)
        {

            this.contactRepo = contactRepo;

        }


        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public IActionResult Index(Contact contact)
        {

            contactRepo.InsertContact(contact);
           
            return View(contact);
        }


        public IActionResult ShowAll()
        {
            contactRepo.LoadAllContacts();

            return View(contactRepo.AllContacts);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
