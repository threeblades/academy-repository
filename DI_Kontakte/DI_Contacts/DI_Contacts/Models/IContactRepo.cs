﻿using System.Collections.Generic;

namespace DI_Contacts.Models
{
    public interface IContactRepo
    {
        List<Contact> AllContacts { get; }

        void InsertContact(Contact contact);

        void LoadAllContacts();
    }
}