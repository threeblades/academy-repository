﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace DI_Contacts.Models
{
    public class ContactRepoJSON : IContactRepo
    {
        string path = @"D:\serialize\contactsjson.json";

        private List<Contact> allContacts;

        DataContractJsonSerializer dcjs = new DataContractJsonSerializer(typeof(List<Contact>));

        public List<Contact> AllContacts
        {
            get { return allContacts; }

        }

        public ContactRepoJSON()
        {

            allContacts = new List<Contact>();

            LoadAllContacts();

        }


        public void LoadAllContacts()
        {
            if (File.Exists(path))
            {

                using (FileStream fs = File.OpenRead(path))
                {

                    allContacts = (List<Contact>)dcjs.ReadObject(fs);
                }

            }

        }



        public void InsertContact(Contact contact)
        {

            allContacts.Add(contact);

            using (var stream = File.Create(path))
            {

                dcjs.WriteObject(stream, allContacts);

            }

        }

    }
}
