﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace DI_Contacts.Models
{
    public class ContactRepoDB : IContactRepo
    {

        string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                 "Initial Catalog=NETDB;" +
                                 "Integrated Security=true;";

        private List<Contact> allContacts = new List<Contact>();

        public List<Contact> AllContacts
        {
            get { return allContacts; }

        }


        public void LoadAllContacts()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    SqlCommand sqlCommand = new SqlCommand("SELECT * FROM tblKontakt", sqlConnection);

                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        allContacts.Add(new Contact {

                            Vorname = reader.GetString(1),
                            Nachname = reader.GetString(2),
                            Telefon = reader.GetString(3),
                            EMail = reader.GetString(4)

                        });


                    }
                    reader.Close();

                }
                catch (Exception)
                {

                    throw;
                }



            }

        }

        public void InsertContact(Contact contact)
        {

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlConnection.Open();

                    SqlCommand sqlCommand = new SqlCommand("INSERT INTO tblKontakt (Name, Vorname, Telefon, Email) VALUES (@lastname, @firstname, @phone, @email)", sqlConnection);

                    sqlCommand.Parameters.AddWithValue("@lastname", contact.Nachname);
                    sqlCommand.Parameters.AddWithValue("@firstname", contact.Vorname);
                    sqlCommand.Parameters.AddWithValue("@phone", contact.Telefon);
                    sqlCommand.Parameters.AddWithValue("@email", contact.EMail);

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception)
                {

                    throw;
                }


            }


        }


    }
}
