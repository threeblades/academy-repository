﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DI_Contacts.Models
{
    public class ContactRepoXML : IContactRepo
    {
        string path = @"D:\serialize\contacts.xml";

        private List<Contact> allContacts = new List<Contact>();

        XmlSerializer xs = new XmlSerializer(typeof(List<Contact>));

        public List<Contact> AllContacts
        {
            get { return allContacts; }

        }

        public ContactRepoXML()
        {
            LoadAllContacts();
        }



        public void LoadAllContacts()
        {
            if (File.Exists(path))
            {

                using (FileStream fs = File.OpenRead(path))
                {

                    allContacts = (List<Contact>)xs.Deserialize(fs);
                }
            }

        }



        public void InsertContact(Contact contact)
        {

            AllContacts.Add(contact);

            using (FileStream fs = File.Create(path))
            {

                xs.Serialize(fs, AllContacts);

            }

        }
    }
}
