﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Middleware_CustomMiddleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class MyMiddleware
    {
        private readonly RequestDelegate _next;

        public MyMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            MyController controller = new MyController();


            if (httpContext.Request.Method == "POST")
            {

                string formVariable = httpContext.Request.Form["inputText"];

                httpContext.Response.WriteAsync(controller.CheckPalindrome(formVariable));
                
            }
            else
            {
                httpContext.Response.WriteAsync(controller.Index());
            }



            bool nächsteStufeAufrufen = false;



            if (nächsteStufeAufrufen)
            {
               
                return _next(httpContext);
            }
            else
            {
                
                return Task.CompletedTask;
            }

           
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class MyMiddlewareExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MyMiddleware>();
        }
    }
}
