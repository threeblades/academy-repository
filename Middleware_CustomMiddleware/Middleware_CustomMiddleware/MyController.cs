﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Middleware_CustomMiddleware
{
    public class MyController
    {
        public string Index()
        {
            return File.ReadAllText("index.html");
        }

        public string CheckPalindrome(string inputText)
        {
            PalindromeRepo prepo = new PalindromeRepo();

            bool ispalindrome = prepo.AskForPalindrome(inputText);

            string palipage = File.ReadAllText("palindrom.html");

            string palindromeString = string.Format(palipage, $"{inputText} ist {(ispalindrome ? "" : "k")}ein Palindrom.");

            //string palindromeString = string.Format(palipage, inputText + string.Format(" ist {0}ein Palindrom!", ispalindrome ? "" : "k"));

            return palindromeString;
        }

    }
}
