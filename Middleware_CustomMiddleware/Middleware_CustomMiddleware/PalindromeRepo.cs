﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Middleware_CustomMiddleware
{
    public class PalindromeRepo
    {

        public string Text { get; set; }

        public bool IsPalindrome { get; set; }

        public bool AskForPalindrome(string palindromString)
        {

            string palindromStringReverse = palindromString;

            palindromStringReverse = string.Join("", palindromStringReverse.ToCharArray().Reverse());


            if (palindromString == palindromStringReverse)
            {
                IsPalindrome = true;
                return IsPalindrome;
            }
            else
            {
                IsPalindrome = false;
                return IsPalindrome;
            }
        }




    }
}
