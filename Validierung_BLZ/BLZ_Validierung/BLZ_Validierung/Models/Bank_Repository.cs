﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BLZ_Validierung.Models
{
    public class Bank_Repository
    {

        private string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                   "Initial Catalog=TeachSQL;" +
                                   "Integrated Security=true;";

        public bool Find_BLZ(string blz)
        {
            bool BLZ_found = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand command = new SqlCommand("SELECT BLZ FROM BLZTabelle WHERE BLZ = @BLZ_search", conn);

                command.Parameters.AddWithValue("BLZ_search", blz);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        if (blz == reader.GetValue(0) as string)
                        {
                            BLZ_found = true;
                        }
                        
                    }

                }

            }

            return BLZ_found;
        }
    }
}
