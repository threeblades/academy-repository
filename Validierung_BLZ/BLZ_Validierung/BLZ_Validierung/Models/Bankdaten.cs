﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BLZ_Validierung.Models
{
    public class Bankdaten
    {


        [Required(ErrorMessage = "Kontonummer muss angegeben werden!")]
        [RegularExpression(@"\d{9}", ErrorMessage = "DIE KONTONUMER ENTSPRISCHT NISCHT MUSTER, JA?")]
        [Display(Name = "Kontonummer")]
        public string KontoNr { get; set; }

        [Required]
        [Remote("Check_BLZ", "Home", ErrorMessage = "DAS NISCHT DIE RISCHTIGE BLZ!")]
        [Display(Name = "BLZ")]
        public string BLZ { get; set; }
    }
}
