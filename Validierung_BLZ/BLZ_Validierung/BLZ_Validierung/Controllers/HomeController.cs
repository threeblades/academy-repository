﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLZ_Validierung.Models;

namespace BLZ_Validierung.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Bankdaten bd)
        {
            if (ModelState.IsValid == false)
            {
                return View();
            }

            return View(bd);
        }

        public JsonResult Check_BLZ(string BLZ)
        {
            bool state = false;

            Bank_Repository br = new Bank_Repository();

            if (BLZ != null)
            {
                state = br.Find_BLZ(BLZ);
            }

            return Json(state);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
