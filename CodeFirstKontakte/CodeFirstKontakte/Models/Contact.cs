﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeFirstKontakte.Models
{
    public class Contact
    {
        public int ContactID { get; set; }
        public string Nachname { get; set; }
        public string Vorname { get; set; }
        public string Telefon { get; set; }
        public string EMail { get; set; }

    }
}
