﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_LookForSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 1, 4, 4, 5, 6, 12, 43, 222, 741, 4578, 9444, 12000 };

            int sum = 8;

            SearchForSum(arr, sum);

            //SearchForSumNoPerformance(arr, sum);

            //SearchforSumLINQ(arr, sum);
        }


        private static void SearchforSumLINQ(int[] arr, int sum)
        {

            var resultsum = arr.Where((x, y) => x + y == sum).Sum();

            Console.WriteLine(resultsum);
        }



        private static void SearchForSum(int[] arr, int sum)
        {
            int left = 0;
            int right = (arr.Length - 1);

            while (left < right)
            {
                if (arr[left] + arr[right] == sum)
                {
                    Console.WriteLine("Left + Right = {0}", arr[left] + arr[right]);
                    break;
                }
                else if(arr[left] + arr[right] < sum)
                {
                    left++;
                }
                else
                {
                    right--;
                }

            }

        }



        private static void SearchForSumNoPerformance(int[] arr, int sum)
        {

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[i] + arr[j] == sum)
                    {
                        Console.WriteLine("Left + Right = {0}", arr[i] + arr[j]);
                        break;
                    }
                }
            }
        }


    }
}
