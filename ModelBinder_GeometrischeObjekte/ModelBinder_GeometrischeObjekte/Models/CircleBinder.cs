﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModelBinder_GeometrischeObjekte.Models
{
    public class CircleBinder : IModelBinder
    {

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            ValueProviderResult value = bindingContext.ValueProvider.GetValue("inputValue");

            ValueProviderResult diaOrPerim = bindingContext.ValueProvider.GetValue("DiameterOrRadius");

            Circle resultC = new Circle();

            if (value.Length == 0 || diaOrPerim.Length == 0)
            {
                return Task.CompletedTask;
            }


            try
            {
                resultC.Value = Int64.Parse(value.FirstValue);

                resultC.DiameterOrRadius = diaOrPerim.ToString();

                resultC.CalculateArea();

                resultC.CalculatePerimeter();

                bindingContext.Result = ModelBindingResult.Success(resultC);


            }
            catch (Exception)
            {

                return Task.CompletedTask;
            }
            

            return Task.CompletedTask;
        }

    }
}
