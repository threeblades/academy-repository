﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModelBinder_GeometrischeObjekte.Models
{

    [ModelBinder(BinderType = typeof(CircleBinder))]
    public class Circle
    {

        public double Value { get; set; }
        public string DiameterOrRadius { get; set; }
        public double Area { get; set; }
        public double Perimeter { get; set; }


        public void CalculateArea()
        {
            if (DiameterOrRadius == "Durchmesser")
            {
                Area = (Value * Value) * Math.PI / 4;

            }
            else
            {
                Area = Math.Pow(Value, 2) * Math.PI;

            }
        }



        public void CalculatePerimeter()
        {

            if (DiameterOrRadius == "Durchmesser")
            {
                Perimeter = Value * Math.PI;

            }
            else
            {
                Perimeter = (Value * 2) * Math.PI;

            }
        }

    }
}
