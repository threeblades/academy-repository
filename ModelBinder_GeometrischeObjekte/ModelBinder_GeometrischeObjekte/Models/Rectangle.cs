﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModelBinder_GeometrischeObjekte.Models
{
    public class Rectangle
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int Area { get; set; }
        public int Perimeter { get; set; }


        public int CalculateArea()
        {
            Area = Height * Width;

            return Area;
        }

        public int CalculatePerimeter()
        {
            Perimeter = (Height * 2) + (Width * 2);

            return Perimeter;
        }

    }
}
