﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelBinder_GeometrischeObjekte.Models;

namespace ModelBinder_GeometrischeObjekte.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public IActionResult CircleView()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CircleView(Circle c)
        {
           

            return View(c);
        }


        [HttpGet]
        public IActionResult RectangleView()
        {
            return View();
        }

        [HttpPost]
        public IActionResult RectangleView(int height, int width)
        {
            Rectangle rect = new Rectangle();

            rect.Height = height;

            rect.Width = width;

            rect.CalculatePerimeter();
            rect.CalculateArea();

            return View(rect);
        }



        public IActionResult Privacy()
        {
            return View();
        }




        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
