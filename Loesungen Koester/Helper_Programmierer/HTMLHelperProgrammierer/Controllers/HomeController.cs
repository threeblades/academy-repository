﻿using HTMLHelperProgrammierer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace HTMLHelperProgrammierer.Controllers
{
    public class HomeController : Controller
    {
        private List<Tätigkeit> alleTätigkeiten = new List<Tätigkeit>();
        private List<Projekt> alleProjekte = new List<Projekt>();

        public HomeController()
        {
            // Diese Daten können/sollten im Model in einer Repository-Klasse verwaltet werden
            alleTätigkeiten.Add(new Tätigkeit() { Id = 1, Name = "Programmierer" });
            alleTätigkeiten.Add(new Tätigkeit() { Id = 2, Name = "Tester" });
            alleTätigkeiten.Add(new Tätigkeit() { Id = 3, Name = "Projektleiter" });

            alleProjekte.Add(new Projekt() { Id = 1, Name = "Web-Anwendung" });
            alleProjekte.Add(new Projekt() { Id = 2, Name = "WPF-Anwendung" });
            alleProjekte.Add(new Projekt() { Id = 3, Name = "JavaScript lernen" });
        }

        public IActionResult Index()
        {
            // Für die Auswahl aller Tätigkeiten
            ViewData["Aufgabe"] = new SelectList(alleTätigkeiten, "Id", "Name");

            // Für die Auswahl aller Projekte
            ViewData["Projekte"] = new MultiSelectList(alleProjekte, "Id", "Name");

            return View();
        }

        [HttpPost]
        public IActionResult Index(Programmierer neuerProgrammierer, int Aufgabe, int[] Projekte)
        {
            ViewData["Aufgabe"] = new SelectList(alleTätigkeiten, "Id", "Name", Aufgabe);

            ViewData["Projekte"] = new MultiSelectList(alleProjekte, "Id", "Name", Projekte);

            // Anhand der geposteten Id die Tätigkeit suchen und zuweisen
            neuerProgrammierer.Aufgabe = alleTätigkeiten.Find(t => t.Id == Aufgabe);

            // Anhand der geposteten Ids die Projekte suchen und zuweisen
            neuerProgrammierer.Projekte = alleProjekte.Where(p => Projekte.Contains(p.Id)).ToList();

            // Setzt alle Eingabefelder wideder zurück
            //ModelState.Clear();

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}