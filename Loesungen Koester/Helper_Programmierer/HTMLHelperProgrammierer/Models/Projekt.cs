﻿namespace HTMLHelperProgrammierer.Models
{
    public class Projekt
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}