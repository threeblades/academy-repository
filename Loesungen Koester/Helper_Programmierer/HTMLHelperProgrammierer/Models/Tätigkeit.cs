﻿namespace HTMLHelperProgrammierer.Models
{
    public class Tätigkeit
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}