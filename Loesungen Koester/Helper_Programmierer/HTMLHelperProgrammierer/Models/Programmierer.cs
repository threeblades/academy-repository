﻿using System.Collections.Generic;

namespace HTMLHelperProgrammierer.Models
{
    public class Programmierer
    {
        public int Id { get; set; }

        public string Vorname { get; set; }

        public string Nachname { get; set; }

        public string EMail { get; set; }

        public string Geschlecht { get; set; }

        public Tätigkeit Aufgabe { get; set; }

        public List<Projekt> Projekte { get; set; }
    }
}