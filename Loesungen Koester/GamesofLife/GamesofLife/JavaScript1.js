﻿/// <reference path="c:\users\ita6-tn16\documents\visual studio 2013\Projects\GamesofLife\GamesofLife\Scripts/jquery-2.1.4.js" />

$(document).ready(function () {


    // Worker anlegen
    worker = new Worker("rechne.js");

    // Canvas Element zeichnen
    c = document.getElementById("myCanvas");
    ctx = c.getContext("2d");

    var xpos = 0;
    var ypos = 0;
    var counter = 0;
    ctx.moveTo(0, 0);
    generationenzähler = 0;


    // waagerechte Linien einfügen
    for (var i = 0; i < 400; i++) {
        if (xpos == 0 && counter % 2 == 0) {
            xpos = 1600;
        }
        else if (xpos == 1600 && counter %2 == 0) {
            xpos = 0;
        }
        if (counter % 2 == 1) {
            ypos += 10;
        }
        ctx.lineTo(xpos, ypos);
        ctx.stroke();
        counter++;
    }


    // senkrechte Linien einfügen
    counter = 0;
    ctx.moveTo(0, 0);
    xpos = 0;
    ypos = 0;

    for (var i = 0; i < 800; i++) {
        if (ypos == 0 && counter % 2 == 0) {
            ypos = 800;
        }
        else if (ypos == 800 && counter % 2 == 0) {
            ypos = 0;
        }
        if (counter % 2 == 1) {
            xpos += 10;
        }
        ctx.lineTo(xpos, ypos);
        ctx.stroke();
        counter++;
    }



    // Referenzarray schreiben
    refarr = new Array();
    for (var i = 0; i < 160; i++) {
        refarr[i] = [];
        for (var j = 0; j < 80; j++) {
            refarr[i][j] = 0;
        }
    };

    // "Friedhofsarray" schreiben
    belarr = new Array();
    for (var i = 0; i < 160; i++) {
        belarr[i] = [];
        for (var j = 0; j < 80; j++) {
            belarr[i][j] = 0;
        }
    }


    // Aktionen weiterleiten
    $("#myCanvas").on('click', einfärben);
    $("#next").on('click', nextGeneration);
    $("#sim").on('click', simulation);
    $("#simstop").on('click', simulationstop);
    $("#weiter").on('click', weiter);
    $("#save").on('click', save);
    $("#load").on('click', load);
    $("#clear").on('click', clear);
    $("#rand").on('click', getFaktor);
    $("#select").on('change', selected);


    // unnötige Buttons und Felder disablen
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', true);
    $("#next").attr('disabled', true);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', false);
    $("#rand").attr('disabled', false);
    $("#text").attr('disabled', true);
    $("#select").attr('disabled', true);
    $("#füllfaktor").attr('disabled', true);

    // Füllfaktor  bestimmen
    var faktor = document.getElementById("füllfaktor");
    faktor.onkeypress = function (e) {

        if (e.keyCode == 13 && faktor.value != "") {

            
            if (parseFloat(faktor.value) >= 0.0 && parseFloat(faktor.value) <= 1.0) {
                fill(parseFloat(faktor.value));
                
            }
            else {
                alert("Die Eingabe muss zwischen 0 und 1 sein!");
            }
        }
        
    };


    // Erstbild malen
    maleBild(refarr);

});

function weiter() {
    
    // Unnötige Buttons ausblenden
    $("#simstop").attr('disabled', false);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', true);
    $("#next").attr('disabled', true);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);


    // Simulation starten
    
    simulation();
};

function einfärben(ev) {

    // Buttons freigeben
    $("#save").attr('disabled', false);
    $("#clear").attr('disabled', false);
    $("#next").attr('disabled', false);
    $("#sim").attr('disabled', false);
    $("#load").attr('disabled', false);
    $("#rand").attr('disabled', true);


    // Umrechnung des Mausklicks zur oberen, linken Quadratecke
    var mousex = ev.offsetX;
    var mousey = ev.offsetY;
    mousex = Math.floor(mousex / 10) * 10;
    mousey = Math.floor(mousey / 10) * 10;
    
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    if (refarr[mousex/10][mousey/10] == 0) {
        ctx.fillStyle = "#1919FF";
        ctx.fillRect(mousex + 2, mousey + 2, 6, 6);
        refarr[mousex / 10][mousey / 10] = 1;
        belarr[mousex / 10][mousey / 10] ++;
    }
    else {
        ctx.fillStyle = "#CCCCCC";
        ctx.fillRect(mousex + 1, mousey + 1, 8, 8);
        refarr[mousex / 10][mousey / 10] = 0;
    }
};

function nextGeneration() {

    // unnötige Buttons ausblenden 
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', false);
    $("#clear").attr('disabled', false);
    $("#next").attr('disabled', false);
    $("#sim").attr('disabled', false);
    $("#load").attr('disabled', false);
    $("#rand").attr('disabled', true);


    // dem worker das Array passend machen
    var refarrjson = JSON.stringify(refarr);
    
    // dem Worker Daten übermitteln
    worker.postMessage(refarrjson);
    generationenzähler++;


    // Wenn der worker antwortet...
    worker.addEventListener("message", function (evt) {


        var rerefarr = JSON.parse(evt.data);
        maleBild(rerefarr);

        aktualisiereLabel();


    });

    
};

function aktualisiereLabel() {

    // Generationenzähler aktualisieren
    $("#aktuell").text("Aktuelle Generation: "+generationenzähler );
};

function maleBild(rerefarr) {


    // neues Bild malen aus den Daten des neuen Arrays
    for (var i = 0; i < 160; i++) {
        for (var j = 0; j < 80; j++) {
            if (rerefarr[i][j] == 1) {
                ctx.fillStyle = "#1919FF";
                    ctx.fillRect(i * 10 + 2, j * 10 + 2, 6, 6);
                    belarr[i][j]++;
                }
            else {

                if (belarr[i][j] > 10 && belarr[i][j] < 100) {

                    var farbe = "#" + (100-belarr[i][j]) + "" + (100-belarr[i][j]) + "" + (100-belarr[i][j]);
                    ctx.fillStyle = farbe;
                    ctx.fillRect(i * 10 + 1, j * 10 + 1, 8, 8);

                }
                else if (belarr[i][j] >=100)
                {
                    ctx.fillStyle = "#000000";
                    ctx.fillRect(i * 10 + 1, j * 10 + 1, 8, 8);
                }
                else {
                    ctx.fillStyle = "#CCCCCC";
                    ctx.fillRect(i * 10 + 1, j * 10 + 1, 8, 8);

                }
                }
            }
    }

    // Das neue Array wird zum Referenzarray
    refarr = rerefarr;
};


function simulation() {

    // unnötige Buttons ausblenden
    $("#simstop").attr('disabled', false);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', true);
    $("#next").attr('disabled', true);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);


    // Dem worker die Daten passend machen
    var refarrjson = JSON.stringify(refarr);


    // Dem Worker die Daten übergeben
    worker.postMessage(refarrjson);

    // Wenn der Worker antwortet:
    worker.addEventListener("message", function (evt) {


        var rerefarr = JSON.parse(evt.data);
        maleBild(rerefarr);
        generationenzähler++;
        aktualisiereLabel();

        // Wieder Worker neue Daten geben (rekursiv)
        refarrjson = JSON.stringify(refarr);
        worker.postMessage(refarrjson);
    });

};

function simulationstop() {

    // Rekursiv arbeitenden Worker unterbrechen und neuen Worker bereitstellen
    worker.terminate();
    worker = new Worker("rechne.js");

    // unnötige Buttons ausgrauen
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', false);
    $("#save").attr('disabled', false);
    $("#clear").attr('disabled', false);
    $("#next").attr('disabled', false);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);
};

function save() {

    // Input-Feld enablen
    $("#text").attr('disabled', false);

    // Wenn Enter-Taste, dann speichern
    $("#text").keypress(function (e) {
        if (e.which == 13 && $("#text").val() != "") {
            
            var speicherdaten = {
                Name: $("#text").val(),
                Daten: refarr,
                Daten2: belarr
            };


            // Für den local Storage zu einem JSON-String machen 
            var daten = JSON.stringify(speicherdaten);

            // speichern
            localStorage.setItem($("#text").val(), daten);
            $("#text").attr('disabled', true);
            alert("Spielstand wurde gespeichert unter dem Namen " + $("#text").val());
            $("#text").val("");
           
        }
    })
    ;
};

function load() {
    
    // Selectbox enablen
    $("#select").attr('disabled', false);

    // Keyliste laden
    var keyliste = allStorage();

    // In die Selectbox einfügen
    var liste = $("#select");
    for (var i = 0; i < keyliste.length; i++) {

        // unbrauchbare local-Storage-Einträge ignorieren
        try {
            keyliste[i] = JSON.parse(keyliste[i]);
            liste.append($("<option>", { value: keyliste[i]["Name"], text: keyliste[i]["Name"] }));

        } catch (e) {
            console.log("Fehler beim Lesen aus dem Local Storage");
        }

        }

};

function clear() {


    // Unnötige Buttons ausblenden
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', true);
    $("#next").attr('disabled', true);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', false);
    $("#rand").attr('disabled', false);

    // refarr auf null setzen
    // Referenzarray schreiben
    for (var i = 0; i < 160; i++) {
        for (var j = 0; j < 80; j++) {
            refarr[i][j] = 0;
            belarr[i][j] = 0;
        }
    };

    maleBild(refarr);
    generationenzähler = 0;
    aktualisiereLabel();
};

function fill(füllfaktor) {

    

    // Buttons auswerten
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', false);
    $("#clear").attr('disabled', false);
    $("#next").attr('disabled', false);
    $("#sim").attr('disabled', false);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);
    $("#füllfaktor").attr('disabled', true);

    // Füllfaktorfeld löschen
    var aus = document.getElementById("füllfaktor");
    aus.value = "";
    

    // refarr mit zufälligen Werten füllen
    for (var i = 0; i < 160; i++) {
        for (var j = 0; j < 80; j++) {

            var zufall = Math.random();
            if (zufall < 1 - füllfaktor) {
                refarr[i][j] = 0;
            }
            else {
                refarr[i][j] = 1;
                belarr[i][j] = 1;
            }
            
        }
    };
    maleBild(refarr);
};

function allStorage() {

    // Den Storage auslesen
    var archive = [],
        keys = Object.keys(localStorage);


    for (var i = 0; i < keys.length; i++) {
        archive.push(localStorage.getItem(keys[i]));
    }

    return archive;
};

function selected() {

    // Daten aus Storage laden
    var ladedaten = localStorage.getItem($("#select option:selected").val());

    // Daten aus Storage umwandeln
    ladedaten = JSON.parse(ladedaten);

    // Geladenes Bild malen
    refarr = ladedaten["Daten"];
    belarr = ladedaten["Daten2"];
    maleBild(refarr);
    $("#select").attr('disabled', true);

    // Die Selectbox leeren
    var liste = $("#select");
    liste.children().remove();

    // Unnötige Buttons ausblenden
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', false);
    $("#next").attr('disabled', false);
    $("#sim").attr('disabled', false);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);
   
   
    
};

function getFaktor() {

    // Unnötige Buttons ausblenden
    $("#simstop").attr('disabled', true);
    $("#weiter").attr('disabled', true);
    $("#save").attr('disabled', true);
    $("#clear").attr('disabled', true);
    $("#next").attr('disabled', true);
    $("#sim").attr('disabled', true);
    $("#load").attr('disabled', true);
    $("#rand").attr('disabled', true);
    $("#text").attr('disabled', true);
    $("#select").attr('disabled', true);
    $("#füllfaktor").attr('disabled', false);
};

