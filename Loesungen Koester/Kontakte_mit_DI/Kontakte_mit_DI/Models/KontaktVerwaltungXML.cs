﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Kontakte_mit_DI.Models
{
    public class KontaktVerwaltungXML : IKontaktVerwaltung
    {
        private string pfad = @"d:\daten\kontakte.xml";

        public List<Kontakt> AlleKontakte { get; private set; }

        private XmlSerializer serialisier;

        public KontaktVerwaltungXML()
        {
            AlleKontakte = new List<Kontakt>();

            serialisier = new XmlSerializer(typeof(List<Kontakt>));

            KontakteLaden();
        }

        public string KontaktEinfügen(Kontakt neuerKontakt)
        {
            AlleKontakte.Add(neuerKontakt);

            return Speichern();
        }

        private string Speichern()
        {
            try
            {
                using (Stream stream = File.Create(pfad))
                {
                    serialisier.Serialize(stream, AlleKontakte);

                    return "Datensatz eingefügt";
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string KontakteLaden()
        {
            try
            {
                using (Stream stream = File.OpenRead(pfad))
                {
                    AlleKontakte = serialisier.Deserialize(stream) as List<Kontakt>;
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}