﻿using System.Collections.Generic;

namespace Kontakte_mit_DI.Models
{
    public interface IKontaktVerwaltung
    {
        List<Kontakt> AlleKontakte { get; }

        string KontaktEinfügen(Kontakt neuerKontakt);

        string KontakteLaden();
    }
}