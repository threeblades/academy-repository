﻿namespace Kontakte_mit_DI.Models
{
    public class Kontakt
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Telefon { get; set; }
        public string EMail { get; set; }
    }
}