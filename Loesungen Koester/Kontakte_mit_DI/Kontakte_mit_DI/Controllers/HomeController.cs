﻿using System.Diagnostics;
using Kontakte_mit_DI.Models;
using Microsoft.AspNetCore.Mvc;

namespace Kontakte_mit_DI.Controllers
{
    public class HomeController : Controller
    {
        private IKontaktVerwaltung verwaltung;

        // Konstruktor des Controller für Dependency Injection
        public HomeController(IKontaktVerwaltung verwaltung)
        {
            this.verwaltung = verwaltung;
        }

        public IActionResult Index()
        {
            ViewBag.HashCode = verwaltung.GetHashCode();

            return View();
        }

        [HttpPost]
        public IActionResult Index(Kontakt neuerKontakt)
        {
            ViewBag.Meldung = verwaltung.KontaktEinfügen(neuerKontakt);

            return View(neuerKontakt);
        }

        public IActionResult AlleAnzeigen()
        {
            return View(verwaltung.AlleKontakte);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}