﻿namespace ClassLibraryKontakt
{
    public class Kontakt
    {
        public int Id { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Geschlecht { get; set; }
        public string StrasseHausnummer { get; set; }
        public string Plz { get; set; }
        public string Ort { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} Geschlecht: {2} Wohnort: {3}", Vorname, Nachname, Geschlecht, Ort);
        }
    }
}
