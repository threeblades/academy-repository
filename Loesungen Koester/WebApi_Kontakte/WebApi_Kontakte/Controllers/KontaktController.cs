﻿using ClassLibraryKontakt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi_Kontakte.Models;

namespace WebApi_Kontakte.Controllers
{
    [Produces("application/json")]
    [Route("api/Kontakt")]
    public class KontaktController : Controller
    {
        private readonly KontaktContext _context;

        public KontaktController(KontaktContext context)
        {
            _context = context;
        }

        // GET: api/Kontakt
        [HttpGet]
        public IEnumerable<Kontakt> GetKontakte()
        {
            return _context.Kontakte;
        }

        // GET: api/Kontakt/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetKontakt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var kontakt = await _context.Kontakte.SingleOrDefaultAsync(m => m.Id == id);

            if (kontakt == null)
            {
                return NotFound();
            }

            return Ok(kontakt);
        }

        // PUT: api/Kontakt/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutKontakt([FromRoute] int id, [FromBody] Kontakt kontakt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kontakt.Id)
            {
                return BadRequest();
            }

            try
            {
                // Ort zur Postleitzahl ermitteln
                PlzServiceClient plzService = new PlzServiceClient();
                kontakt.Ort = await plzService.GetPlzInfo(kontakt.Plz);

                // Geschlecht ermitteln
                NameServiceClient nameService = new NameServiceClient();
                kontakt.Geschlecht = await nameService.GetNameInfo(kontakt.Vorname);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            _context.Entry(kontakt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KontaktExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Kontakt
        [HttpPost]
        public async Task<IActionResult> PostKontakt([FromBody] Kontakt kontakt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                // Ort zur Postleitzahl ermitteln
                PlzServiceClient plzService = new PlzServiceClient();
                kontakt.Ort = await plzService.GetPlzInfo(kontakt.Plz);

                // Geschlecht ermitteln
                NameServiceClient nameService = new NameServiceClient();
                kontakt.Geschlecht = await nameService.GetNameInfo(kontakt.Vorname);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            _context.Kontakte.Add(kontakt);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetKontakt", new { id = kontakt.Id }, kontakt);
        }

        // DELETE: api/Kontakt/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteKontakt([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var kontakt = await _context.Kontakte.SingleOrDefaultAsync(m => m.Id == id);

            if (kontakt == null)
            {
                return NotFound();
            }

            _context.Kontakte.Remove(kontakt);

            await _context.SaveChangesAsync();

            return Ok(kontakt);
        }

        private bool KontaktExists(int id)
        {
            return _context.Kontakte.Any(e => e.Id == id);
        }
    }
}