﻿using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi_Kontakte.Models
{
    public class PlzServiceClient
    {
        private readonly WebServiceClient serviceClient = new WebServiceClient("http://api.zippopotam.us/de/");

        public async Task<string> GetPlzInfo(string plz)
        {
            ZipCode zipCode = await serviceClient.GetInfo<ZipCode>(plz);

            if (zipCode != null)
            {
                return zipCode.Places[0].Placename;
            }

            return string.Empty;
        }
    }

    public class ZipCode
    {
        public Place[] Places { get; set; }
    }

    public class Place
    {
        [JsonProperty("place name")]
        public string Placename { get; set; }
    }
}

