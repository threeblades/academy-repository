﻿using ClassLibraryKontakt;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace WebApi_Kontakte.Models
{
    public class KontaktContext : DbContext
    {
        public DbSet<Kontakt> Kontakte { get; set; }

        public KontaktContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();

            if (Kontakte.Count() == 0)
            {
                List<Kontakt> kontakte = new List<Kontakt>()
                {
                    new Kontakt() { Vorname = "Tim", Nachname = "Müller", Email = "tim@home.de", StrasseHausnummer = "Langer Weg 13", Plz = "44787", Telefon = "123456789" },
                    new Kontakt() { Vorname = "Lisa", Nachname = "Meyer", Email = "lisa@home.de", StrasseHausnummer = "Kurze Straße 111", Plz = "44139", Telefon = "987654321" },
                    new Kontakt() { Vorname = "Nina", Nachname = "Schmitt", Email = "nina@home.de", StrasseHausnummer = "Am Ufer 29", Plz = "59065", Telefon = "1273617236" },
                    new Kontakt() { Vorname = "Frank", Nachname = "Schulz", Email = "frank@home.de", StrasseHausnummer = "Am Strand 56", Plz = "45127", Telefon = "989865341" }
                };

                PlzServiceClient plzService = new PlzServiceClient();

                NameServiceClient nameService = new NameServiceClient();

                foreach (Kontakt kontakt in kontakte)
                {
                    // Ort und Geschlecht der Testdaten ermitteln
                    kontakt.Ort = plzService.GetPlzInfo(kontakt.Plz).Result;
                    kontakt.Geschlecht = nameService.GetNameInfo(kontakt.Vorname).Result;
                }

                Kontakte.AddRange(kontakte);

                SaveChanges();
            }
        }
    }
}
