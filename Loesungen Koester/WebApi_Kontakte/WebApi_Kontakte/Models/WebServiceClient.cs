﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebApi_Kontakte.Models
{
    public class WebServiceClient
    {
        private readonly HttpClient client;

        public WebServiceClient(string url, string accepts = "application/json")
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("ISO-8859-1"));

            // Datenformat der Antwort festlegen (Ist optional)
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(accepts));
        }

        public async Task<T> GetInfo<T>(string query) where T : class
        {
            // GET-Aufruf an den Service starten
            HttpResponseMessage responseMessage = await client.GetAsync(query);

            return await DeserializeRequest<T>(responseMessage);
        }

        public async Task<T> DeserializeRequest<T>(HttpResponseMessage responseMessage) where T : class
        {
            // Aufruf erfolgreich?
            if (responseMessage.IsSuccessStatusCode)
            {
                // Antwort der Web-API in einem String speichern
                string result = await responseMessage.Content.ReadAsStringAsync();

                // JSON Parsen 
                return JsonConvert.DeserializeObject<T>(result);
            }

            return null;
        }
    }
}
