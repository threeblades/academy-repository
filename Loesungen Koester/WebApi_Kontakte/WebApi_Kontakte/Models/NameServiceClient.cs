﻿using System.Threading.Tasks;

namespace WebApi_Kontakte.Models
{
    public class NameServiceClient
    {
        private readonly WebServiceClient serviceClient = new WebServiceClient("https://api.genderize.io/");

        public async Task<string> GetNameInfo(string name)
        {
            NameInfo nameInfo = await serviceClient.GetInfo<NameInfo>("?name=" + name);

            return nameInfo.Gender;
        }
    }

    // Hilfsklasse zum Deserialisieren der Daten vom WebService   
    // Nur der Wert für Gender ist von Interesse
    public class NameInfo
    {
        public string Gender { get; set; }
    }
}