﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsClient.Model;
using WindowsFormsClient.Models;

namespace WindowsFormsClient
{
    public partial class Form1 : Form
    {
        private readonly KontaktRepository kontaktRepository;

        private List<Kontakt> kontakte;

        private BindingSource source = new BindingSource();

        public Form1()
        {
            InitializeComponent();

            kontaktRepository = new KontaktRepository();
        }

        private async void buttonGet_Click(object sender, EventArgs e)
        {
            kontakte = await kontaktRepository.Get();
            source.DataSource = kontakte;
            dataGridViewKontakte.DataSource = source;
        }

        private async void buttonCreate_Click(object sender, EventArgs e)
        {
            Kontakt kontakt = new Kontakt();

            FormNeuerKontakt formNeuerKontakt = new FormNeuerKontakt(kontakt);

            DialogResult dialogResult = formNeuerKontakt.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                bool success = await kontaktRepository.Create(kontakt);

                if (success)
                {
                    kontakte.Add(kontakt);

                    // DataGrid aktualisieren
                    await UpdateDataGrid();

                }
            }
        }

        private async Task UpdateDataGrid()
        {
            kontakte = await kontaktRepository.Get();

            source.ResetBindings(false);
        }

        private async void buttonDelete_Click(object sender, EventArgs e)
        {
            Kontakt kontakt = (Kontakt)dataGridViewKontakte.CurrentRow.DataBoundItem;

            if (kontakt != null)
            {
                bool success = await kontaktRepository.Delete(kontakt.Id);

                if (success)
                {
                    kontakte.Remove(kontakt);

                    // DataGrid aktualisieren
                    await UpdateDataGrid();
                }
            }
        }

        private async void buttonEdit_Click(object sender, EventArgs e)
        {
            Kontakt kontakt = (Kontakt)dataGridViewKontakte.CurrentRow.DataBoundItem;

            FormNeuerKontakt formNeuerKontakt = new FormNeuerKontakt(kontakt);

            DialogResult result = formNeuerKontakt.ShowDialog();

            if (result == DialogResult.OK)
            {
                bool success = await kontaktRepository.Edit(kontakt);

                if (success)
                {
                    // DataGrid aktualisieren
                    await UpdateDataGrid();
                }
            }
        }

        private async void buttonTestData_Click(object sender, EventArgs e)
        {
            List<Kontakt> testKontakte = new List<Kontakt>()
                {
                    new Kontakt() { Vorname = "Tim", Nachname = "Müller", Email = "tim@home.de", StrasseHausnummer = "Langer Weg 13", Plz = "44787", Telefon = "0123456789" },
                    new Kontakt() { Vorname = "Lisa", Nachname = "Meyer", Email = "lisa@home.de", StrasseHausnummer = "Kurze Straße 111", Plz = "44139", Telefon = "0987654321" },
                    new Kontakt() { Vorname = "Nina", Nachname = "Schmitt", Email = "nina@home.de", StrasseHausnummer = "Am Ufer 29", Plz = "59065", Telefon = "01273617236" },
                    new Kontakt() { Vorname = "Frank", Nachname = "Schulz", Email = "frank@home.de", StrasseHausnummer = "Am Strand 56", Plz = "45127", Telefon = "0989865341" },
                    new Kontakt() { Vorname = "Klaus", Nachname = "Mann", Email = "klaus@work.de", StrasseHausnummer = "Am Meer 134", Plz = "50127", Telefon = "07216376437" },
                    new Kontakt() { Vorname = "Miriam", Nachname = "Peters", Email = "miri@work.de", StrasseHausnummer = "Hauptstrasse 78", Plz = "33615", Telefon = "09812387" }
                };

            foreach (Kontakt kontakt in testKontakte)
            {
                await kontaktRepository.Create(kontakt);
            }

            kontakte.AddRange(testKontakte);

            // DataGrid aktualisieren
            await UpdateDataGrid();
        }
    }
}