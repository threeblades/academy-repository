﻿namespace WindowsFormsClient.Models
{
    // Die ClassLibrary ist mit .NET Core erstellt
    // und kann noch nicht in Windows Forms Anwendungen
    // verwendet werden
    public class Kontakt
    {
        public int Id { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Geschlecht { get; set; }
        public string StrasseHausnummer { get; set; }
        public string Plz { get; set; }
        public string Ort { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", Vorname, Nachname, Geschlecht, Ort);
        }
    }
}
