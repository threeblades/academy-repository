﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsClient.Models;

namespace WindowsFormsClient
{
    public partial class FormNeuerKontakt : Form
    {
        private Kontakt kontakt;

        public FormNeuerKontakt()
        {
            InitializeComponent();
        }

        public FormNeuerKontakt(Kontakt kontakt) :this()
        {
            this.kontakt = kontakt;

            textBoxVorname.Text = kontakt.Vorname;
            textBoxNachname.Text = kontakt.Nachname;

            textBoxStrasseHausnummer.Text = kontakt.StrasseHausnummer;
            textBoxPlz.Text = kontakt.Plz;

            textBoxTelefon.Text = kontakt.Telefon;
            textBoxEMail.Text = kontakt.Email;
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            kontakt.Vorname = textBoxVorname.Text;
            kontakt.Nachname = textBoxNachname.Text;

            kontakt.StrasseHausnummer = textBoxStrasseHausnummer.Text;
            kontakt.Plz = textBoxPlz.Text;

            kontakt.Telefon = textBoxTelefon.Text;
            kontakt.Email = textBoxEMail.Text;

            this.Close();
        }
    }
}
