﻿using ClassLibraryKontakt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryKontaktRepository
{
    public class KontaktRepository
    {
        private const string url = "http://localhost:52842";

        private readonly HttpClient client;

        public KontaktRepository()
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            // Datenformat der Antwort festlegen (Ist optional)
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Kontakt>> Get()
        {
            HttpResponseMessage responseMessage = await client.GetAsync("api/kontakt");

            return await DeserializeRequest<List<Kontakt>>(responseMessage);
        }

        public async Task<Kontakt> Get(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync("api/kontakt/" + id);

            return await DeserializeRequest<Kontakt>(responseMessage);
        }

        public async Task<bool> Create(Kontakt kontakt)
        {
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(kontakt), Encoding.UTF8, "application/json");

            HttpResponseMessage responseMessage = await client.PostAsync("api/kontakt", stringContent);

            return responseMessage.IsSuccessStatusCode;
        }

        public async Task<bool> Edit(Kontakt kontakt)
        {
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(kontakt), Encoding.UTF8, "application/json");

            HttpResponseMessage responseMessage = await client.PutAsync("api/kontakt/" + kontakt.Id, stringContent);

            return responseMessage.IsSuccessStatusCode;
        }

        public async Task<bool> Delete(int id)
        {
            HttpResponseMessage responseMessage = await client.DeleteAsync("api/kontakt/" + id);

            return responseMessage.IsSuccessStatusCode;
        }

        public async Task<T> DeserializeRequest<T>(HttpResponseMessage responseMessage) where T : class
        {
            // Aufruf erfolgreich?
            if (responseMessage.IsSuccessStatusCode)
            {
                // Antwort der Web-API in einem String speichern
                string result = await responseMessage.Content.ReadAsStringAsync();

                // JSON Parsen
                return JsonConvert.DeserializeObject<T>(result);
            }

            return null;
        }
    }
}
