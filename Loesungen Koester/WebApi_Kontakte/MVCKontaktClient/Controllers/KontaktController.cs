﻿using ClassLibraryKontakt;
using ClassLibraryKontaktRepository;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MVCKontaktClient.Controllers
{
    public class KontaktController : Controller
    {
        private readonly KontaktRepository kontaktRepository = new KontaktRepository();

        // GET: Kontakt
        public async Task<IActionResult> Index()
        {
            return View(await kontaktRepository.Get());
        }

        // GET: Kontakt/Details/5
        public async Task<IActionResult> Details(int id)
        {
            return View(await kontaktRepository.Get(id));
        }

        // GET: Kontakt/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Kontakt/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Kontakt kontakt)
        {
            try
            {
                bool result = await kontaktRepository.Create(kontakt);

                if (result)
                {
                    ViewBag.Message = "Datensatz eingefügt.";
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Kontakt/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            return View(await kontaktRepository.Get(id));
        }

        // POST: Kontakt/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Kontakt kontakt)
        {
            try
            {
                await kontaktRepository.Edit(kontakt);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        // GET: Kontakt/Delete/5
        public IActionResult Delete(int id)
        {
            return View(kontaktRepository.Get(id).Result);
        }

        // POST: Kontakt/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id, Kontakt kontakt)
        {
            try
            {
                await kontaktRepository.Delete(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}