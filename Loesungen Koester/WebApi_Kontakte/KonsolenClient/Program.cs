﻿using ClassLibraryKontakt;
using ClassLibraryKontaktRepository;
using System;
using System.Collections.Generic;

namespace KonsolenClient
{
    class Program
    {
        static void Main(string[] args)
        {
            KontaktRepository repository = new KontaktRepository();

            List<Kontakt> alleKontakte = repository.Get().Result;

            foreach (Kontakt kontakt in alleKontakte)
            {
                Console.WriteLine(kontakt);
            }

            Console.WriteLine("Enter zum Beenden");
            Console.ReadLine();
        }
    }
}
