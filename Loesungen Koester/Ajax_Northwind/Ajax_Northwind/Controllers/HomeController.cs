﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ajax_Northwind.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ajax_Northwind.Controllers
{
    public class HomeController : Controller
    {
        private readonly NorthwindContext context;

        public HomeController(NorthwindContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            ViewData["Laender"] = new SelectList(context.Customers.GroupBy(c => c.Country).Select(g => g.Key).ToList());

            return View();
        }

        public PartialViewResult ZeigeKundeProLand(string Laender)
        {
            return PartialView("_Kunden", context.Customers.Where(c => c.Country == Laender));
        }

        public PartialViewResult ZeigeBestellungenProKunde(string id)
        {
            List<string> alleNummern = context.Orders.Where(o => o.CustomerId == id).Select(o => o.OrderId.ToString()).ToList();

            ViewData["Nummern"] = new SelectList(alleNummern);

            return PartialView("_Bestellungen");
        }

        public PartialViewResult ZeigeBestellDetails(string id, int bestellnummer)
        {
            return PartialView("_BestellDetails", context.Orders
                                                         .Include(o => o.OrderDetails)
                                                         .Include(o => o.Employee)
                                                         .SingleOrDefault(o => o.CustomerId == id && o.OrderId == bestellnummer));
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
