﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ajax_Northwind.Models
{
    public partial class Orders
    {
        public decimal OrderValue
        {
            get
            {
                return OrderDetails.Sum(o => o.Quantity * o.UnitPrice);
            }
        }
    }
}
