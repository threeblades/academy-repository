﻿$(document).ready(function ()
{
    $("#bestellNummern").empty();
    $("#bestellDetails").empty();

    // Click auf RadioButton
    $("input[name='details']").on("click", function ()
    {
        // Kundennummer merken
        var kundenNr = $(this).val();

        // <div> für Details löschen
        $("#bestellDetails").empty();

        // Ausgewählte Kundennummer per Ajax übertragen
        $.post("/Home/ZeigeBestellungenProKunde", { id: kundenNr }, function (data)
        {
            // Auswahl löschen
            $("#bestellNummern").empty();

            // Ergebnis des Ajax Aufruf in das <div> einfügen
            $("#bestellNummern").append(data);

            // Click auf die Bestellnummern
            $("#Nummern").on("change", function ()
            {
                var bestellNr = $(this).val();

                // Details für die Bestellung laden und anzeigen
                $("#bestellDetails").load("/Home/ZeigeBestellDetails", { id: kundenNr, bestellnummer: bestellNr });
            });
        });
    });
});