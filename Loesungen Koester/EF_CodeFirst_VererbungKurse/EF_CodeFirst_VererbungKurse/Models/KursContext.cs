﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EF_CodeFirst_VererbungKurse.Models;

namespace EF_CodeFirst_VererbungKurse.Models
{
    public class KursContext : DbContext
    {
        public DbSet<Kurs> Kurse { get; set; }

        public KursContext(DbContextOptions options)
            :base (options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OnlineKurs>().HasBaseType<Kurs>();
            modelBuilder.Entity<InHouseKurs>().HasBaseType<Kurs>();
        }

        public DbSet<EF_CodeFirst_VererbungKurse.Models.InHouseKurs> InHouseKurs { get; set; }

        public DbSet<EF_CodeFirst_VererbungKurse.Models.OnlineKurs> OnlineKurs { get; set; }
    }
}
