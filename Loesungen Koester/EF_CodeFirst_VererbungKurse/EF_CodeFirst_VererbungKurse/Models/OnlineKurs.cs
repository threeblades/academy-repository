﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_VererbungKurse.Models
{
    public class OnlineKurs : Kurs
    {
        [Url]
        public string URL { get; set; }
    }
}
