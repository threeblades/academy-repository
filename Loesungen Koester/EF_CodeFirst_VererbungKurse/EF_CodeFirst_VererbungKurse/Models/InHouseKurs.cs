﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_VererbungKurse.Models
{
    public class InHouseKurs : Kurs
    {
        public string Ort { get; set; }
        public DateTime Starttermin { get; set; }
        public int DauerInTagen { get; set; }
    }
}
