﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_VererbungKurse.Models
{
    public abstract class Kurs
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nachweis { get; set; }
    }
}
