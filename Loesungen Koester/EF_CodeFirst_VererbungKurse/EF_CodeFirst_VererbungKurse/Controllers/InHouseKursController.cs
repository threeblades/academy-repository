﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EF_CodeFirst_VererbungKurse.Models;

namespace EF_CodeFirst_VererbungKurse.Controllers
{
    public class InHouseKursController : Controller
    {
        private readonly KursContext _context;

        public InHouseKursController(KursContext context)
        {
            _context = context;
        }

        // GET: InHouseKurs
        public async Task<IActionResult> Index()
        {
            return View(await _context.InHouseKurs.ToListAsync());
        }

        // GET: InHouseKurs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inHouseKurs = await _context.InHouseKurs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (inHouseKurs == null)
            {
                return NotFound();
            }

            return View(inHouseKurs);
        }

        // GET: InHouseKurs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InHouseKurs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Ort,Starttermin,DauerInTagen,Id,Name,Nachweis")] InHouseKurs inHouseKurs)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inHouseKurs);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(inHouseKurs);
        }

        // GET: InHouseKurs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inHouseKurs = await _context.InHouseKurs.SingleOrDefaultAsync(m => m.Id == id);
            if (inHouseKurs == null)
            {
                return NotFound();
            }
            return View(inHouseKurs);
        }

        // POST: InHouseKurs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Ort,Starttermin,DauerInTagen,Id,Name,Nachweis")] InHouseKurs inHouseKurs)
        {
            if (id != inHouseKurs.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inHouseKurs);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InHouseKursExists(inHouseKurs.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(inHouseKurs);
        }

        // GET: InHouseKurs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inHouseKurs = await _context.InHouseKurs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (inHouseKurs == null)
            {
                return NotFound();
            }

            return View(inHouseKurs);
        }

        // POST: InHouseKurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inHouseKurs = await _context.InHouseKurs.SingleOrDefaultAsync(m => m.Id == id);
            _context.InHouseKurs.Remove(inHouseKurs);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InHouseKursExists(int id)
        {
            return _context.InHouseKurs.Any(e => e.Id == id);
        }
    }
}
