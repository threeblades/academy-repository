﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EF_CodeFirst_VererbungKurse.Models;

namespace EF_CodeFirst_VererbungKurse.Controllers
{
    public class OnlineKursController : Controller
    {
        private readonly KursContext _context;

        public OnlineKursController(KursContext context)
        {
            _context = context;
        }

        // GET: OnlineKurs
        public async Task<IActionResult> Index()
        {
            return View(await _context.OnlineKurs.ToListAsync());
        }

        // GET: OnlineKurs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineKurs = await _context.OnlineKurs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (onlineKurs == null)
            {
                return NotFound();
            }

            return View(onlineKurs);
        }

        // GET: OnlineKurs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: OnlineKurs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("URL,Id,Name,Nachweis")] OnlineKurs onlineKurs)
        {
            if (ModelState.IsValid)
            {
                _context.Add(onlineKurs);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(onlineKurs);
        }

        // GET: OnlineKurs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineKurs = await _context.OnlineKurs.SingleOrDefaultAsync(m => m.Id == id);
            if (onlineKurs == null)
            {
                return NotFound();
            }
            return View(onlineKurs);
        }

        // POST: OnlineKurs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("URL,Id,Name,Nachweis")] OnlineKurs onlineKurs)
        {
            if (id != onlineKurs.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(onlineKurs);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OnlineKursExists(onlineKurs.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(onlineKurs);
        }

        // GET: OnlineKurs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var onlineKurs = await _context.OnlineKurs
                .SingleOrDefaultAsync(m => m.Id == id);
            if (onlineKurs == null)
            {
                return NotFound();
            }

            return View(onlineKurs);
        }

        // POST: OnlineKurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var onlineKurs = await _context.OnlineKurs.SingleOrDefaultAsync(m => m.Id == id);
            _context.OnlineKurs.Remove(onlineKurs);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OnlineKursExists(int id)
        {
            return _context.OnlineKurs.Any(e => e.Id == id);
        }
    }
}
