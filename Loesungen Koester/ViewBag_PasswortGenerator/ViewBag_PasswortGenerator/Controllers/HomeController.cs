﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ViewBag_PasswortGenerator.Models;

namespace ViewBag_PasswortGenerator.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View(new PasswortConfig());
        }

        [HttpPost]
        public IActionResult Index(PasswortConfig pwdConfig)
        {
            PasswortGenerator generator = new PasswortGenerator();

            // ViewBag
            ViewBag.Passwort = generator.Generiere(pwdConfig);

            // ViewData
            // Fünf Passwörter generieren
            List<string> pwds = new List<string>();

            for (int i = 0; i < 5; i++)
            {
                pwds.Add(generator.Generiere(pwdConfig));
            }

            // Liste im ViewData speichern
            ViewData["Passwörter"] = pwds;

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}