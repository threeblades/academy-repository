﻿using System;

namespace ViewBag_PasswortGenerator.Models
{
    public class PasswortGenerator
    {
        private readonly Random random = new Random();

        public string Generiere(PasswortConfig config)
        {
            char[] dasPasswort = new char[config.Länge];

            int index = 0;

            // Array mit kleinen Buchstaben zwischen a und z füllen
            for (int i = 0; i < config.Länge; i++)
            {
                dasPasswort[i] = (char)random.Next('a', 'z' + 1);
            }

            for (int i = 0; i < config.AnzahlGrossbuchstaben; i++)
            {
                do
                {
                    // Zufällige Postion im Array
                    index = random.Next(config.Länge);
                }
                // Solange eine Position suchen, bis ein kleiner Buchstabe gefunden wurde
                while ((dasPasswort[index] >= 'a' && dasPasswort[index] <= 'z') == false);

                // Zufälliger Großbuchstabe
                char grossbuchstabe = (char)random.Next('A', 'Z' + 1);

                dasPasswort[index] = grossbuchstabe;
            }

            for (int i = 0; i < config.AnzahlZiffern; i++)
            {
                do
                {
                    // Zufällige Postion im Array
                    index = random.Next(config.Länge);
                }
                // Solange eine Position suchen bis ein kleiner Buchstabe gefunden wurde
                while ((dasPasswort[index] >= 'a' && dasPasswort[index] <= 'z') == false);

                // Zufällige Ziffer einsetzen
                char ziffer = (char)random.Next('0', '9' + 1);

                dasPasswort[index] = ziffer;
            }

            for (int i = 0; i < config.AnzahlSonderzeichen; i++)
            {
                do
                {
                    // Zufällige Postion im Array
                    index = random.Next(config.Länge);
                }
                // Solange Position suchen bis ein kleiner Buchstabe gefunden wurde
                while ((dasPasswort[index] >= 'a' && dasPasswort[index] <= 'z') == false);

                // Sonderzeichen einsetzen
                char sonderzeichen = (char)random.Next('!', '/' + 1);

                dasPasswort[index] = sonderzeichen;
            }

            return new string(dasPasswort);
        }
    }
}