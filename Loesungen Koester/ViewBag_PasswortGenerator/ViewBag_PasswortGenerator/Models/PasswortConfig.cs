﻿using System.ComponentModel.DataAnnotations;

namespace ViewBag_PasswortGenerator.Models
{
    public class PasswortConfig
    {
        // Das Display wird vom HTML-Helper LabelFor ausgewertet

        //[Display(Name = "Wie viele Zeichen:")]
        public int Länge { get; set; }

        [Display(Name = "Wie viele Ziffern:")]
        public int AnzahlZiffern { get; set; }

        [Display(Name = "Wie viele Sonderzeichen:")]
        public int AnzahlSonderzeichen { get; set; }

        [Display(Name = "Wie viele Großbuchstaben:")]
        public int AnzahlGrossbuchstaben { get; set; }

        public PasswortConfig()
        {
            // Default-Werte
            Länge = 8;
            AnzahlZiffern = 1;
            AnzahlSonderzeichen = 1;
            AnzahlGrossbuchstaben = 1;
        }
    }
}