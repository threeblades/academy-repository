﻿namespace Validierung_ISBN.Models
{
    public static class ISBNCheck
    {
        public static bool CheckISBN10(string isbn)
        {
            if (isbn.Length != 10)
            {
                return false;
            }

            int checkDigit = (isbn[9].ToString() == "X") ? 10 : (isbn[9] - '0');

            int sum = 0;

            for (int i = 0; i < 9; i++)
            {
                sum += (i + 1) * int.Parse(isbn[i].ToString());
            }

            return checkDigit == (sum % 11);
        }

        public static bool CheckISBN13(string isbn)
        {
            if (isbn.Length != 13)
            {
                return false;
            }

            int checkDigit = isbn[12] - '0';

            int sum = 0;

            for (int i = 0; i < 12; i++)
            {
                if (((i + 1) % 2) == 0)
                {
                    sum += 3 * (isbn[i] - '0');
                }
                else
                {
                    sum += isbn[i] - '0';
                }
            }

            return checkDigit == (10 - (sum % 10)) % 10;
        }
    }
}