﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Validierung_ISBN.Models
{
    public class Book
    {
        [Required(ErrorMessage = "Pflichtfeld")]
        [StringLength(50, MinimumLength = 2)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Pflichtfeld")]
        [RegularExpression("^([0-9]{13})|([0-9]{9}[0-9Xx])$", ErrorMessage = "Falsches Format!")]
        // Attribut-Klasse für serverseitige Validierung
        [ISBN]
        // Remote-Validierung des Clients ruft Methode beim Server auf
        //[Remote(action: "CheckISBN", controller: "Home", ErrorMessage = "Remote-Methode: Die ISBN nicht korrekt!")]
        public string ISBN { get; set; }
    }
}