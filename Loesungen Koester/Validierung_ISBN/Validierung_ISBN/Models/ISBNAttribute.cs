﻿using System.ComponentModel.DataAnnotations;

namespace Validierung_ISBN.Models
{
    public class ISBNAttribute : ValidationAttribute
    {
        // Konstruktoren
        public ISBNAttribute()
        {
            this.ErrorMessage = "Attribut-Klasse: Die ISBN ist nicht korrekt!";
        }

        public ISBNAttribute(string ErrorMessage)
        {
            this.ErrorMessage = ErrorMessage;
        }

        // Validierungsmethode
        public override bool IsValid(object value)
        {
            var valid = false;

            string isbn = value as string;

            if (isbn != null)
            {
                if (isbn.Length == 10)
                {
                    valid = ISBNCheck.CheckISBN10(isbn);
                }

                if (isbn.Length == 13)
                {
                    valid = ISBNCheck.CheckISBN13(isbn);
                }
            }

            return valid;
        }
    }
}