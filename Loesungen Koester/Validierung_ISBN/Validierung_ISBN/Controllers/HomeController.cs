﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Validierung_ISBN.Models;

namespace Validierung_ISBN.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Book book)
        {
            if (ModelState.IsValid == false)
            {
                return View();
            }

            return View(book);
        }

        // Methode für Remote-Validierung
        public IActionResult CheckISBN(string isbn)
        {
            bool valid = false;

            if (isbn != null)
            {
                if (isbn.Length == 10)
                {
                    valid = ISBNCheck.CheckISBN10(isbn);
                }

                if (isbn.Length == 13)
                {
                    valid = ISBNCheck.CheckISBN13(isbn);
                }
            }

            return Json(valid);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}