﻿using System;

namespace OpenLiga.Models
{
    public class Match
    {
        public int MatchID { get; set; }

        public DateTime MatchDateTime { get; set; }

        public Team Team1 { get; set; }

        public Team Team2 { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1} : {2}", MatchDateTime.ToString("f"), Team1, Team2);
        }
    }
}