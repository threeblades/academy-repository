﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace OpenLiga.Models
{
    public class OpenLigaRepository
    {
        private readonly HttpClient client = new HttpClient();

        public List<Match> Matches { get; set; }

        public List<Team> Teams { get; set; }

        public OpenLigaRepository()
        {
            DownloadMatchesAsync().Wait();
        }

        public async Task DownloadMatchesAsync()
        {
            client.BaseAddress = new Uri("https://www.openligadb.de/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                Matches = await GetMatchesAsync("api/getmatchdata/bl1/2019").ConfigureAwait(false);

                DownloadTeamIcons();
            }
            catch (Exception ex)
            {
                await Task.FromException(ex);
            }
        }

        private void DownloadTeamIcons()
        {
            // Liste mit den Teams erzeugen (jedes Team nur einmal)
            IEnumerable<Match> filteredList = Matches.GroupBy(m => m.Team1.TeamId).Select(group => group.First());

            Teams = filteredList.Select(f => f.Team1).ToList();

            WebClient webClient = new WebClient();

            foreach (Team team in Teams)
            {
                team.ImageData = webClient.DownloadData(team.TeamIconUrl);
            }
        }

        private async Task<List<Match>> GetMatchesAsync(string path)
        {
            List<Match> matches = null;

            HttpResponseMessage response = await client.GetAsync(path);

            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();

                matches = JsonConvert.DeserializeObject<List<Match>>(responseString);
            }

            return matches;
        }
    }
}
