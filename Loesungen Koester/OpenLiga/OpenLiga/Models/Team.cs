﻿namespace OpenLiga.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public string TeamIconUrl { get; set; }

        public byte[] ImageData { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", TeamId, TeamName, ImageData?.Length);
        }
    }
}