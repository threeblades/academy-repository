﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenLiga.Models;

namespace OpenLiga.Controllers
{
    public class HomeController : Controller
    {
        private readonly OpenLigaRepository ligaRepository;

        public HomeController(OpenLigaRepository ligaRepository)
        {
            this.ligaRepository = ligaRepository;
        }

        public IActionResult Index()
        {
            return View(ligaRepository.Matches.Take(20));
        }

        public IActionResult GetImage(int Id)
        {
            Team team = ligaRepository.Teams.Single(t => t.TeamId == Id);

            return File(team.ImageData, "image/jpg");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
