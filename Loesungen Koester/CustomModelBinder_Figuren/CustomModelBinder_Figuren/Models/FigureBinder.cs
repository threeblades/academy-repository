﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;

namespace CustomModelBinder_Figuren.Models
{
    public class FigureBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {

            

            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            if (bindingContext.ModelMetadata.ModelType == typeof(Circle))
            {
                // Die Daten aus dem Formular auslesen (Attribut name="")
                ValueProviderResult isRadius = bindingContext.ValueProvider.GetValue("IsRadius");
                ValueProviderResult value = bindingContext.ValueProvider.GetValue("Value");

                Circle circle = new Circle
                {
                    IsRadius = bool.Parse(isRadius.FirstValue),
                    Value = double.Parse(value.FirstValue)
                };

                bindingContext.Result = ModelBindingResult.Success(circle);

                return Task.CompletedTask;
            }

            if (bindingContext.ModelMetadata.ModelType == typeof(Rectangle))
            {
                // Die Daten aus dem Formular auslesen (Attribut name="")
                ValueProviderResult sideA = bindingContext.ValueProvider.GetValue("SideA");
                ValueProviderResult sideB = bindingContext.ValueProvider.GetValue("SideB");

                Rectangle rectangle = new Rectangle
                {
                    SideA = double.Parse(sideA.FirstValue),
                    SideB = double.Parse(sideB.FirstValue)
                };

                bindingContext.Result = ModelBindingResult.Success(rectangle);

                return Task.CompletedTask;
            }
            
            return Task.CompletedTask;
        }
    }
}