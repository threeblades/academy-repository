﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CustomModelBinder_Figuren.Models
{
    [ModelBinder(BinderType = typeof(FigureBinder))]
    public class Rectangle : GeoFigure
    {
        [Required]
        [Display(Name = "Länge")]
        public double SideA { get; set; }

        [Required]
        [Display(Name = "Höhe")]
        public double SideB { get; set; }

        public override double Area
        {
            get { return SideA * SideB; }
        }

        public override double Circumference
        {
            get { return 2 * (SideA + SideB); }
        }
    }
}