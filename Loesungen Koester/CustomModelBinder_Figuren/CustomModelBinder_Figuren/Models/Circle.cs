﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace CustomModelBinder_Figuren.Models
{
    [ModelBinder(BinderType = typeof(FigureBinder))]
    public class Circle : GeoFigure
    {
        [Required]
        [Display(Name = "Wert")]
        public double Value { get; set; }

        [Required]
        [Display(Name = "Wert ist Radius")]
        public bool IsRadius { get; set; }

        public override double Area
        {
            get
            {
                // Wert als Radius oder Durchmesser verwenden
                double length = IsRadius ? Value : Value / 2;
                return Math.PI * (length * length);
            }
        }

        [DisplayFormat(DataFormatString ="{0:N3}")]
        public override double Circumference
        {
            get
            {
                // Wert als Radius oder Durchmesser verwenden
                double length = IsRadius ? Value : Value / 2;
                return Math.PI * (2 * length);
            }
        }
    }
}