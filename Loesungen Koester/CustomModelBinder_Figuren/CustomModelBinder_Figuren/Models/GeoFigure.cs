﻿namespace CustomModelBinder_Figuren.Models
{
    public abstract class GeoFigure
    {
        public abstract double Area { get; }
        public abstract double Circumference { get; }
    }
}