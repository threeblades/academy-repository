﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CustomModelBinder_Figuren.Models;

namespace CustomModelBinder_Figuren.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Circle()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Circle(Circle figure)
        {
            if (figure != null && ModelState.IsValid)
            {
                return View(figure);
            }
            return View();
        }

        public IActionResult Rectangle()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Rectangle(Rectangle figure)
        {
            if (figure != null && ModelState.IsValid)
            {
                return View(figure);
            }

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
