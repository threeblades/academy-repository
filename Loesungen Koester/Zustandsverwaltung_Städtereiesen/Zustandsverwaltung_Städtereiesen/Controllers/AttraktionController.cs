﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Zustandsverwaltung_Städtereiesen.Models;

namespace Zustandsverwaltung_Städtereiesen.Controllers
{
    public class AttraktionController : Controller
    {
        private readonly NetDBContext _context;

        public AttraktionController(NetDBContext context)
        {
            _context = context;
        }

        // GET: Attraktion
        public async Task<IActionResult> Index()
        {
            var netDBContext = _context.Attraktion.Include(a => a.Stadt);
            return View(await netDBContext.ToListAsync());
        }

        // GET: Attraktion/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var attraktion = await _context.Attraktion
                .Include(a => a.Stadt)
                .SingleOrDefaultAsync(m => m.AttraktionId == id);
            if (attraktion == null)
            {
                return NotFound();
            }

            return View(attraktion);
        }

        // GET: Attraktion/Create
        public IActionResult Create()
        {
            ViewData["StadtId"] = new SelectList(_context.Stadt, "StadtId", "Name");
            return View();
        }

        // POST: Attraktion/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AttraktionId,Name,Anschrift,Bewertung,Url,StadtId")] Attraktion attraktion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(attraktion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StadtId"] = new SelectList(_context.Stadt, "StadtId", "Name", attraktion.StadtId);
            return View(attraktion);
        }

        // GET: Attraktion/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var attraktion = await _context.Attraktion.SingleOrDefaultAsync(m => m.AttraktionId == id);
            if (attraktion == null)
            {
                return NotFound();
            }
            ViewData["StadtId"] = new SelectList(_context.Stadt, "StadtId", "Name", attraktion.StadtId);
            return View(attraktion);
        }

        // POST: Attraktion/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AttraktionId,Name,Anschrift,Bewertung,Url,StadtId")] Attraktion attraktion)
        {
            if (id != attraktion.AttraktionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(attraktion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AttraktionExists(attraktion.AttraktionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StadtId"] = new SelectList(_context.Stadt, "StadtId", "Name", attraktion.StadtId);
            return View(attraktion);
        }

        // GET: Attraktion/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var attraktion = await _context.Attraktion
                .Include(a => a.Stadt)
                .SingleOrDefaultAsync(m => m.AttraktionId == id);
            if (attraktion == null)
            {
                return NotFound();
            }

            return View(attraktion);
        }

        // POST: Attraktion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var attraktion = await _context.Attraktion.SingleOrDefaultAsync(m => m.AttraktionId == id);
            _context.Attraktion.Remove(attraktion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AttraktionExists(int id)
        {
            return _context.Attraktion.Any(e => e.AttraktionId == id);
        }
    }
}
