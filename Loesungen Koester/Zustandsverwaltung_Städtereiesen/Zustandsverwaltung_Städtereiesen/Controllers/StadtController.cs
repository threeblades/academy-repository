﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Zustandsverwaltung_Städtereiesen.Models;

namespace Zustandsverwaltung_Städtereiesen.Controllers
{
    public class StadtController : Controller
    {
        private readonly NetDBContext _context;

        public StadtController(NetDBContext context)
        {
            _context = context;
        }

        // GET: Stadt
        public async Task<IActionResult> Index()
        {
            var netDBContext = _context.Stadt.Include(s => s.Bundesland).Include(s => s.Attraktion);
            return View(await netDBContext.ToListAsync());
        }

        // GET: Stadt/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stadt = await _context.Stadt
                .Include(s => s.Bundesland)
                .Include(s => s.Attraktion)
                .SingleOrDefaultAsync(m => m.StadtId == id);
            if (stadt == null)
            {
                return NotFound();
            }

            return View(stadt);
        }

        // GET: Stadt/Create
        public IActionResult Create()
        {
            ViewData["BundeslandId"] = new SelectList(_context.Bundesland, "BundeslandId", "Name");
            return View();
        }

        // POST: Stadt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StadtId,Name,BundeslandId")] Stadt stadt)
        {
            if (ModelState.IsValid)
            {
                _context.Add(stadt);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BundeslandId"] = new SelectList(_context.Bundesland, "BundeslandId", "Name", stadt.BundeslandId);
            return View(stadt);
        }

        // GET: Stadt/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stadt = await _context.Stadt.SingleOrDefaultAsync(m => m.StadtId == id);
            if (stadt == null)
            {
                return NotFound();
            }
            ViewData["BundeslandId"] = new SelectList(_context.Bundesland, "BundeslandId", "Name", stadt.BundeslandId);
            return View(stadt);
        }

        // POST: Stadt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StadtId,Name,BundeslandId")] Stadt stadt)
        {
            if (id != stadt.StadtId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(stadt);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StadtExists(stadt.StadtId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BundeslandId"] = new SelectList(_context.Bundesland, "BundeslandId", "Name", stadt.BundeslandId);
            return View(stadt);
        }

        // GET: Stadt/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stadt = await _context.Stadt
                .Include(s => s.Bundesland)
                .SingleOrDefaultAsync(m => m.StadtId == id);
            if (stadt == null)
            {
                return NotFound();
            }

            return View(stadt);
        }

        // POST: Stadt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var stadt = await _context.Stadt.SingleOrDefaultAsync(m => m.StadtId == id);
            _context.Stadt.Remove(stadt);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StadtExists(int id)
        {
            return _context.Stadt.Any(e => e.StadtId == id);
        }

        /*
         * Neue Methoden
         */

        public IActionResult Top10()
        {
            IEnumerable<Stadt> top10 = _context.Stadt.Include(s => s.Attraktion);
            top10 = top10.OrderByDescending(s => s.Bewertung).Take(10);
            return View(top10);
        }

        public IActionResult NachBundesland()
        {
            ViewData["bundesland"] = new SelectList(_context.Bundesland, "BundeslandId", "Name");
            return View();
        }

        public PartialViewResult StädteNachBundesland(int bundesland)
        {
            return PartialView("_StädteAuflisten", _context.Stadt.Include(s => s.Attraktion).Where(s => s.Bundesland.BundeslandId == bundesland).ToList());
        }

        public IActionResult Suche()
        {
            // Vorherige Suchbegriffe aus der Session laden
            string sessionDaten = HttpContext.Session.GetString("suchstrings");

            // Alle Städtenamen für das input Element speichern
            ViewData["Städtenamen"] = _context.Stadt.Select(s => s.Name).ToList();

            // Ist ein Suchstring in der Session gespeichert?
            if (sessionDaten != null)
            {
                // Am Komma zerlegen
                string[] suchstrings = sessionDaten.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                // Doppelte Einträge entfernen
                suchstrings = suchstrings.Distinct().ToArray();

                // Nur die letzten 5 Suchen anzeigen
                if (suchstrings.Length > 5)
                {
                    suchstrings = suchstrings.Skip(suchstrings.Length - 5).ToArray();
                }

                // "Aufgeräumte" Suchbegriffe in der Session speichern
                HttpContext.Session.SetString("suchstrings", string.Join(",", suchstrings) + ",");

                return View(suchstrings);
            }

            return View();
        }

        // Partielle View für die Suchergebnisse
        public PartialViewResult ZeigeSuchErgebnisse(string suchString)
        {
            // Vorherige Suchbegriffe aus der Session laden
            string sessionDaten = HttpContext.Session.GetString("suchstrings");

            // Suchstring in der Session speichern
            HttpContext.Session.SetString("suchstrings", sessionDaten += suchString + ",");

            return PartialView("_StädteAuflisten", _context.Stadt.Include(s => s.Attraktion).Where(s => s.Name.ToLower().Contains(suchString.ToLower())).ToList());
        }

    }
}
