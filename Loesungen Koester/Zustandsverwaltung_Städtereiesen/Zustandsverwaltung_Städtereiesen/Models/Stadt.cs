﻿using System;
using System.Collections.Generic;

namespace Zustandsverwaltung_Städtereiesen.Models
{
    public partial class Stadt
    {
        public Stadt()
        {
            Attraktion = new HashSet<Attraktion>();
        }

        public int StadtId { get; set; }
        public string Name { get; set; }
        public int BundeslandId { get; set; }

        public Bundesland Bundesland { get; set; }
        public ICollection<Attraktion> Attraktion { get; set; }
    }
}
