﻿using System;
using System.Collections.Generic;

namespace Zustandsverwaltung_Städtereiesen.Models
{
    public partial class Bundesland
    {
        public Bundesland()
        {
            Stadt = new HashSet<Stadt>();
        }

        public int BundeslandId { get; set; }
        public string Name { get; set; }

        public ICollection<Stadt> Stadt { get; set; }
    }
}
