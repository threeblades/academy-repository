﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Zustandsverwaltung_Städtereiesen.Models
{
    public partial class Stadt
    {
        [Display(Name = "Anzahl Zeile")]
        public int AnzahlZiele
        {
            get { return Attraktion.Count; }
        }

        [DisplayFormat(DataFormatString = "{0:F}")]
        public double Bewertung
        {
            get
            {
                return Attraktion.Count() > 0 ? Attraktion.Average(a => a.Bewertung) : 0;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Name, Bewertung);
        }
    }
}
