﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Zustandsverwaltung_Städtereiesen.Models
{
    public partial class Attraktion
    {
        public int AttraktionId { get; set; }
        public string Name { get; set; }
        public string Anschrift { get; set; }
        public int Bewertung { get; set; }
        [Url]
        public string Url { get; set; }
        public int StadtId { get; set; }

        public Stadt Stadt { get; set; }
    }
}
