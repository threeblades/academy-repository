﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Zustandsverwaltung_Städtereiesen.Models
{
    public partial class NetDBContext : DbContext
    {
        public virtual DbSet<Attraktion> Attraktion { get; set; }
        public virtual DbSet<Bundesland> Bundesland { get; set; }
        public virtual DbSet<Stadt> Stadt { get; set; }

        public NetDBContext(DbContextOptions options) : base (options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Attraktion>(entity =>
            {
                entity.Property(e => e.Anschrift)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Stadt)
                    .WithMany(p => p.Attraktion)
                    .HasForeignKey(d => d.StadtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Attraktio__Stadt__44FF419A");
            });

            modelBuilder.Entity<Bundesland>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stadt>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Bundesland)
                    .WithMany(p => p.Stadt)
                    .HasForeignKey(d => d.BundeslandId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Stadt__Bundeslan__4222D4EF");
            });
        }
    }
}
