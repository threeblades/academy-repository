﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdventureWorks_Personal.Models
{
    public class EmployeeViewModel
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime BirthDate { get; set; }

        public string Gender { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime HireDate { get; set; }

        public short VacationHours { get; set; }

        public short SickLeaveHours { get; set; }
    }
}