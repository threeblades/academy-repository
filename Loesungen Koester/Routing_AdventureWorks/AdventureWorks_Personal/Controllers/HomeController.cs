﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AdventureWorks_Personal.Models;
using Microsoft.EntityFrameworkCore;

namespace AdventureWorks_Personal.Controllers
{
    public class HomeController : Controller
    {
        private readonly AdventureWorksContext _context;

        public HomeController(AdventureWorksContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            List<EmployeeViewModel> employees = await GetAllEmployees();

            return View(employees);
        }

        private async Task<List<EmployeeViewModel>> GetAllEmployees()
        {
            var adventureWorksContext = await _context.Employee.Include(e => e.BusinessEntity).ToListAsync();

            List<EmployeeViewModel> employees = new List<EmployeeViewModel>();

            adventureWorksContext.ForEach(e =>
            {
                EmployeeViewModel employeeViewModel = new EmployeeViewModel()
                {
                    FirstName = e.BusinessEntity.FirstName,
                    MiddleName = e.BusinessEntity.MiddleName,
                    LastName = e.BusinessEntity.LastName,
                    JobTitle = e.JobTitle,
                    BirthDate = e.BirthDate,
                    Gender = e.Gender,
                    HireDate = e.HireDate,
                    VacationHours = e.VacationHours,
                    SickLeaveHours = e.SickLeaveHours
                };

                employees.Add(employeeViewModel);
            });

            return employees;
        }

        // Für die Aufgabe hinzugefügt
        [HttpGet("/HR")]
        public async Task<IActionResult> Top30NachAlter()
        {
            var employees = await GetAllEmployees();

            employees = employees.OrderBy(e => e.BirthDate).Take(30).ToList();

            return View("Index", employees);
        }

        // Für die Aufgabe hinzugefügt
        [HttpGet("/HR/Employees/{gender:regex(^(male|female)$)}")]
        public async Task<IActionResult> NachGeschlecht(string gender)
        {
            var employees = await GetAllEmployees();

            employees = employees.Where(e => e.Gender == gender[0].ToString().ToUpper()).ToList();

            return View("Index", employees);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
