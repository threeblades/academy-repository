﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity_FilmeBewerten.Data;
using Identity_FilmeBewerten.Models;
using Microsoft.AspNetCore.Authorization;

namespace Identity_FilmeBewerten.Controllers
{
    [Authorize]
    public class FilmController : Controller
    {
        private readonly FilmContext _context;

        #region Generierte Methoden

        public FilmController(FilmContext context)
        {
            _context = context;
        }

        // GET: Film
        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            // Include eingefügt
            return View(await _context.Films.Include(f => f.Votes).ToListAsync());
        }

        // GET: Film/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var film = await _context.Films
                .SingleOrDefaultAsync(m => m.ID == id);
            if (film == null)
            {
                return NotFound();
            }

            return View(film);
        }

        // GET: Film/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Film/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Year,Director")] Film film)
        {
            if (ModelState.IsValid)
            {
                _context.Add(film);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(film);
        }

        // GET: Film/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var film = await _context.Films.SingleOrDefaultAsync(m => m.ID == id);
            if (film == null)
            {
                return NotFound();
            }
            return View(film);
        }

        // POST: Film/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Year,Director")] Film film)
        {
            if (id != film.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(film);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FilmExists(film.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(film);
        }

        // GET: Film/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var film = await _context.Films
                .SingleOrDefaultAsync(m => m.ID == id);
            if (film == null)
            {
                return NotFound();
            }

            return View(film);
        }

        // POST: Film/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var film = await _context.Films.SingleOrDefaultAsync(m => m.ID == id);
            _context.Films.Remove(film);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FilmExists(int id)
        {
            return _context.Films.Any(e => e.ID == id);
        }

        #endregion

        [AllowAnonymous]
        public IActionResult ShowTopVoted()
        {
            // Alle Abstimmungen nach Filmen gruppiert
            // Filmtitel und Durchschnittsbewertung wird im Hilfsobjekt zwischengespeichert
            var top = from vote in _context.Votes
                      group vote by vote.Film into filmgroup
                      select new Bewertung
                      {
                          FilmTitel = filmgroup.Key.Title,
                          Durchschnittspunkte = filmgroup.Average(v => v.Value)
                      };

            // Die 10 besten Filme anzeigen
            return View(top.OrderByDescending(b => b.Durchschnittspunkte).Take(10).ToList());
        }

        public ActionResult Vote(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Film filmModel = _context.Films.Find(id);

            if (filmModel == null)
            {
                return NotFound();
            }

            return View(filmModel);
        }

        [HttpPost]
        public ActionResult Vote(int? id, int? vote)
        {
            if (id == null || vote == null)
            {
                return NotFound();
            }

            Film filmModel = _context.Films.Find(id);

            if (filmModel == null)
            {
                return NotFound();
            }

            if (vote != null)
            {
                filmModel.Votes.Add(new Vote { Value = vote.Value });
                _context.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}
