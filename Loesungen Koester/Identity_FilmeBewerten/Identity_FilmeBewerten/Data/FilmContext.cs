﻿using Identity_FilmeBewerten.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_FilmeBewerten.Data
{
    public class FilmContext : DbContext
    {
        public DbSet<Film> Films { get; set; }
        public DbSet<Vote> Votes { get; set; }

        public FilmContext(DbContextOptions options) : base(options)
        {
            this.Database.EnsureCreated();

            if (Films.Count() == 0)
            {
                List<Film> filme = new List<Film>
                {
                    new Film { Title = "Star Wars", Year = 1977, Director ="George Lucas"},
                    new Film { Title = "The Empire Strikes Back", Year = 1980, Director = "Irvin Kershner"},

                    new Film { Title = "Return of the Jedi", Year = 1983, Director = "Richard Marquand"},
                    new Film { Title = "Alien", Year = 1979, Director ="Ridley Scott"},

                    new Film { Title = "The Terminator", Year = 1984, Director ="James Cameron"},
                    new Film { Title = "The Godfahter", Year = 1972, Director ="Francis Ford Coppola"},

                    new Film { Title = "Blade Runner", Year = 1982, Director ="Ridley Scott"},
                    new Film { Title = "2001: A Space Odyssey", Year = 1968, Director = "Stanley Kubrick"},

                    new Film { Title = "Star Trek: The Motion Picture", Year = 1979, Director = "Robert Wise"},
                    new Film { Title = "Ghostbusters", Year = 1984, Director = "Ivan Reitman"},

                    new Film { Title = "The Dark Knight", Year = 2008, Director = "Christopher Nolan"}

                };

                Random random = new Random();

                foreach (Film film in filme)
                {
                    // Zufällige Anzahl an Bewertungen
                    for (int i = 0; i < random.Next(1, 6); i++)
                    {
                        // Zufällige Bewertung zwischen 7 und 10 Punkten pro Film
                        film.Votes.Add(new Vote() { Film = film, Value = random.Next(7, 11) });
                    }
                }

                filme.ForEach(f => this.Films.Add(f));

                this.SaveChanges();
            }
        }
    }
}
