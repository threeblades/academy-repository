﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Identity_FilmeBewerten.Models
{
    public class Film
    {
        public int ID { get; set; }

        [Required, MaxLength(100)]
        [Display(Name="Titel:")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Erscheinungsjahr:")]
        public int Year { get; set; }

        [Required, MaxLength(100)]
        [Display(Name = "Regie:")]
        public string Director { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }

        public Film()
        {
            Votes = new List<Vote>();
        }
    }
}