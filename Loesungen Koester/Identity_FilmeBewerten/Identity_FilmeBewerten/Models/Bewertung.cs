﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Identity_FilmeBewerten.Models
{
    public class Bewertung
    {
        public string FilmTitel { get; set; }

        [DisplayFormat(DataFormatString = "{0:F}")]
        public double Durchschnittspunkte { get; set; }
    }
}