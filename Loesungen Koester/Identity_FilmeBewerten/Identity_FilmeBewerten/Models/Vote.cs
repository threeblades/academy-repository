﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity_FilmeBewerten.Models
{
    public class Vote
    {
        public int ID { get; set; }

        [Required, Range(1, 10)]
        public int Value { get; set; }

        [Required, ForeignKey("Film")]
        public int FilmID { get; set; }

        public virtual Film Film { get; set; }
    }
}