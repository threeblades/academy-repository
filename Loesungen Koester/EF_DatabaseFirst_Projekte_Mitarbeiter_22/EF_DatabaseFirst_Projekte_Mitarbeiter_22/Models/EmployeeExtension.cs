﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace EF_DatabaseFirst_Projekte_Mitarbeiter_22.Models
{
    [ModelMetadataType(typeof(Metadata))]
    public partial class Employees
    {
        private sealed class Metadata
        {
            [Required]
            [Display(Name = "Vorname:")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Nachname:")]
            public string LastName { get; set; }

            [Display(Name = "Aufgabe:")]
            public string ProjectRole { get; set; }

            [Display(Name = "Projekt:")]
            public Projects Project { get; set; }

            [Display(Name = "Projekt:")]
            public int? ProjectId { get; set; }
        }
    }
}