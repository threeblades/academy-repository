﻿using System;
using System.Collections.Generic;
using System.Text;
using Identity_Todo.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Identity_Todo.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        // Hinzugefügtes DdSet
        // Anschließend per Migrations die Datenbank anpassen
        public DbSet<ToDo> ToDo { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
