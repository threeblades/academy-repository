﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_Todo.Models
{
    public class ToDo
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(80)]
        [Display(Name = "Beschreibung:")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Fällig bis:")]
        [DataType(DataType.Date)]
        public DateTime DueDate { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        [Display(Name = "Anlegedatum:")]
        public DateTime InsertDate { get; set; }

        [Display(Name = "Erledigt:")]
        public bool IsDone { get; set; }

        // Speichern zu welchem Benutzer das Aufgaben-Objekt gehört
        // Dazu muss die Kontext-Klasse der Benutzerverwaltung verwendet werden
        public virtual IdentityUser User { get; set; }

        public ToDo()
        {
            InsertDate = DateTime.Now;
        }
    }
}
