﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity_Todo.Data;
using Identity_Todo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Identity_Todo.Controllers
{
    [Authorize]
    public class ToDoController : Controller
    {
        private readonly ApplicationDbContext _context;
        readonly UserManager<IdentityUser> userManager;

        public ToDoController(ApplicationDbContext context,
                              UserManager<IdentityUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        // GET: ToDo
        public async Task<IActionResult> Index()
        {
            // Angemeldeten Benutzer ermitteln
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            // Nur die Todo-Objekte des angemeldeten Benutzers laden
            return View(await _context.ToDo.Include(t => t.User).Where(todo => todo.User.Id == currentUser.Id).ToListAsync());

            // Generierter Code
            //return View(await _context.ToDo.ToListAsync());
        }

        // Hinzugefügte Methode
        public async Task<IActionResult> OpenTasks()
        {
            // Angemeldeten Benutzer ermitteln
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            return View("Index", await _context.ToDo.Where(todo => todo.User.Id == currentUser.Id && todo.IsDone == false).ToListAsync());
        }

        // Hinzugefügte Methode
        public async Task<IActionResult> ClosedTasks()
        {
            // Angemeldeten Benutzer ermitteln
            IdentityUser currentUser = await userManager.GetUserAsync(HttpContext.User);

            return View("Index", await _context.ToDo.Where(todo => todo.User.Id == currentUser.Id && todo.IsDone == true).ToListAsync());
        }

        // GET: ToDo/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDo = await _context.ToDo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDo == null)
            {
                return NotFound();
            }

            return View(toDo);
        }

        // GET: ToDo/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,DueDate,IsDone")] ToDo toDo)
        {
            if (ModelState.IsValid)
            {
                // **** Hinzugefügter Code ****
                // Angemeldeten Benutzer ermitteln
                // und diesem dem Todo-Objekt hinzufügen
                toDo.User = await userManager.FindByNameAsync(User.Identity.Name);

                _context.Add(toDo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(toDo);
        }

        // GET: ToDo/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDo = await _context.ToDo.FindAsync(id);

            if (toDo == null)
            {
                return NotFound();
            }

            // **** Hinzugefügter Code ****
            // Identität des Benutzers überprüfen um zu verhindern, dass ein angemeldeter 
            // Benutzer ein Todo eines anderen Anwenders editiert
            IdentityUser currentUser = await userManager.FindByNameAsync(User.Identity.Name);

            if (toDo.User != currentUser)
            {
                return NotFound();
            }

            return View(toDo);
        }

        // POST: ToDo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,DueDate,IsDone")] ToDo toDo)
        {
            if (id != toDo.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toDo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoExists(toDo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDo);
        }

        // GET: ToDo/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDo = await _context.ToDo
                .FirstOrDefaultAsync(m => m.Id == id);

            if (toDo == null)
            {
                return NotFound();
            }

            // **** Hinzugefügter Code ****
            // Identität des Benutzers überprüfen um zu verhindern, dass ein angemeldeter 
            // Benutzer ein Todo eines anderen Anwenders editiert
            IdentityUser currentUser = await userManager.FindByNameAsync(User.Identity.Name);

            if (toDo.User != currentUser)
            {
                return NotFound();
            }

            return View(toDo);
        }

        // POST: ToDo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var toDo = await _context.ToDo.FindAsync(id);
            _context.ToDo.Remove(toDo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoExists(int id)
        {
            return _context.ToDo.Any(e => e.Id == id);
        }
    }
}
