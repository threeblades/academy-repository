﻿using System.ComponentModel.DataAnnotations;

namespace EF_Kontakte.Models
{
    public class Contact
    {
        [Key]
        public int ContactID { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Nachname")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Vorname")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(25)]
        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        [Required]
        [MaxLength(255)]
        [Display(Name = "E-Mail")]
        public string EMail { get; set; }
    }
}