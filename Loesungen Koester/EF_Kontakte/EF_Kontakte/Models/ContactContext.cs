﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EF_Kontakte.Models
{
    public class ContactContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }

        public ContactContext(DbContextOptions<ContactContext> options)
        : base(options)
        {
            this.Database.EnsureCreated();

            if (this.Contacts.Count() == 0)
            {
                List<Contact> kontakte = new List<Contact>
                {
                    new Contact { FirstName="Chong", LastName="Li", Phone= "1234" , EMail= "chong@kumite.hg"},
                    new Contact { FirstName="Chuck", LastName="Norris", Phone= "5623", EMail= "chuck@home.de" },
                    new Contact { FirstName="James T.", LastName="Kirk", Phone= "1701", EMail= "james@enterprise.com" },
                    new Contact { FirstName="Frank", LastName="Dux", Phone= "3847", EMail= "frankD@kumite.hg" },
                    new Contact { FirstName="Bruce", LastName="Lee", Phone= "6857", EMail= "bruce@home.de" },
                    new Contact { FirstName="Han", LastName="Solo", Phone= "1423", EMail= "han@home.de" },
                    new Contact { FirstName="Luke", LastName="Skywalker", Phone= "7086", EMail= "luke@home.de" },
                    new Contact { FirstName="Leonard", LastName="McCoy", Phone= "4760", EMail= "bones@enterprise.com" },
                    new Contact { FirstName="Darth", LastName="Vader", Phone= "4071", EMail= "darth@home.de" },
                    new Contact { FirstName="Frank N.", LastName="Stein", Phone= "1092", EMail= "frankN@home.de" },
                    new Contact { FirstName="Hannibal", LastName="Lecter", Phone= "3478", EMail= "hannibal@home.de" },
                    new Contact { FirstName="John", LastName="McClane", Phone= "2643", EMail= "john@home.de" },
                    new Contact { FirstName="Leia", LastName="Organa", Phone= "2354", EMail= "leia@home.de" },
                    new Contact { FirstName="Nyota", LastName="Uhura", Phone= "1987", EMail= "nyota@enterprise.de" },
                    new Contact { FirstName="Ellen", LastName="Ripley", Phone= "2346", EMail= "ellen@nostromo.com" },
                    new Contact { FirstName="Rick", LastName="Deckard", Phone= "7324", EMail= "rick@tyrell.de" },
                    new Contact { FirstName="Roy", LastName="Batty", Phone= "9871", EMail= "roy@tyrell.de" },
                    new Contact { FirstName="Henry Dorsett", LastName="Case", Phone= "1984", EMail= "case@ta.com" },
                    new Contact { FirstName="Molly", LastName="Millions", Phone= "2983", EMail= "molly@ta.com" },
                    new Contact { FirstName="Bowman", LastName="David", Phone= "5231", EMail= "dave@hal.com" },
                    new Contact { FirstName="Neil", LastName="McCauley", Phone= "7234", EMail= "neil@home.de" },
                    new Contact { FirstName="Vincent", LastName="Hanna", Phone= "9376", EMail= "vincent@lapd.gov" }
                };

                this.Contacts.AddRange(kontakte);
                this.SaveChanges();
            }
        }
    }
}