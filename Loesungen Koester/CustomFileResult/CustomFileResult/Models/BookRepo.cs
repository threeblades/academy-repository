﻿using System.Collections.Generic;

namespace CustomFileResult.Models
{
    public class BookRepo
    {
        private readonly List<Book> books;

        public BookRepo()
        {
            books = new List<Book>()
            {
                new Book { Author = "Tim", Title="A", Pages = 100},
                new Book { Author = "Tom", Title="B", Pages = 101},
                new Book { Author = "Tina", Title="C", Pages = 102},
                new Book { Author = "Tanja", Title="D", Pages = 103},
                new Book { Author = "Toni", Title="E", Pages = 104}
            };
        }

        public List<Book> GetBooks()
        {
            return books;
        }
    }
}