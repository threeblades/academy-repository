﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFileResult.Results
{
    public class CsvFileResult<T> : FileResult
    {
        private readonly IList<T> list;
        private readonly string separatorString;

        public CsvFileResult(IList<T> sourceList, string fileName, string separator = ",")
            : base("text/csv")
        {
            list = sourceList;
            FileDownloadName = fileName;
            separatorString = separator;
        }

        // Überschriebene Methode von FileResult
        public override void ExecuteResult(ActionContext context)
        {
            context.HttpContext.Response.ContentType = ContentType;

            context.HttpContext.Response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });

            // CSV-Daten aus den Objekten als Antwortdaten zurückliefern
            WriteList(context.HttpContext.Response.Body);
        }

        private void WriteList(Stream stream)
        {
            using (StreamWriter streamWriter = new StreamWriter(stream, Encoding.Default))
            {
                // Kopfzeile schreiben
                WriteHeaderLine(streamWriter);

                // Leerzeile einfügen
                streamWriter.WriteLine();

                // Datenzeilen schreiben
                WriteDataLines(streamWriter);
            }
        }

        private void WriteHeaderLine(StreamWriter streamWriter)
        {
            // Die Namen aller Properties von T ermitteln
            var propertyNames = typeof(T).GetProperties().Select(m => m.Name);

            // Die Namen mit dem Separator-Zeichen zu einem String verbinden und in den Stream schreiben
            streamWriter.Write(string.Join(separatorString, propertyNames));
        }

        private void WriteDataLines(StreamWriter streamWriter)
        {
            // Liste der Objekte durchlaufen
            foreach (T oneListItem in list)
            {
                // Vom jedem Objekt alle Properties ermitteln und dessen Werte auslesen
                // Ausgelesene Werte mittels Separatorzeichen verbinden und in den Stream schreiben
                streamWriter.Write(
                    string.Join(separatorString,
                    typeof(T).
                    GetProperties().
                    Select(m => GetPropertyValue(oneListItem, m.Name))));

                // Zeilenumbruch
                streamWriter.WriteLine();
            }
        }

        /// <summary>
        /// Den Wert (Inhalt) eines Properties von einem Objekt ermitteln
        /// </summary>
        /// <param name="src">Das Objekt mit den Properties</param>
        /// <param name="propName">Name des Properties, dessen Wert ermittelt werden soll</param>
        /// <returns></returns>
        public static string GetPropertyValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src).ToString() ?? "";
        }
    }
}