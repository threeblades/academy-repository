﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CustomFileResult.Models;
using CustomFileResult.Results;

namespace CustomFileResult.Controllers
{
    public class HomeController : Controller
    {
        private BookRepo rep = new BookRepo();

        public IActionResult Index()
        {
            return View(rep.GetBooks());
        }

        public CsvFileResult<Book> DownloadCSV()
        {
            return new CsvFileResult<Book>(rep.GetBooks(), "Books.csv",";");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
