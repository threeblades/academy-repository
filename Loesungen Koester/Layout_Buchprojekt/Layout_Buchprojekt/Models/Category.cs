﻿namespace Layout_Buchprojekt.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}