﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Layout_Buchprojekt.Models
{
    public class Book
    {
        public string ISBN { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public string CategoryName { get; set; }
    }
}