﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Layout_Buchprojekt.Models
{
    public class BookRepository
    {
        private static string connectionString = null;

        public BookRepository()
        {
            connectionString = @"Data Source=.\SQLEXPRESS;" +
                           "Initial Catalog=BooksDB;" +
                           "Integrated Security=true;";
        }

        public List<Book> GetAllBooks()
        {
            List<Book> allBooks = new List<Book>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand sqlCommand = connection.CreateCommand();
                    sqlCommand.CommandText = "usp_show_all_books";
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)
                        });
                    }
                }
                catch (Exception e)
                {
                    // TODO Exception ins ViewBag schreiben
                }
            }

            return allBooks;
        }

        public List<Book> FindBooks(string searchString)
        {
            List<Book> allBooks = new List<Book>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();

                    sql.CommandType = CommandType.StoredProcedure;
                    sql.Parameters.Add(new SqlParameter("filter", SqlDbType.NVarChar, 100));
                    sql.Parameters[0].Value = searchString ?? "";
                    sql.CommandText = "usp_filter_books";

                    SqlDataReader reader = sql.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)
                        });
                    }
                }
                catch (Exception e)
                {
                    // TODO Exception ins ViewBag schreiben
                }
            }

            return allBooks;
        }

        public List<Category> GetCategories()
        {
            List<Category> allCategories = new List<Category>();

            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = "SELECT CategoryID, CategoryName FROM Categories;";

                    SqlDataReader reader = sql.ExecuteReader();

                    while (reader.Read())
                    {
                        allCategories.Add(new Category
                        {
                            CategoryID = reader.GetInt32(0),
                            CategoryName = reader.GetString(1),
                        });
                    }
                }
                catch (Exception e)
                {
                    // TODO Exception ins ViewBag schreiben
                }
            }

            return allCategories;
        }

        public List<Book> GetBooksByCategory(int CategoryID)
        {
            List<Book> allBooks = new List<Book>();

            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandType = CommandType.StoredProcedure;
                    sql.Parameters.Add(new SqlParameter("CategoryID", SqlDbType.Int));
                    sql.Parameters[0].Value = CategoryID;
                    sql.CommandText = "usp_books_by_categoryid";

                    SqlDataReader reader = sql.ExecuteReader();

                    while (reader.Read())
                    {
                        allBooks.Add(new Book
                        {
                            ISBN = reader.GetString(0),
                            Title = reader.GetString(1),
                            Year = reader.GetDateTime(2),
                            CategoryName = reader.GetString(3)
                        });
                    }
                }
                catch (Exception e)
                {
                    // TODO Exception ins ViewBag schreiben
                }
            }

            return allBooks;
        }
    }
}