﻿using Layout_Buchprojekt.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Diagnostics;

namespace Layout_Buchprojekt.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookRepository repository = new BookRepository();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ShowAllBooks()
        {
            return View(repository.GetAllBooks());
        }

        public IActionResult FindBooks()
        {
            return View();
        }

        // Partielle View für die Suchergebnisse
        public PartialViewResult ShowSearchResults(string searchString)
        {
            return PartialView("_ListBooks", repository.FindBooks(searchString));
        }

        public IActionResult BooksByCategory()
        {
            // Alle Kategorien auslesen...
            var categories = repository.GetCategories();

            // ...und in ViewData schreiben
            // Das Property CategoryID soll als Value für die <options> verwendet werden
            // Das Property CategoryName soll als Anzeigewert für die <options> verwendet werden
            ViewData["Categories"] = new SelectList(categories, "CategoryID", "CategoryName");

            return View();
        }

        // Partielle View für die Ausgabe aller Bücher einer Kategorie
        public PartialViewResult ShowCategoryResult(int Categories)
        {
            return PartialView("_ListBooks", repository.GetBooksByCategory(Categories));
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}