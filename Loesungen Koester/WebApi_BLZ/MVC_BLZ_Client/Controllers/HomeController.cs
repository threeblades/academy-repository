﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC_BLZ_Client.Models;

namespace MVC_BLZ_Client.Controllers
{
    public class HomeController : Controller
    {
        private readonly BlzRepository blzRepository = new BlzRepository();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Alle()
        {
            return View(blzRepository.GetAllBankInfos().Take(100));
        }

        public IActionResult BlzSuche()
        {
            return View();
        }

        [HttpPost]
        public IActionResult BlzSuche(string blz)
        {
            BankInfo info = blzRepository.GetBankInfo(blz);

            return View(info);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
