﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MVC_BLZ_Client.Models
{
    public class BlzRepository
    {
        private const string url = "http://localhost:50804";

        private readonly HttpClient client;

        public BlzRepository()
        {
            client = new HttpClient();

            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Clear();

            // Datenformat der Antwort festlegen (Ist optional)
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public List<BankInfo> GetAllBankInfos()
        {
            // GET-Aufruf für alle BankInfos starten
            HttpResponseMessage responseMessage = client.GetAsync("api/blz").Result;

            return DeserializeRequest<List<BankInfo>>(responseMessage);
        }

        public BankInfo GetBankInfo(string blz)
        {
            // GET-Aufruf für eine BLZ starten
            HttpResponseMessage responseMessage = client.GetAsync("api/blz/" + blz).Result;

            return DeserializeRequest<BankInfo>(responseMessage);
        }

        private static T DeserializeRequest<T>(HttpResponseMessage responseMessage) where T : class
        {
            // Aufruf erfolgreich?
            if (responseMessage.IsSuccessStatusCode)
            {
                // Antwort der Web-API in einem String speichern
                string result = responseMessage.Content.ReadAsStringAsync().Result;

                // JSON Parsen 
                return JsonConvert.DeserializeObject<T>(result);
            }
            return null;
        }
    }
}
