﻿namespace MVC_BLZ_Client.Models
{
    public class BankInfo
    {
        public string BLZ { get; set; }
        public string BIC { get; set; }
        public string Bezeichnung { get; set; }
        public string PLZ { get; set; }
        public string Ort { get; set; }
    }
}
