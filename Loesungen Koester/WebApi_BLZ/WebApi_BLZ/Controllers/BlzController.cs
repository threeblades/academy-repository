﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi_BLZ.Models;

namespace WebApi_BLZ.Controllers
{
    // Scaffold-DbContext "Server=.\SQLEXPRESS;Database=TeachSQL;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -Tables BLZTabelle -outputdir Models

    [Route("api/blz")]
    public class BlzController : Controller
    {
        private readonly TeachSQLContext context;

        public BlzController(TeachSQLContext context)
        {
            this.context = context;
        }

        // Get: http://localhost:50804/api/blz
        public IEnumerable<BankInfo> Get()
        {
            IEnumerable<BankInfo> infos = context.Blztabelle.Select(blz => new BankInfo()
            {
                Bezeichnung = blz.Bezeichnung,
                BLZ = blz.Blz,
                BIC = blz.Bic,
                Ort = blz.Ort,
                PLZ = blz.Plz.Value.ToString() ?? string.Empty
            });

            return infos;
        }

        // Get: http://localhost:50804/api/blz/44050199
        [HttpGet("{blz}")]
        public IActionResult Get(string blz)
        {
            var info = context.Blztabelle.Where(b => b.Blz == blz).Select(b => new BankInfo()
            {
                Bezeichnung = b.Bezeichnung,
                BLZ = b.Blz,
                BIC = b.Bic,
                Ort = b.Ort,
                PLZ = b.Plz.Value.ToString() ?? string.Empty
            });

            if (info.Count() == 0)
            {
                return NotFound("Kein Eintrag gefunden");
            }

            // Nur das erste Element zurückliefern
            return Ok(info.First());
        }
    }
}