﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApi_BLZ.Models
{
    public partial class TeachSQLContext : DbContext
    {
        public virtual DbSet<Blztabelle> Blztabelle { get; set; }

        public TeachSQLContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blztabelle>(entity =>
            {
                entity.ToTable("BLZTabelle");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Aenderungskennzeichen)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Bezeichnung)
                    .HasMaxLength(58)
                    .IsUnicode(false);

                entity.Property(e => e.Bic)
                    .HasColumnName("BIC")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Blz)
                    .HasColumnName("BLZ")
                    .HasMaxLength(50);

                entity.Property(e => e.Kurzbezeichnung)
                    .HasMaxLength(27)
                    .IsUnicode(false);

                entity.Property(e => e.NachfolgeBlz)
                    .HasColumnName("NachfolgeBLZ")
                    .HasMaxLength(50);

                entity.Property(e => e.Ort)
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.Pan).HasColumnName("PAN");

                entity.Property(e => e.Plz).HasColumnName("PLZ");

                entity.Property(e => e.Pruefzifferberechnungsmethode)
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });
        }
    }
}
