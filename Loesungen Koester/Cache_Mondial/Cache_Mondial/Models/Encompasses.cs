﻿using System;
using System.Collections.Generic;

namespace Cache_Mondial.Models
{
    public partial class Encompasses
    {
        public string Country { get; set; }
        public string Continent { get; set; }
        public double? Percentage { get; set; }

        public Continent ContinentNavigation { get; set; }
        public Country CountryNavigation { get; set; }
    }
}
