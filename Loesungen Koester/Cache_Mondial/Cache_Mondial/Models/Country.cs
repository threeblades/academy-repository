﻿using System;
using System.Collections.Generic;

namespace Cache_Mondial.Models
{
    public partial class Country
    {
        public Country()
        {
            Encompasses = new HashSet<Encompasses>();
        }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Capital { get; set; }
        public string Province { get; set; }
        public int? Area { get; set; }
        public int? Population { get; set; }

        public ICollection<Encompasses> Encompasses { get; set; }
    }
}
