﻿using System;
using System.Collections.Generic;

namespace Cache_Mondial.Models
{
    public partial class Continent
    {
        public Continent()
        {
            Encompasses = new HashSet<Encompasses>();
        }

        public string Name { get; set; }
        public int? Area { get; set; }

        public ICollection<Encompasses> Encompasses { get; set; }
    }
}
