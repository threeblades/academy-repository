﻿using Cache_Mondial.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq;

namespace Cache_Mondial.Controllers
{
    public class HomeController : Controller
    {
        private MondialContext context;

        public HomeController(MondialContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            // Alle Kontinente auslesen und in ViewData schreiben
            // Das Property Name soll als Value für die <options> verwendet werden
            // Das Property Name soll als Anzeigewert für die <options> verwendet werden
            ViewData["continent"] = new SelectList(context.Continent, "Name", "Name");

            return View();
        }

        public PartialViewResult ShowContinentsResult(string continent)
        {
            var encompasses = context.Continent.Include(c => c.Encompasses).SingleOrDefault(c => c.Name == continent).Encompasses;

            var countryCodes = encompasses.Select(e => e.Country);

            var countries = context.Country.Where(c => countryCodes.Contains(c.Code));

            // Name des angezeigten Kontinents für den Cache speichern
            ViewBag.Continent = continent;

            return PartialView("_ShowCountries", countries);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}