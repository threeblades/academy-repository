﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF_Kursverwaltung_VHS.Models
{
    public class Kurs
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime Starttermin { get; set; }

        public int Dauer { get; set; }

        public static Kurs ParseCSV(string csvZeile)
        {
            try
            {
                string[] felder = csvZeile.Split(';');

                Kurs neuerKurs = new Kurs
                {
                    Name = felder[1],
                    Starttermin = DateTime.Parse(felder[2]),
                    Dauer = int.Parse(felder[3])
                };

                return neuerKurs;
            }
            catch
            {
                return null;
            }
        }

        public static bool TryParse(string zeileMitCSVDaten, out Kurs neuerKurs)
        {
            neuerKurs = ParseCSV(zeileMitCSVDaten);
            return neuerKurs != null;
        }
    }
}