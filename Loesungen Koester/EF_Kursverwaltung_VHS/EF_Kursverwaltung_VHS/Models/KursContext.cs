﻿using Microsoft.EntityFrameworkCore;

namespace EF_Kursverwaltung_VHS.Models
{
    public class KursContext : DbContext
    {
        public DbSet<Kurs> Kurse { get; set; }

        public KursContext(DbContextOptions<KursContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}