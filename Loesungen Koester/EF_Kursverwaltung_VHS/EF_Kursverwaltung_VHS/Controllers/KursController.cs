﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EF_Kursverwaltung_VHS.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;

namespace EF_Kursverwaltung_VHS.Controllers
{
    public class KursController : Controller
    {
        private readonly KursContext _context;

        public KursController(KursContext context)
        {
            _context = context;
        }

        // GET: Kurs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Kurse.ToListAsync());
        }

        // GET: Kurs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kurs = await _context.Kurse
                .SingleOrDefaultAsync(m => m.Id == id);
            if (kurs == null)
            {
                return NotFound();
            }

            return View(kurs);
        }

        // GET: Kurs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Kurs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Starttermin,Dauer")] Kurs kurs)
        {
            if (ModelState.IsValid)
            {
                _context.Add(kurs);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(kurs);
        }

        // GET: Kurs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kurs = await _context.Kurse.SingleOrDefaultAsync(m => m.Id == id);
            if (kurs == null)
            {
                return NotFound();
            }
            return View(kurs);
        }

        // POST: Kurs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Starttermin,Dauer")] Kurs kurs)
        {
            if (id != kurs.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(kurs);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KursExists(kurs.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(kurs);
        }

        // GET: Kurs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kurs = await _context.Kurse
                .SingleOrDefaultAsync(m => m.Id == id);
            if (kurs == null)
            {
                return NotFound();
            }

            return View(kurs);
        }

        // POST: Kurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var kurs = await _context.Kurse.SingleOrDefaultAsync(m => m.Id == id);
            _context.Kurse.Remove(kurs);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KursExists(int id)
        {
            return _context.Kurse.Any(e => e.Id == id);
        }

        /* ------------------------
         * Hinzugefügte Methoden
         * ---------------------------
         */
        [HttpPost]
        public IActionResult Upload(IFormFile csvupluad)
        {
            StreamReader streamReader = new StreamReader(csvupluad.OpenReadStream());
            string zeile = null;
            Kurs neuerKurs = null;

            while ((zeile = streamReader.ReadLine()) != null)
            {
                // Versuchen aus der Zeile eine Kurs-Objekt zu erzeugen
                if (Kurs.TryParse(zeile, out neuerKurs))
                {
                    // Neues Kursobjekt einfügen
                    _context.Kurse.Add(neuerKurs);
                }
            }

            // Veränderungen in die Datenbank schreiben
            _context.SaveChanges();

            // Zur Index umleiten, um die eingefügten Daten anzuzeigen            
            return RedirectToAction("Index");
        }

        public FileResult Download()
        {
            string csv = string.Empty;

            foreach (Kurs einKurs in _context.Kurse)
            {
                csv += string.Format("{0};{1};{2};{3}", einKurs.Id, einKurs.Name, einKurs.Starttermin.ToShortDateString(), einKurs.Dauer);
                csv += Environment.NewLine;
            }

            return File(Encoding.UTF32.GetBytes(csv), "Text/Csv", "Kurse.csv");
        }
    }
}
