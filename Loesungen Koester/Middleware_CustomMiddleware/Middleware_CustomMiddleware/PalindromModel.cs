﻿using System.Linq;

namespace Middleware_CustomMiddleware
{
    public class PalindromModel
    {
        public string Text { get; set; }

        public bool IstPalindrom
        {
            get
            {
                if (Text.Length < 2)
                {
                    return false;
                }
                return Text.ToLower() == new string(Text.ToLower().Reverse().ToArray());
            }
        }
    }
}