﻿using System.IO;
using System.Reflection;

namespace Middleware_CustomMiddleware
{
    public class MyController
    {
        private readonly string path = Path.Combine(Directory.GetCurrentDirectory(), "HTML");

        private const string extension = "html";

        public string Index()
        {
            // Namen der ausgeführten Methode ermitteln
            string name = MethodBase.GetCurrentMethod().Name;

            return DateiInhaltEinlesen(name);
        }

        private string DateiInhaltEinlesen(string dateiname)
        {
            return File.ReadAllText(Path.ChangeExtension(Path.Combine(path, dateiname), extension));
        }

        public string Echo(string postData)
        {
            string name = MethodBase.GetCurrentMethod().Name;

            string content = DateiInhaltEinlesen(name);

            return string.Format(content, "Aufruf von Index per POST: " + postData);
        }

        public string Palindrom(string wort)
        {
            string name = MethodBase.GetCurrentMethod().Name;

            string content = DateiInhaltEinlesen(name);

            PalindromModel palindrom = new PalindromModel() { Text = wort };

            return string.Format(content, "<h3>"+ wort + " ist " + (palindrom.IstPalindrom ? "ein" : "kein") + " Palindrom.</h3>");
        }
    }
}