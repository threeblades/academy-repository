﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Middleware_CustomMiddleware
{
    public class MyMiddleware
    {
        private readonly RequestDelegate next;

        public MyMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            MyController controller = new MyController();

            string url = httpContext.Request.Path;

            string[] formData = null;

            // Bei einem Post...
            if (httpContext.Request.Method == "POST")
            {
                // ...alle Daten des Posts auswerten
                formData = GetFormData(httpContext.Request.Form);
            }

            string html = ControllerMethodeSuchenUndAufrufen(url, controller, formData);

            // Generiertes HTML in die Antwort für den Client schreiben
            httpContext.Response.WriteAsync(html);

            return Task.CompletedTask;
        }

        private string[] GetFormData(IFormCollection pairs)
        {
            string[] formData = new string[pairs.Count];

            foreach (string key in pairs.Keys)
            {
                formData = pairs[key];
            }

            return formData;
        }

        private string ControllerMethodeSuchenUndAufrufen(string url, MyController controller, string[] formData)
        {
            string html = string.Empty;

            Type type = typeof(MyController);

            // Alle öffentlichen Methoden der Klasse, welche nicht geerbt wurden, ermitteln
            MethodInfo[] controllerMethods = type.GetMethods(BindingFlags.DeclaredOnly
                                                             | BindingFlags.Public
                                                             | BindingFlags.Instance);

            // Sonderfall /
            // / durch index ersetzten, damit das Ausführen
            // der Methode mittels Reflection funktioniert
            if (url == "/")
            {
                url = "/index";
            }

            url = url.Replace("/", string.Empty);

            // Methode des Controllers finden, welche dem Namen des URL entspricht
            MethodInfo methodToCall = controllerMethods.SingleOrDefault(m => m.Name.ToLower() == url);

            if (methodToCall != null)
            {
                // Methode ausführen (z.B. Index())
                try
                {
                    html = methodToCall.Invoke(controller, formData) as string;
                }
                catch (Exception e)
                {
                    html = e.Message;
                }
            }
            else
            {
                html = "<h3>Seite nicht gefunden</h3>";
            }

            return html;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class MyMiddlewareExtensions
    {
        public static IApplicationBuilder UseMyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<MyMiddleware>();
        }
    }
}