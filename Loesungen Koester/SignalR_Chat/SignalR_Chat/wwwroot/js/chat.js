﻿var chat;

var username;

$(document).ready(init);

function init()
{
    // Benutzernamen einlesen
    $('#displayname').val(prompt('Bitte Ihren Namen eingeben:', ''));

    // ...und speichern
    username = $('#displayname').val();

    // Benutzername leer?
    if (username === undefined || username === '')
    {
        username = "ChatUser" + Math.floor(Math.random() * (100 - 1) + 1);
    }

    // Benutzername in die Seite einfügen
    $('#username').text(username);

    // Auswahl aufheben
    $('#deselect').on('click', function () {
        $("#teilnehmer").val([]);
    });

    chatStart();
}

function chatStart()
{
    // Referenz auf den Hub
    chat = new signalR.HubConnectionBuilder()
        .withUrl("/chatHub")
        .build();

    // Methode für Fehlermeldung vom Server
    chat.on("sendServerError", (errorMessage) => {
        alert(errorMessage);
        init();
    });

    // Start der Verbindung
    chat.start().then(() =>
    {
        // Benutzernamen an den Server senden
        chat.invoke("SignOn", username);

        // Bei Buttonclick
        $('#sendmessage').on("click",sendButtonClick);
    });

    // Aufruf für den Erhalt einer Nachricht
    chat.on("receiveMsg", (message) => 
    {
        // Nachricht einfügen
        $('#discussion').prepend(message + '\n');
    });

    // Aufruf für die Verbindung eines Benutzers
    chat.on("handleSignOn", (chatNames) =>
    {
        // Teilnehmerliste löschen
        $("#teilnehmer").empty();
        $("#teilnehmer").append('<option disabled="disabled">Benutzer auswählen</option>');

        // Namen am Komma zerlegen
        var names = chatNames.split(',');

        // Namen durchlaufen
        for (var i = 0; i < names.length; i++)
        {
            // Alle Namen bis auf den eigenen...
            if (names[i] !== username)
            {
                // ...in die Liste einfügen
                $("#teilnehmer").append('<option value="' + names[i] + '">' + names[i] + '</option>');
            }
        }
    });

    // Aufruf für den Verbindungsabbau eines Benutzers
    chat.on("handleSignOff", (chatName) =>
    {
        // Teilnehmer aus dem select entfernen
        $("#teilnehmer option[value='" + chatName + "']").remove();
    });
}

function sendButtonClick()
{
    // Nachricht auslesen
    var message = username + ": " + $('#message').val();

    // Ausgewählten Benutzer ermitteln
    var selectedUser = $('#teilnehmer option:selected').val();

    // An alle oder einen Benutzer?
    if (selectedUser)
    {
        // An einen Benutzer

        // Aufruf der Methode SendMsg aus der ChatHub.cs
        chat.invoke("SendMsg", selectedUser, message);
    }
    else
    {
        // An alle Benutzer

        // Aufruf der Methode BroadcastMsg aus der ChatHub.cs
        chat.invoke("BroadcastMsg", message);
    }

    // Textbox löschen und wieder den Fokus auf die Textbox setzten
    $('#message').val('').focus();
}