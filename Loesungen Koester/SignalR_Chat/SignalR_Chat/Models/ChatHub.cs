﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_Chat.Models
{
    public class ChatHub : Hub
    {
        // Muss static sein, da der Hub bei der Kommunikation jedes mal neu instanziiert wird
        private static readonly Dictionary<string, string> chatClients = new Dictionary<string, string>();

        public void SignOn(string chatName)
        {
            // Ausnahme bei doppelten Usernamen
            try
            {
                // Benutzernamen inklusive ID merken
                chatClients.Add(chatName, Context.ConnectionId);

                // Alle Benutzernamen als ein String zusammenfügen
                string users = string.Join(",", chatClients.Keys);

                // Alle Namen an die Clients versenden
                Clients.All.SendAsync("handleSignOn", users);
            }
            catch (Exception e)
            {
                Clients.Caller.SendAsync("sendServerError", e.Message);
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            // Benutzer zu der ID ermitteln, welche die Verbidung geschlossen hat
            var user = chatClients.SingleOrDefault(kvp => kvp.Value == Context.ConnectionId);

            // Benutzer aus der Liste der Clients entfernen
            chatClients.Remove(user.Key);

            // Info an die verbleibenden Clients senden
            await Clients.Others.SendAsync("handleSignOff", user.Key);

            await base.OnDisconnectedAsync(exception);
        }

        public void BroadcastMsg(string msg)
        {
            // Aufruf der JavaScript-Funktion receiveMsg auf dem Client
            Clients.All.SendAsync("receiveMsg", msg);
        }

        public void SendMsg(string user, string msg)
        {
            // TODO Ausnahme prüfen falls Schlüssel nicht existiert
            // ID des Benutzers ermitteln
            string id = chatClients[user];

            // Nachricht nur an den Client mit dieser Id versenden
            Clients.Client(id).SendAsync("receiveMsg", msg);
        }
    }
}
