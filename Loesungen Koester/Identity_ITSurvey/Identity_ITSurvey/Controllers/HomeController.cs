﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Identity_ITSurvey.Data;
using Identity_ITSurvey.Models;
using Microsoft.AspNetCore.Authorization;

namespace Identity_ITSurvey.Controllers
{
    public class HomeController : Controller
    {
        private readonly SurveyContext _context;

        public HomeController(SurveyContext context)
        {
            _context = context;
        }

        // GET: Home
        public async Task<IActionResult> Index()
        {
            return View(await _context.Questions.ToListAsync());
        }

        [Authorize(Roles = "Users")]
        public IActionResult Vote(int id)
        {
            SurveyQuestion question = _context.Questions.Find(id);

            return View(question);
        }

        [HttpPost]
        [Authorize(Roles = "Users")]
        public IActionResult Vote(int? answerId, int? id)
        {
            if (answerId.HasValue && id.HasValue)
            {
                AnswerOption theAnswer = _context.AnswerOptions.Find(answerId);

                // Liste aller Anwendernamen, die auf irgendeine Antwortoption geantwortet haben
                // Nur wenn der aktuelle Anwender noch nicht in der Liste ist,
                // darf er/sie abstimmen
                bool benutzerHatschonAbgestimmt = theAnswer.Question.AnswerOptions.SelectMany(ao => ao.UserAnswers)
                                                                                  .Select(t => t.UserName)
                                                                                  .Contains(this.User.Identity.Name);

                if (benutzerHatschonAbgestimmt == false)
                {
                    theAnswer.UserAnswers.Add(new UserAnswer { UserName = this.User.Identity.Name });

                    _context.SaveChanges();

                    return RedirectToAction("ShowResult", new { id });
                }
                else
                {
                    ViewBag.Error = "Sie haben bereits für diese Frage abgestimmt!";

                    return View(_context.Questions.Find(id));
                }

            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public IActionResult ShowResult(int id)
        {
            SurveyQuestion question = _context.Questions.Find(id);

            // Wie oft wurde für die Frage eine Antwort abgegeben
            ViewBag.SumOfAllVotes = (int)question.AnswerOptions.Sum(q => q.Votes);

            return View(question);
        }
    }
}
