﻿using Identity_ITSurvey.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_ITSurvey.Data
{
    public class SurveyContext : DbContext
    {
        public DbSet<SurveyQuestion> Questions { get; set; }
        public DbSet<AnswerOption> AnswerOptions { get; set; }
        public DbSet<UserAnswer> UserAnswers { get; set; }

        public SurveyContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();

            if (Questions.Count() == 0)
            {
                var question = new SurveyQuestion()
                {
                    QuestionText = "Welche Programmiersprachen beherrschen Sie?"
                };

                question.AnswerOptions.Add(new AnswerOption() { OptionText = "C#" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "C++" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "Objective-C" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "C" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "Java" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "JavaScript" });

                Questions.Add(question);

                question = new SurveyQuestion()
                {
                    QuestionText = "Welches Datenbanksystem verwenden Sie regelmäßig?"
                };
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "Oracle" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "SQL Server" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "PostgreSQL" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "DB2" });
                question.AnswerOptions.Add(new AnswerOption() { OptionText = "MySQL" });

                Questions.Add(question);

                SaveChanges();
            }
        }
    }
}
