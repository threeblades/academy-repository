﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Identity_ITSurvey.Data
{
    public static class UserAndRolesSeed
    {
        // Den Aufruf dieser Methode in der Startup.cs in der Configure Methode hinzufügen
        public static void Initialize(IServiceProvider serviceProvider)
        {
            RoleManager<IdentityRole> roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();

            if (roleManager.RoleExistsAsync("Users").Result == false)
            {
                roleManager.CreateAsync(new IdentityRole("Users")).Wait();
            }

            if (roleManager.RoleExistsAsync("Admins").Result == false)
            {
                roleManager.CreateAsync(new IdentityRole("Admins")).Wait();
            }

            // Einen Benutzer suchen
            IdentityUser account = userManager.FindByNameAsync("admin@home.de").Result;

            // Falls dieser Benutzer nicht existiert, diesen anlegen
            if (account == null)
            {
                account = new IdentityUser
                {
                    UserName = "admin@home.de",
                    Email = "admin@home.de"
                };

                IdentityResult identityResult = userManager.CreateAsync(account, "Test123!").Result;

                // Den angelegten Benutzer der angegebenen Rolle hinzufügen
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(account, "Admins").Wait();
                }
            }
        }
    }
}