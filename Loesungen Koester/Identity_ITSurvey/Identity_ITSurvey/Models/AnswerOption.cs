﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Identity_ITSurvey.Models
{
    public class AnswerOption
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(200)]
        public string OptionText { get; set; }

        [ForeignKey("Question")]
        public int QuestionID { get; set; }

        public virtual SurveyQuestion Question { get; set; }

        public virtual ICollection<UserAnswer> UserAnswers { get; set; }

        // Wie oft wurde von einzelnen Benutzern für die Frage abgestimmt
        public int Votes
        {
            get
            {
                return UserAnswers.Count;
            }
        }

        public AnswerOption()
        {
            this.UserAnswers = new List<UserAnswer>();
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", OptionText, Votes);
        }
    }
}
