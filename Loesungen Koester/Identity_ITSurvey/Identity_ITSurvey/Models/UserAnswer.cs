﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity_ITSurvey.Models
{
    public class UserAnswer
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string UserName { get; set; }

        [ForeignKey("Answer")]
        public int AnswerID { get; set; }

        public virtual AnswerOption Answer { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Answer.OptionText, UserName);
        }
    }
}