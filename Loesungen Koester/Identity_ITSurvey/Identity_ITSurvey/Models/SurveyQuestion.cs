﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Identity_ITSurvey.Models
{
    public class SurveyQuestion
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(200)]
        [UIHint("MultilineText")]
        [Display(Name = "Fragetext")]
        public string QuestionText { get; set; }

        public virtual ICollection<AnswerOption> AnswerOptions { get; set; }

        public SurveyQuestion()
        {
            this.AnswerOptions = new List<AnswerOption>();
        }

        public override string ToString()
        {
            return QuestionText;
        }
    }
}