﻿using System;

namespace Helper_EigenenHelperErstellen.Models
{
    public class Film
    {
        public string Titel { get; set; }
        public int Id { get; set; }
        public string Regie { get; set; }
        public int Jahr { get; set; }

        public Film(string daten)
        {
            string[] teile = daten.Split(';');
            Id = Convert.ToInt32(teile[0]);
            Titel = teile[1];
            Jahr = Convert.ToInt32(teile[2]);
            Regie = teile[3];
        }
    }
}