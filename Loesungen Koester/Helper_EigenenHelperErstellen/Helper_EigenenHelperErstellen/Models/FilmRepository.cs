﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Helper_EigenenHelperErstellen.Models
{
    public class FilmRepository
    {
        private readonly string pfad = @"d:\daten\filme.txt";

        public List<Film> AlleFilmeLaden(out string nachricht)
        {
            List<Film> dieFilme = new List<Film>();

            try
            {
                using (StreamReader streamReader = new StreamReader(pfad))
                {
                    while (streamReader.EndOfStream == false)
                    {
                        string zeile = streamReader.ReadLine();
                        dieFilme.Add(new Film(zeile));
                    }
                }

                // Daten wurden erfolgreich gelesen
                // Keine Meldung
                nachricht = null;
            }
            catch (Exception e)
            {
                // Fehler beim Lesen
                // Exception-Text als Meldung
                nachricht = e.Message;
            }

            return dieFilme;
        }
    }
}