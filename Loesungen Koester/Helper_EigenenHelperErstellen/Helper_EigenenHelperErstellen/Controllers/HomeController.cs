﻿using Helper_EigenenHelperErstellen.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;

namespace Helper_EigenenHelperErstellen.Controllers
{
    public class HomeController : Controller
    {
        private FilmRepository verwaltung = new FilmRepository();

        public IActionResult Index()
        {
            string nachricht;

            List<Film> alleFilme = verwaltung.AlleFilmeLaden(out nachricht);

            // Ggf. Fehlernachricht in das ViewBag schreiben
            ViewBag.Nachricht = nachricht;

            return View(alleFilme);
        }

        [HttpPost]
        public IActionResult Index(string[] movies)
        {
            if (movies != null)
            {
                ViewBag.SelectedIDs = movies;
            }

            string nachricht;

            List<Film> alleFilme = verwaltung.AlleFilmeLaden(out nachricht);

            // Ggf. Fehlernachricht in das ViewBag schreiben
            ViewBag.Nachricht = nachricht;

            return View(alleFilme);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}