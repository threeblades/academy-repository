﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Text;

namespace Helper_EigenenHelperErstellen.Helper
{
    public static class HtmlErweiterungen
    {
        public static IHtmlContent CheckBoxList(this IHtmlHelper helper, string name, SelectList liste)
        {
            // Umschließendes div, verwendet den übergebenen Namen als ID
            StringBuilder stringBuilder = new StringBuilder(string.Format("<div id='cbl_{0}'>\n", name));

            // Für jedes Element in der SelectList
            // Erzeugt z.B.
            // <input type='checkbox' name='movies' id='cb_movies_1' value='1' /> Shawshank Redemption, The<br />
            foreach (SelectListItem item in liste)
            {
                // Ein input mit dem übergebenen Namen und dem aktuellen Wert aus der Liste anlegen
                // item.Value ist in diesem Fall das Property "Id" der Klasse Film, 
                // da beim Konstruktor "Id" als Parameter für das 'dataValueField' verwendet wurde
                stringBuilder.AppendFormat("<input type='checkbox' name='{0}' id='cb_{0}_{1}' value='{1}' ", name, item.Value);

                // Abschluss mit dem Inhalt des Elementes als Text der Checkbox sowie einem Zeilenumbruch anhängen
                // item.Text ist in diesem Fall das Property "Titel" der Klasse Film, 
                // da beim Konstruktor "Titel" als Parameter für das 'dataTextField' verwendet wurde
                stringBuilder.AppendFormat(" /> {0}<br />", item.Text).AppendLine();
            }

            stringBuilder.AppendLine("</div>");

            return new HtmlString(stringBuilder.ToString());
        }

        public static IHtmlContent CheckBoxList(this IHtmlHelper helper, string name, SelectList liste, string[] selected)
        {
            // Umschließendes div, verwendet den übergebenen Namen als ID
            StringBuilder sb = new StringBuilder(string.Format("<div id='cbl_{0}'>\n", name));

            // Für jedes Element in der SelectList
            // Erzeugt z.B.
            // <input type='checkbox' name='movies' id='cb_movies_1' value='1' /> Shawshank Redemption, The<br />
            foreach (SelectListItem item in liste)
            {
                // Ein input mit dem übergebenen Namen und dem aktuellen Wert aus der Liste anlegen
                sb.AppendFormat("<input type='checkbox' name='{0}' id='cb_{0}_{1}' value='{1}' ", name, item.Value);

                // Wurde eine Auswahlliste übergeben und ist das aktuelle Element in der Auswahl enthalten?
                if (selected != null && selected.Contains(item.Value))
                {
                    // Die Checkbox auf checked setzen
                    sb.Append(" checked='checked' ");
                }

                // Abschluss mit dem Inhalt des Elementes als Text der Checkbox sowie einem Zeilenumbruch anhängen
                sb.AppendFormat(" /> {0}<br />", item.Text).AppendLine();
            }

            sb.AppendLine("</div>");

            return new HtmlString(sb.ToString());
        }
    }
}