﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Diagnostics;
using System.IO;

namespace Filter_Eigene_Filter.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly TraceSource traceSource = new TraceSource("MyLog", SourceLevels.All);

        public override void OnException(ExceptionContext context)
        {
            traceSource.Listeners.Clear();

            FileStream fileStream = new FileStream(@"d:\log\trace.txt", FileMode.OpenOrCreate);
            traceSource.Listeners.Add(new TextWriterTraceListener(fileStream));

            traceSource.TraceInformation("IExceptionFilter: " + DateTime.Now.ToLongTimeString() + " " + context.Exception.Message);
            traceSource.Flush();
            traceSource.Close();

            // Welche View soll im Fehlerfall angezeigt werden?
            context.Result = new ViewResult { ViewName = "CustomError" };

            Debug.WriteLine("IExceptionFilter: " + DateTime.Now.ToLongTimeString() + " " + context.Exception.Message);
        }
    }
}