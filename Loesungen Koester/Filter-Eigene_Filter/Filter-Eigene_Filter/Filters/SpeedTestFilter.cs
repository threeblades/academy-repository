﻿using Filter_Eigene_Filter.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Diagnostics;

namespace Filter_Eigene_Filter.Filters
{
    public class SpeedTestFilter : IActionFilter
    {
        private Stopwatch stopwatch = new Stopwatch();

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Debug.Indent();
            Debug.Indent();

            // Ausgabe im Log von VS
            Debug.WriteLine("IActionFilter: " + DateTime.Now.ToLongTimeString());

            HomeController homeController = filterContext.Controller as HomeController;
            homeController.ViewBag.StartZeit = "Startzeit: " + DateTime.Now.ToLongTimeString();

            // Stoppuhr starten
            stopwatch.Start();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // Stoppuhr beenden
            stopwatch.Stop();

            // Gemessene Zeit in das ViewBag schreiben
            HomeController homeController = filterContext.Controller as HomeController;
            homeController.ViewBag.Zeit = "Dauer: " + stopwatch.ElapsedMilliseconds + " ms";

            // Stoppuhr zurücksetzen
            stopwatch.Reset();

            // Stopp-Zeit in das ViewBag schreiben
            homeController.ViewBag.EndZeit = "Endzeit: " + DateTime.Now.ToLongTimeString();
        }
    }
}