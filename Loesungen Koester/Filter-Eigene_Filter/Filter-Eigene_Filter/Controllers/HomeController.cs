﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Filter_Eigene_Filter.Models;
using Filter_Eigene_Filter.Filters;
using System.Threading;

namespace Filter_Eigene_Filter.Controllers
{
    //[SpeedTestFilter]
    public class HomeController : Controller
    {
        //[TypeFilter(typeof(SpeedTestFilter))]
        public IActionResult Index()
        {
            Thread.Sleep(100);

            return View();
        }

        //[SpeedTestFilter]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        // GET: Home/MitFehler
        //[SpeedTestFilter]
        [CustomExceptionFilter]
        public IActionResult MitFehler()
        {
            int x = 10;
            int y = 0;
            int ergebnis = x / y;

            //throw new ArgumentException("Und Ende");

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
