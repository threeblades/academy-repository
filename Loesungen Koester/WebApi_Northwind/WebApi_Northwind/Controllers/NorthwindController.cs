﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi_Northwind.Models;

namespace WebApi_Northwind.Controllers
{
    [Route("api/Northwind")]
    public class NorthwindController : Controller
    {
        private readonly NorthwindContext context;

        public NorthwindController(NorthwindContext context)
        {
            this.context = context;
        }

        // GET api/Northwind/GetCustomerByID?customerID=ALFKI
        [HttpGet("GetCustomerByID")]
        public IActionResult GetCustomerByID([FromQuery]string customerID)
        {
            if (customerID == null)
            {
                return NotFound();
            }

            //Customers customer = context.Customers.SingleOrDefault(c => c.CustomerId == customerID);
            Customers customer = context.Customers.Find(customerID);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // GET api/Northwind/GetCustomersByCountry?country=Germany
        [HttpGet("GetCustomersByCountry")]
        public IActionResult GetCustomersByCountry([FromQuery]string country)
        {
            if (country == null)
            {
                return NotFound();
            }

            var customers = context.Customers.Where(customer => string.Equals(customer.Country, country, System.StringComparison.CurrentCultureIgnoreCase));

            if (customers == null)
            {
                return NotFound();
            }

            return Ok(customers);
        }

        // GET api/Northwind/GetOrdersByCustomerID/ALFKI
        [HttpGet("GetOrdersByCustomerID/{customerID}")]
        public IActionResult GetOrdersByCustomerID(string customerID)
        {
            var orders = context.Orders.Where(order => order.CustomerId == customerID);

            if (orders == null)
            {
                return NotFound();
            }

            return Ok(orders);
        }

        // GET api/Northwind/GetOrderValueByID/10248
        [HttpGet("GetOrderValueByID/{orderID}")]
        public double GetOrderValueByID(int orderID)
        {
            var orderDetails = context.OrderDetails.Where(order => order.OrderId == orderID);

            double orderValue = -1;

            if (orderDetails != null)
            {
                orderValue = 0;

                orderValue = orderDetails.Sum(o => o.Quantity * (double)o.UnitPrice * (1.0f - o.Discount));
            }

            return orderValue;
        }
    }
}