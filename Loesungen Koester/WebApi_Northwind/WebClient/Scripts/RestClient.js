﻿// URL der Web-API
const baseUrl = 'http://localhost:50858/api/northwind';

$(document).ready(function ()
{
    $("#buttonCustomerID").on("click", function ()
    {
        let customerId = $("#textboxCustomerID").val();

        let path = 'GetCustomerByID?customerID=';

        loadFromRest(path, customerId, "#output", showCustomer);
    });

    $("#buttonCountry").on("click", function ()
    {
        let country = $("#textboxCountry").val();

        let path = 'GetCustomersByCountry?country=';

        loadFromRest(path, country, "#output", showAllCustomers);
    });

    $("#buttonOrders").on("click", function ()
    {
        let customerId = $("#textboxCustomerIdOrders").val();

        //console.log(customerId);

        let path = 'GetOrdersByCustomerID/';

        loadFromRest(path, customerId, "#output", showAllOrders);
    });

    $("#buttonOrderId").on("click", function ()
    {
        let orderId = $("#textboxOrderId").val();

        let path = 'GetOrderValueByID/';

        loadFromRest(path, orderId, "#output", showOrderValue);
    });
});

function loadFromRest(path, searchValue, outputID, displayFunction)
{
    'use strict';

    $.ajax({
        url: baseUrl + '/' + path + searchValue,
        accepts: 'application/json',
        success: function (data)
        {
            //console.log(data);

            $(outputID).empty();

            displayFunction(data, outputID);
        }
    });
}

function showCustomer(customer, outputID)
{
    var table = $('<table border="1">');

    table.append(createTableHeader());

    table.append(showOneCustomer(customer));

    $(outputID).append(table);
}

function createTableHeader()
{
    var row = $('<tr>');

    row.append($('<th>').text("Kunde"));
    row.append($('<th>').text("Kundennummer"));
    row.append($('<th>').text("Anschrift"));
    row.append($('<th>').text("Stadt"));
    row.append($('<th>').text("Land"));

    return row;
}

function showOneCustomer(customer)
{    
    var row = $('<tr>');

    row.append($('<td>').text(customer.companyName));
    row.append($('<td>').text(customer.customerId));
    row.append($('<td>').text(customer.address));
    row.append($('<td>').text(customer.city));
    row.append($('<td>').text(customer.country));

    return row;
}

function showAllCustomers(customers, outputID)
{
    var table = $('<table border="1">');

    table.append(createTableHeader());

    for (var i = 0; i < customers.length; i++)
    {
        table.append(showOneCustomer(customers[i]));        
    }

    $(outputID).append(table);
}

function showAllOrders(orders, outputID)
{
    var table = $('<table border="1">');

    for (var i = 0; i < orders.length; i++)
    {
        var row = $('<tr>');

        row.append($('<td>').text(orders[i].orderId));
        row.append($('<td>').text(orders[i].orderDate));

        table.append(row);
    }

    $(outputID).append(table);
}

function showOrderValue(orderValue, outputID)
{
    $(outputID).append(orderValue);
}