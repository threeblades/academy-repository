﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Caching_Wetterdaten.Models;
using Microsoft.Extensions.Caching.Memory;

namespace Caching_Wetterdaten.Controllers
{
    public class HomeController : Controller
    {
        WetterRepository repository = new WetterRepository();

        private IMemoryCache myCache;

        public HomeController(IMemoryCache memoryCache)
        {
            myCache = memoryCache;
        }

        public IActionResult Index()
        {
            List<WetterInfo> wetterInfos;

            // Versuchen Wetterdaten aus dem Cache zu laden
            bool ifExist = myCache.TryGetValue("Wetterdaten", out wetterInfos);

            if (ifExist == false)
            {
                // Alle Dateien der Webseite laden
                repository.DownloadAllFiles();

                // Sortieren
                wetterInfos = repository.WetterInfos.OrderBy(wi => wi.WetterDatum).ToList();

                // Im Cache speichern
                myCache.Set("Wetterdaten", wetterInfos);
            }

            return View(wetterInfos);
        }

        public IActionResult UpdateWeatherData()
        {
            // Wetterdaten aus dem Cache im Repository speichern
            repository.WetterInfos = myCache.Get<List<WetterInfo>>("Wetterdaten");

            // Dateien herunterladen (lädt nur neue Dateien)
            repository.DownloadAllFiles();

            // Sortieren
            List<WetterInfo> wetterInfos = repository.WetterInfos.OrderBy(wi => wi.WetterDatum).ToList();

            // Neue Liste in den Cache
            myCache.Set("Wetterdaten", wetterInfos);

            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
