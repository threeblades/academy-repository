﻿// Write your JavaScript code.
$(document).ready(init);

function init()
{
    // Alle div Element verstecken
    $(".line2").hide();

    // Bei einem Klick auf eine Überschrift...
    $(".line1").click(function ()
    {
        // ...das nächste Nachbar-Element anzeigen/ausblenden
        $(this).next(".line2").toggle();
    });
}