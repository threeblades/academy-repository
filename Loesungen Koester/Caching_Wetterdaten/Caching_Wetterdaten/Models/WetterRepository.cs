﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading.Tasks;

namespace Caching_Wetterdaten.Models
{
    public class WetterRepository
    {
        public List<WetterInfo> WetterInfos { get; set; }

        public WetterRepository()
        {
            WetterInfos = new List<WetterInfo>();
        }

        public void DownloadAllFiles()
        {
            string url = "http://opendata.dwd.de/weather/text_forecasts/txt/";

            WebClient webClient = new WebClient();

            string htmlIndexPage = webClient.DownloadString(url);

            // Nur die Dateien mit COR im Dateinamen herunterladen
            //string regex = "href=\"(.*COR.*)\"";

            // All Dateien herunterladen
            string regex = "href=\"(.*)\"";

            // Alle href Matches
            MatchCollection matches = Regex.Matches(htmlIndexPage, regex);

            Parallel.For(0, matches.Count, i =>
            {
                try
                {
                    // Dateiname per Regex
                    string downloadName = matches[i].Groups[1].Value;

                    // Wurde für diesen Dateinamen schon ein Objekt erzeugt?
                    WetterInfo wetterInfo = WetterInfos.AsParallel().SingleOrDefault(w => w.Dateiname == downloadName);

                    // Noch kein Objekt vorhanden, dann den Download der Datei starten um ein Objekt zu erzeugen
                    if (wetterInfo == null)
                    {
                        DownloadSingleFile(url, downloadName);
                    }
                }
                catch
                {
                }
            });
        }

        private void DownloadSingleFile(string url, string downloadName)
        {
            WebClient webClient = new WebClient();

            // Encoding für die Textdateien vom DWD setzen
            webClient.Encoding = Encoding.UTF7;

            WetterInfo wetterInfo = new WetterInfo();

            wetterInfo.Dateiname = downloadName;

            string fileURL = Path.Combine(url, downloadName);

            // Den Inhalt in einen String laden            
            string content = webClient.DownloadString(fileURL);

            // Inhalt an Zeilenumbruch zerlegen
            string[] lines = content.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            // Region ist in der dritten Zeile nach dem Wort 'für'
            wetterInfo.Region = Regex.Split(lines[3], "für")[1];

            // Datum und Uhrzeit des Wetterberichts in der 5. Zeile
            string datum = lines[5].Split(',')[1];
            string uhrzeit = lines[5].Split(',')[2];
            wetterInfo.WetterDatum = DateTime.Parse(datum + " " + uhrzeit);

            // Wetterbericht ab der 6. Zeile
            wetterInfo.Beschreibung = string.Join(" ", lines, 6, lines.Length - 6);

            wetterInfo.DownloadDatum = DateTime.Now;

            // TODO Concurrent Collection!!!
            WetterInfos.Add(wetterInfo);
        }
    }
}