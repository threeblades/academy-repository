﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EF_CodeFirst_MigrationsAufgabe.Models
{
    // Klasse für Aufgabe 4 erstellen
    public class Publisher
    {
        public int ID { get; set; }

        [Display(Name = "Verlag:")]
        [Required, MaxLength(100)]
        public string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}