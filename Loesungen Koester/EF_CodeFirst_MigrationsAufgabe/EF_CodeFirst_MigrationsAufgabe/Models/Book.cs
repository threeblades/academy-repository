﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF_CodeFirst_MigrationsAufgabe.Models
{
    public class Book
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(200)]
        [Display(Name = "Titel:")]
        public String Title { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Veröffentlichungsdatum:")]
        public DateTime Published { get; set; }

        // für Aufgabe 3
        [Required]
        [Display(Name = "Seiten:")]
        public int Pages { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Sprachen:")]
        public string Languages { get; set; }

        // für Aufgabe 4
        [Display(Name = "Verlag:")]
        public int PublisherID { get; set; }

        public Publisher Publisher { get; set; }
    }
}