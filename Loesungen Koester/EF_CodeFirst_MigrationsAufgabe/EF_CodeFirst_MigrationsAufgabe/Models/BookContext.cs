﻿using Microsoft.EntityFrameworkCore;

namespace EF_CodeFirst_MigrationsAufgabe.Models
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public BookContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<EF_CodeFirst_MigrationsAufgabe.Models.Publisher> Publisher { get; set; }
    }
}