﻿using Microsoft.AspNetCore.Mvc;
using MVC_Validierung_BLZ.Models;
using System.Diagnostics;

namespace MVC_Validierung_BLZ.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(PaymentInfo info)
        {
            if (info != null && ModelState.IsValid)
            {
                return View(info);
            }

            return View();
        }

        public IActionResult PaymentDataValidation(string BLZ)
        {
            BLZRepository blzRepository = new BLZRepository();

            if (blzRepository.BLZSuchen(BLZ))
            {
                return Json(true);
            }

            return Json("BLZ nicht gefunden");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}