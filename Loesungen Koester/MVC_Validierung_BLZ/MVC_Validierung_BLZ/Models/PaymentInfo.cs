﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace MVC_Validierung_BLZ.Models
{
    public class PaymentInfo
    {
        [Required]
        [RegularExpression(@"^\d{4,9}$", ErrorMessage = "Ungültiges Format!")]
        [Display(Name = "Kontonummer:")]
        public string KontoNr { get; set; }

        [Required]
        [Remote(action: "PaymentDataValidation", controller: "Home")]
        [Display(Name = "BLZ:")]
        public string BLZ { get; set; }
    }
}