﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace MVC_Validierung_BLZ.Models
{
    public class BLZRepository
    {
        private readonly string connectionString = @"Data Source=.\SQLEXPRESS;" +
                                                    "Initial Catalog=TeachSQL;" +
                                                    "Integrated Security=true;";

        public bool BLZSuchen(string blz)
        {
            bool gefunden = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("SELECT BLZ FROM BLZTabelle WHERE BLZ = @gesuchteBLZ", connection);
                    command.Parameters.AddWithValue("gesuchteBLZ", blz);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        gefunden = reader.HasRows;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
            return gefunden;
        }
    }
}