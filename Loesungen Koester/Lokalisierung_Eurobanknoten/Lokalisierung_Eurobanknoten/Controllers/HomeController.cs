﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Lokalisierung_Eurobanknoten.Models;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Lokalisierung_Eurobanknoten.Controllers
{
    public class HomeController : Controller
    {
        private readonly Seriennummerprüfer prüfer;

        // Der Prüfer wird per Dependency Injection in den HomeController übergeben
        // Wird in der Starup.cs konfiguriert
        public HomeController(Seriennummerprüfer prüfer)
        {
            this.prüfer = prüfer;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Single(string serialnumber)
        {
            // Prüfe die übergebene Seriennummer
            ViewBag.Message = prüfer.CheckSerial(serialnumber);

            return View("Index");
        }

        [HttpPost]
        public IActionResult Multiple(string serialnumbers)
        {
            // Seriennummer aus der Textarea zerlegen
            // TODO weitere Trennzeichen einfügen
            string[] numbers = serialnumbers.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            CheckMultipleNumbers(numbers);

            // Geprüfte Nummern mit Information als String ins ViewData schreiben
            ViewData["checkedSerials"] = string.Join(Environment.NewLine, numbers);

            return View("Index");
        }

        private void CheckMultipleNumbers(string[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = prüfer.CheckSerial(numbers[i].Trim());
            }
        }

        [HttpPost]
        public IActionResult FileCheck(IFormFile file)
        {
            List<string> allSerialsFromFile = new List<string>();

            ReadSerialsFromFile(file, allSerialsFromFile);

            // Geprüfte Nummern mit Informationen als String ins ViewData schreiben
            ViewData["checkedSerialsFromFile"] = string.Join(Environment.NewLine, allSerialsFromFile);

            return View("Index");
        }

        // Der folgende Methode könnte/sollte in einer extra Model-Klasse liegen
        private void ReadSerialsFromFile(IFormFile file, List<string> allSerialsFromFile)
        {
            try
            {
                StreamReader streamReader = new StreamReader(file.OpenReadStream());

                while (streamReader.EndOfStream == false)
                {
                    // Es wird von einer Seriennummer pro Zeile ausgegangen
                    string number = streamReader.ReadLine().Trim();

                    number = prüfer.CheckSerial(number);

                    allSerialsFromFile.Add(number);
                }
            }
            catch (Exception e)
            {
                ViewBag.MessageFile = e.Message;
            }
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
