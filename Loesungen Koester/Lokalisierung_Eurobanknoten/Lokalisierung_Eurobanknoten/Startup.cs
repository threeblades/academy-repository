﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Lokalisierung_Eurobanknoten.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lokalisierung_Eurobanknoten
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Für die Lokalisierung (resx-Dateien liegen im Ordner Resources)
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            // Für die Lokalisierung von Views mittels @inject
            services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);

            // Seriennummerprüfer per Dependency Injection (DI) hinzufügen
            // (Seriennummerprüfer steht dem HomeController per DI zur Verfügung)
            // (Seriennummerprüfer wird per DI der IStringLocalizer zugewiesen)
            services.AddTransient<Seriennummerprüfer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            CultureInfo[] supportedCultures = new[]
            {
                new CultureInfo("en-us"),
                new CultureInfo("pt-br"),
                new CultureInfo("de-de"),
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                // Standard Kultur (auch als Fallback für unbekannte Kulturen)
                DefaultRequestCulture = new RequestCulture("de-de"),

                // Formatierung für Zahlen, Datums und Währungsangaben, etc.
                SupportedCultures = supportedCultures,

                // Für lokalisierte Strings aus Ressourcendateien
                SupportedUICultures = supportedCultures
            });

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
