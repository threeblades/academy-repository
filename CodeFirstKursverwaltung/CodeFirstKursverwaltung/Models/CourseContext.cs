﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeFirstKursverwaltung.Models
{
    public class CourseContext : DbContext
    {
        
        public DbSet<Course> Courses { get; set; }

        public CourseContext(DbContextOptions options): base(options)
        {
            Database.EnsureCreated();
        }

    }
}
